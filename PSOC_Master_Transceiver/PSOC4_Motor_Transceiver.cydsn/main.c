#include <project.h>
#include "../../External References/Definitions.h"
#include "../../External References/RFM7X.h"

#define PackageSignal 		0x25
#define ToDevice			0x20

struct RfHandler rfHandler;
unsigned char state;
unsigned char hasHeader;
unsigned char currentIndex;

int main()
{
    Initialize_HW();
    Initialize_SW();
    for(;;)
    {
        Single_Operation();
    }
}

void Initialize_HW()
{
	CyGlobalIntEnable;   
	
	Serial_Start();
    
    SPIM_Start();
	RFM7X_Start();
    RFM7X_SetChannel(10);
    LED_Write(1);
}

void Initialize_SW()
{    
    state = State_Wait_Input;
    hasHeader = 0;
    currentIndex = 0;
    
    rfHandler.Pipe = 0;
    rfHandler.Length = 0;
    rfHandler.Mode = RF_Receiver;
    Sub_ResetArray_Unsigned(rfHandler.TransmitPackage, Size_RF_Package);
    Sub_ResetArray_Unsigned(rfHandler.ReceivePackage, Size_RF_Package);
}

void Single_Operation()
{
    switch(state)
    {
        case State_RF_Receive:      Do_RF_Receive();        break;
        case State_RF_Transmit:     Do_RF_Transmit();       break;
        case State_Wait_Input:
        default:                    Do_Wait_Input();        break;
    }
}

void Do_RF_Receive()
{
    int i;
    
    if(RFM7X_Receive(rfHandler.ReceivePackage, &rfHandler.Length, &rfHandler.Pipe))
    {
        for(i = 0; i < rfHandler.Length; i++)
        {
            Serial_UartPutChar(rfHandler.ReceivePackage[i]);
        }
    }    
    
    state = State_Wait_Input;
}

void Do_RF_Transmit()
{
    state = State_Wait_Input;
    if((rfHandler.TransmitPackage[1] & ToDevice) != 0)
    {
        return;
    }
    
    if(rfHandler.Mode != RF_Transmitter)
    {
        RFM7X_Mode_Tx();
        rfHandler.Mode = RF_Transmitter;
    }
    else
    {
        RFM7X_ClearInterrupt();
    }
    
    LED_Write(!LED_Read());
    RFM7X_Transmit(rfHandler.TransmitPackage, Size_RF_Package);
    CyDelay(5);
    
    RFM7X_Mode_Rx();
    rfHandler.Mode = RF_Receiver;
}

void Do_Wait_Input()
{
    unsigned char tmpValue;
    //PC Input
    while(Serial_SpiUartGetRxBufferSize() > 0)
    {
        tmpValue = Serial_UartGetChar();
        if(tmpValue == PackageSignal)
        {
            hasHeader = 1;
        }
        
        if(hasHeader)
        {
            if(currentIndex < Size_RF_Package)
            {
                rfHandler.TransmitPackage[currentIndex] = tmpValue;
                currentIndex++;
            }
            
            if(currentIndex >= Size_RF_Package)
            {
                state = State_RF_Transmit;
                currentIndex = 0;
                hasHeader = 0;
                return;
            }
        }
    }
    //RF Input
    if(rfHandler.Mode == RF_Receiver && RFM7X_HasData())
    {
        state = State_RF_Receive;
        return;
    }
    //Do nothing
    CyDelay(1);
}