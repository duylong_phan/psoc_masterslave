#include <project.h>
#include "../../External References/ESP8266.h"
#include "../../External References/Definitions.h"
#include "../../External References/Definitions.h"
#include "../../External References/Queue.h"
#include "../../External References/SubFunctions.h"
#include "../../External References/HeaderPackage.h"

//#define Mode_Passive        1
#define Mode_Active         1

struct ESP8266Handler handler;
struct HeaderPackage inputPackage;
unsigned char __headerContainer[Size_HeaderContainer];

int main()
{
    Initialize_HW();
    Initialize_SW();
    Initialize_Parameters();
    
    for(;;)
    {
        Single_Operation();
    }
}

void Initialize_HW()
{
    CyGlobalIntEnable; 
    Serial_Start();
    Serial_1_Start(); 
}

void Initialize_SW()
{
    ESP8266Handler_Initialize(&handler, __headerContainer, Size_HeaderContainer);
}

void Initialize_Parameters()
{
    
}

void Single_Operation()
{
//    char responseText[32];
    unsigned char tmpValue = 0, status  = 0;
    unsigned char responseBuffer1[10] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99};
    unsigned char responseBuffer2[10] = {0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00};
    unsigned char *counter = &responseBuffer2[9];
    
    #ifdef Mode_Passive
        unsigned char hasJob = 0;
        while(Serial_SpiUartGetRxBufferSize() > 0)
        {
            Serial_1_UartPutChar(Serial_UartGetChar());
            hasJob = 1;
        }
        
        while(Serial_1_SpiUartGetRxBufferSize() > 0)
        {
            Serial_UartPutChar(Serial_1_UartGetChar());
            hasJob = 1;
        }
        
        if(hasJob == 0)
        {
            CyDelay(10);
        }
    #endif
    
    #ifdef Mode_Active        
        CyDelay(2000);              
        ESP8266_SetMode(ESP8266_Mode_AP);
        CyDelay(100);
        
        ESP8266_SetIpAddress("192.168.25.1");
        CyDelay(100);
        
        ESP8266_SetMacAddress("62:01:94:06:52:AA");
        CyDelay(100);
        
        Serial_SpiUartClearRxBuffer();
        ESP8266_TurnWlanOn("LongESP", "LongESP123", 3, ESP8266_Enc_WPA2_PSK);
        CyDelay(5000);
        while(Serial_SpiUartGetRxBufferSize() > 0)
        {
            tmpValue = Serial_UartGetChar();
            status = ESP8266_Parse(&handler,tmpValue );
            if(status == ESP8266_Status_Ok)
            {
                LED_1_Write(1);
                break;
            }
        }
        
        ESP8266_SetMuxConnection(ESP8266_Mux_Multi);
        CyDelay(100);
        ESP8266_StartWebServer(1234);       
        
        for(;;)
        {
            ESP8266_CheckInput(&handler);
            if(HeaderPackage_Parse(&handler.InputPackage))
            {
                switch(handler.InputPackage.Type)
                {
                    case 0:                 
                    LED_2_Write(0);     
                    ESP8266_WriteResponse(&handler, responseBuffer1, 10);
                    break;
                    
                    case 1:                 
                    LED_2_Write(1);     
                    ESP8266_WriteResponse(&handler, responseBuffer2, 10);
                    (*counter)++;
                    break;
                    
                    default:                break;
                }
            }
        }
        
    #endif
}