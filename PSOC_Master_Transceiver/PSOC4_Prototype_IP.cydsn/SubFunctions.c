#include "SubFunctions.h"


//------------------------------------Abstract Method-------------------------------
void Sub_DelayMs(int miliSecond)
{
	#if defined(PSOC4_Mode) || defined(PSOC5_Mode)
	    CyDelay(miliSecond);
    #endif
    
    #ifdef Arduino_Mode
        delay(miliSecond);
    #endif
}

void Sub_DelayUs(int microSecond)
{
    #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
	    CyDelayUs(microSecond);
    #endif
    
    #ifdef Arduino_Mode
        delayMicroseconds(microSecond);
    #endif
}
//---------------------------------------Method-------------------------------------

unsigned char Sub_GetCheckSum(unsigned char* data, int length)
{
	unsigned char checkSum = 0;
	int i = 0;

	for (i = 0; i < length; i++)
	{
		checkSum ^= data[i];
	}

	return checkSum;
}

void Sub_ResetArray_Unsigned(unsigned char* data, int length)
{
	int i = 0;

	for (i = 0; i < length; i++)
	{
		data[i] = 0;
	}
}

void Sub_ResetArray_Signed(char* data, int length)
{
	int i = 0;

	for (i = 0; i < length; i++)
	{
		data[i] = 0;
	}
}