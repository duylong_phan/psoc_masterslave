#include "Config.h"
#include "../../External_References_V2/IDefinitions.h"
#include "../../External_References_V2/IQueue.h"
#include "../../External_References_V2/ISystem.h"
#include "../../External_References_V2/IRF.h"

//#define Mode_Tx     1
#define Mode_Rx     1


//--------------------------Buffer--------------------------
unsigned char __stateContainer[Size_StateContainer];

//---------------------------Global Variable--------------------
struct ISystem *_system;
struct ISPIM *_spim;
struct IRF _rf;
struct IQueue stateQueue;
struct RfHandler rfHandler;

unsigned char mode;
unsigned char RF_Channel = 20;
unsigned char RF_Rx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};
unsigned char RF_Tx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};
uint tickCount;

int main()
{    
    Initialize_SW();
    Initialize_HW();
    
    #ifdef Mode_Tx
    _rf.Enter_Tx(&_rf);
    
    for(;;)
    {
        _rf.Transmit(&_rf, rfHandler.TransmitPackage, 32);
        LED_2_Write(!LED_2_Read());
        CyDelay(50);
    }    
    #endif
    
    #ifdef Mode_Rx
        for(;;)
        {
            if(_rf.HasData(&_rf))
            {
                _rf.Receive(&_rf, rfHandler.ReceivePackage, &rfHandler.Length, &rfHandler.Pipe);
                LED_2_Write(!LED_2_Read());
            }
            CyDelay(1);
        }    
    #endif
}

void Initialize_SW()
{
    unsigned char nextState = State_RF_Transmit;
    tickCount = 0;
    
    //System
    Initialize_ISystem();
    _system = Get_ISystem();
    
    //SPI
    _spim = Get_ISPIM(0);
    Initialize_ISPIM(_spim);
    
    //Queue
    Initialize_IQueue(&stateQueue, __stateContainer, Size_StateContainer, sizeof(unsigned char));    
    stateQueue.Enqueue(&stateQueue, &nextState);
    
    //RF
    Initialize_IRF(&_rf);
    
    //-----handler----
    rfHandler.Pipe = 0;
    rfHandler.Length = 0;
    rfHandler.LostCount = 0;
    rfHandler.Mode = RF_Receiver;
    _system->Reset_UnsignedArray(rfHandler.TransmitPackage, Size_RF_Package);
    _system->Reset_UnsignedArray(rfHandler.ReceivePackage, Size_RF_Package); 
}

void Initialize_HW()
{
    CyGlobalIntEnable;
    
    //SPI
    _spim->Start(_spim);    
    
    //RF    
    _rf.Start(&_rf, _spim);
    _rf.SetChannel(&_rf, RF_Channel);
    _rf.SetAddress_Tx(&_rf, RF_Tx_Address);
    _rf.SetAddress_Rx(&_rf, RF_Rx_Address);   
}