#include  "Config.h"
    
#ifndef ISerialCom_h
#define ISerialCom_h
    struct ISerialCom
    {
        unsigned char ID;
        unsigned char IsDisposed;        
        
        void (*Start)(struct ISerialCom *me);                
        int (*GetBytesToRead)(struct ISerialCom *me);
        void (*WriteString)(struct ISerialCom *me, const char *message);
        void (*Write)(struct ISerialCom *me, const unsigned char *data, int length);
        void (*Read)(struct ISerialCom *me, unsigned char *data, int *length);
        void (*Dispose)(struct ISerialCom *me);
    };
    
    void Initialize_ISerialCom(struct ISerialCom *me, unsigned char id);
#endif