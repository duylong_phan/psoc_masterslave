#include "Config.h"

#ifndef ICapacitor_h
#define ICapacitor_h  
    #define CapacitorMeasureController_SPI          0
    #define CapacitorMeasureController_Charging     1
    #define CapacitorMeasureController_Discharging  2
    
    struct ICapacitor
    {
        unsigned char IsDisposed;
        unsigned char ID;
        unsigned char IsEnabled;
        unsigned char HasValue;
        unsigned int *Value;
        void (*Dispose)(struct ICapacitor *me);
    };
    
    struct ICapacitorMeasureController
    {
        unsigned char IsDisposed;
        unsigned char Status;
        void (*Enter)(struct ICapacitorMeasureController *me, unsigned char mode);
        void (*Enter_SPI)(struct ICapacitorMeasureController *me);
        void (*Enter_Charging)(struct ICapacitorMeasureController *me);
        void (*Enter_Discharging)(struct ICapacitorMeasureController *me);
        void (*Dispose)(struct ICapacitorMeasureController *me);
    };
        
    void Initialize_ICapacitor(struct ICapacitor *me, unsigned char id, unsigned char isEnabled, unsigned int *value);
    void Initialize_ICapacitorMeasureController(struct ICapacitorMeasureController *me);
#endif