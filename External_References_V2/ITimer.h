#ifndef ITimer_h
#define ITimer_h 
    
    struct ITimer
    {
        unsigned char IsDisposed;
        unsigned int TickCount;
        
        void (*OnMiliSecondTick)(struct ITimer *me);
        void (*Dispose)(struct ITimer *me);
    };
    
    void Initialize_ITimer(struct ITimer *me);
#endif