#include "Config.h"
    
#ifndef IADC_h
#define IADC_h 
    struct IADC
    {
        unsigned char IsDisposed;
        void (*Start)(struct IADC *me);
        unsigned int (*GetValue)(struct IADC *me, unsigned char id);
        void (*Dispose)(struct IADC *me);
    };
    
    void Initialize_IADC(struct IADC *me);
    
#endif