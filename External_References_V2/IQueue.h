#include <stdio.h>
#include <string.h>
    
#ifndef IQueue_h
#define IQueue_h   
    
    struct IQueueInfo
    {
        unsigned char IsValid;
        int Count;
        int StartIndex;
        int EndIndex;
    };
    
    struct IQueue
    {
        unsigned char *Container;   
        unsigned char IsDispose;
        int MaxAmount;        
        int TypeSize;        
        int Count;        
        int StartIndex, EndIndex;
        
        void (*Enqueue)(struct IQueue *me, void *item);
        void* (*Dequeue)(struct IQueue *me);
        void* (*Peek)(struct IQueue *me);
        void (*ToInfo)(struct IQueue *me, struct IQueueInfo *info);
        void (*ToQueue)(struct IQueue *me, struct IQueueInfo *info);        
        void (*Dispose)(struct IQueue *me);
    };
    
    void Initialize_IQueue(struct IQueue *me, unsigned char* container, int maxAmount, int typeSize);    
#endif