#include "IQueue.h"

//---------------------------Private Method---------------------------------
unsigned char* IQueue_GetItem(struct IQueue *me, int index)
{
    return me->Container + index*me->TypeSize;
}

void IQueue_SetItem(struct IQueue *me, int index, void *item)
{    
    //Pointer Type as Container Pointer Type
    unsigned char *source = (unsigned char *)item;
    unsigned char *dest = IQueue_GetItem(me, index);
    //copy byte by byte
    memcpy(dest, source, me->TypeSize);
}

void IQueue_Enqueue(struct IQueue *me, void *item)
{
    //Overflow Array Index => reset
    if(me->StartIndex >= me->MaxAmount)
    {
        me->StartIndex = 0;
    }
    if(me->EndIndex >= me->MaxAmount)
    {
        me->EndIndex = 0;
    }

    //Overflow Count => increase EndIndex
    if(me->EndIndex == me->StartIndex && me->Count > 0)
    {
        me->EndIndex++;
    }

    //Set item value, Update Index
    IQueue_SetItem(me, me->StartIndex, item);
    me->StartIndex++;

    //Update Count
    if(me->Count < me->MaxAmount)
    {
        me->Count++;
    }
}

void* IQueue_Dequeue(struct IQueue *me)
{
    void* item = NULL;
    //Overflow Max Amount
    if(me->EndIndex >= me->MaxAmount)
    {
        me->EndIndex = 0;
    }

    //Update Counter
    if(me->Count > 0)
    {
        me->Count--;
    }

    //Get item
    item = IQueue_GetItem(me, me->EndIndex);
    
    //Update EndIndex
    me->EndIndex++;    
    return item;
}

void* IQueue_Peek(struct IQueue *me)
{
    //Overflow Max Amount
    if(me->EndIndex > me->MaxAmount)
    {
        me->EndIndex = 0;
    }
    return IQueue_GetItem(me, me->EndIndex);
}

void IQueue_ToInfo(struct IQueue *me, struct IQueueInfo *info)
{
    info->IsValid = 1;
    info->Count = me->Count;
    info->StartIndex = me->StartIndex;
    info->EndIndex = me->EndIndex;
}

void IQueue_ToQueue(struct IQueue *me, struct IQueueInfo *info)
{
    me->Count = info->Count;
    me->StartIndex = info->StartIndex;
    me->EndIndex = info->EndIndex;
}

void IQueue_Dispose(struct IQueue *me)
{
    me->IsDispose = 1;
}

void Initialize_IQueue(struct IQueue *me, unsigned char* container, int maxAmount, int typeSize)
{
    me->Container = container;    
    me->MaxAmount = maxAmount;
    me->TypeSize = typeSize;
    me->Count = 0;
    me->StartIndex = 0;
    me->EndIndex = 0;    
    
    me->Enqueue = &IQueue_Enqueue;
    me->Dequeue = &IQueue_Dequeue;
    me->Peek = &IQueue_Peek;
    me->ToInfo = &IQueue_ToInfo;
    me->ToQueue = &IQueue_ToQueue;
    me->Dispose = &IQueue_Dispose;
}