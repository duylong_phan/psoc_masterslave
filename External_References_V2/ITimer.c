#include "ITimer.h"

void ITimer_Dispose(struct ITimer *me)
{
    me->IsDisposed = 1;
}

void Initialize_ITimer(struct ITimer *me)
{
    me->TickCount = 0;
    me->IsDisposed = 0;
    me->Dispose = &ITimer_Dispose;
}