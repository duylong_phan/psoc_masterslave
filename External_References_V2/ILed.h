#include "Config.h"

#ifndef ILed_h
#define ILed_h
    
    struct ILed
    {
        unsigned char ID;
        unsigned char (*GetValue)(struct ILed *me);
        void (*SetValue)(struct ILed *me, unsigned char value);
        void (*SetHigh)(struct ILed *me);
        void (*SetLow)(struct ILed *me);
        void (*Toggle)(struct ILed *me);
    };
    
    void Initialize_ILed(struct ILed *me, unsigned char id, unsigned char value);
    
#endif