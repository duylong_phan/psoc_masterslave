#include "IAccelerometer.h"

#ifdef Com_ADXL343
    #define ADXL343_Reg_ID                      0x00
    #define ADXL343_Reg_BW_RATE                 0x2C
    #define ADXL343_Reg_POWER_CTL               0x2D
    #define ADXL343_Reg_DATA_FORMAT             0x31
    #define ADXL343_Reg_X0                      0x32
    #define ADXL343_Reg_X1                      0x33
    #define ADXL343_Reg_Y0                      0x34
    #define ADXL343_Reg_Y1                      0x35
    #define ADXL343_Reg_Z0                      0x36
    #define ADXL343_Reg_Z1                      0x37
    #define ADXL343_Reg_FIFO_CTL                0x38
    #define ADXL343_Reg_FIFO_STATUS             0x39

    #define ADXL343_Bit_BW_RATE_LowPower        4
    #define ADXL343_Bit_BW_RATE_DataRate        0
    #define ADXL343_Bit_POWER_CTL_Link          5
    #define ADXL343_Bit_POWER_CTL_AutoSleep     4
    #define ADXL343_Bit_POWER_CTL_Measure       3
    #define ADXL343_Bit_POWER_CTL_Sleep         2
    #define ADXL343_Bit_POWER_CTL_WakeUp        0
    #define ADXL343_Bit_DATA_FORMAT_Test        7
    #define ADXL343_Bit_DATA_FORMAT_SPI         6
    #define ADXL343_Bit_DATA_FORMAT_Int_Invert  5
    #define ADXL343_Bit_DATA_FORMAT_Full        3
    #define ADXL343_Bit_DATA_FORMAT_Justify     2
    #define ADXL343_Bit_DATA_FORMAT_Range       0
    #define ADXL343_Bit_FIFO_MODE               6
    #define ADXL343_Bit_FIFO_Trigger            5

    #define ADXL343_Mask_FIFO_TRIG              0x40
    #define ADXL343_Mast_FIFO_Entry             0x3F

    #define ADXL343_Value_ID                    0xE5
    #define ADXL343_Value_DataRate_3200         0x0F
    #define ADXL343_Value_DataRate_1600         0x0E
    #define ADXL343_Value_DataRate_800          0x0D
    #define ADXL343_Value_DataRate_400          0x0C
    #define ADXL343_Value_DataRate_200          0x0B
    #define ADXL343_Value_DataRate_100          0x0A
    #define ADXL343_Value_DataRate_50           0x09
    #define ADXL343_Value_DataRate_25           0x08
    #define ADXL343_Value_Test_Enable           1
    #define ADXL343_Value_Test_Disable          0
    #define ADXL343_Value_SPI_3                 1
    #define ADXL343_Value_SPI_4                 0
    #define ADXL343_Value_Int_Low               1
    #define ADXL343_Value_Int_High              0
    #define ADXL343_Value_Full_4mg_LSB          1
    #define ADXL343_Value_Full_10_Bit           0
    #define ADXL343_Value_Justify_Sign_Left     1
    #define ADXL343_Value_Justify_Sign_Right    0
    #define ADXL343_Value_G_2                   0
    #define ADXL343_Value_G_4                   1
    #define ADXL343_Value_G_8                   2
    #define ADXL343_Value_G_16                  3
    #define ADXL343_Value_FIFO_Bypass           0
    #define ADXL343_Value_FIFO_FIFO             1
    #define ADXL343_Value_FIFO_Stream           2
    #define ADXL343_Value_FIFO_Trigger          3
    #define ADXL343_Value_FIFO_Trigger_INT1     0
    #define ADXL343_Value_FIFO_Trigger_INT2     1
    
    //----------------------Private Method----------------------
    void ADXL343_WriteReg(struct IAccelerometer *me, unsigned char reg, unsigned char data)
    {
        me->Master->WriteReg(me->Master, reg, data, me->Pin_SS);
    }
    
    unsigned char ADXL343_ReadReg(struct IAccelerometer *me, unsigned char reg)
    {
        //Read Register
        reg |= 0x80;
        return me->Master->ReadReg(me->Master, reg, me->Pin_SS);
    }
    
    void ADXL343_ReadRegSequence(struct IAccelerometer *me, unsigned char reg, unsigned char *buffer, int length)
    {
        //Read Register + multiple read
        reg |= 0xC0;
        me->Master->ReadRegBuffer(me->Master, reg, buffer, length, me->Pin_SS);        
    }
    
    short ADXL343_ReadAxisValue(struct IAccelerometer *me, unsigned char reg)
    {
        unsigned char buffer[2];
        short value;
        
        ADXL343_ReadRegSequence(me, reg, buffer, 2);
        value = (buffer[1] << 8) + buffer[0];
        value = (short)(value * me->G_Scale);
        return value;
    }
    
    //----------------------Public Method----------------------
    void ADXL343_Start(struct IAccelerometer *me)
    {
        unsigned char tmp;       
        
        //Enable Normal Operation Power
        //Select 800Hz
        tmp = (0 << ADXL343_Bit_BW_RATE_LowPower) |
              (ADXL343_Value_DataRate_800 << ADXL343_Bit_BW_RATE_DataRate); 
        ADXL343_WriteReg(me, ADXL343_Reg_BW_RATE, tmp);
        
        //disable Link
        //disable AutoSleep
        //Enable Measure
        //Enable Normal Operation
        //WakeUp is not interested
        tmp = (0 << ADXL343_Bit_POWER_CTL_Link) |
              (0 << ADXL343_Bit_POWER_CTL_AutoSleep) |
              (1 << ADXL343_Bit_POWER_CTL_Measure) |
              (0 << ADXL343_Bit_POWER_CTL_Sleep) |
              (0 << ADXL343_Bit_POWER_CTL_WakeUp);
        ADXL343_WriteReg(me, ADXL343_Reg_POWER_CTL, tmp);
        
        //disable Self-Test
        //enable SPI 4 Wire
        //Interrupt with LOW value
        //select 10 Bits Mode
        //select sign on MSB
        //select 16g
        tmp = (ADXL343_Value_Test_Disable << ADXL343_Bit_DATA_FORMAT_Test) |
              (ADXL343_Value_SPI_4 << ADXL343_Bit_DATA_FORMAT_SPI) |
              (ADXL343_Value_Int_Low << ADXL343_Bit_DATA_FORMAT_Int_Invert) |
              (ADXL343_Value_Full_10_Bit << ADXL343_Bit_DATA_FORMAT_Full) |
              (ADXL343_Value_Justify_Sign_Left << ADXL343_Bit_DATA_FORMAT_Justify) |
              (ADXL343_Value_G_16 << ADXL343_Bit_DATA_FORMAT_Range);
        ADXL343_WriteReg(me, ADXL343_Reg_DATA_FORMAT, tmp);
        
        //select Stream Mode
        //Trigger INT is not interested
        tmp = (ADXL343_Value_FIFO_Bypass << ADXL343_Bit_FIFO_MODE) |
              (ADXL343_Value_FIFO_Trigger_INT1 << ADXL343_Bit_FIFO_Trigger);
        ADXL343_WriteReg(me, ADXL343_Reg_FIFO_CTL, tmp);
    }

    unsigned char ADXL343_HasDevice(struct IAccelerometer *me)
    {
        return (ADXL343_ReadReg(me, ADXL343_Reg_ID) == ADXL343_Value_ID);
    }

    unsigned char ADXL343_HasData(struct IAccelerometer *me)
    {
        unsigned char tmp = ADXL343_ReadReg(me, ADXL343_Reg_FIFO_STATUS);
        tmp &= ADXL343_Mast_FIFO_Entry;
        return tmp;
    }
    
    void ADXL343_GetXYZ(struct IAccelerometer *me, short *data)
    {
        unsigned char buffer[6];
        unsigned char i, k;
        ADXL343_ReadRegSequence(me,ADXL343_Reg_X0, buffer, 6);
        
        for(i = 0, k = 0; i < 3; i++, k = k+2)
        {
            data[i] = (buffer[k + 1] << 8) + buffer[k]; 
            data[i] = (short)(data[i] * me->G_Scale);
        }        
    }

    short ADXL343_GetX(struct IAccelerometer *me)
    {        
        return ADXL343_ReadAxisValue(me, ADXL343_Reg_X0);
    }

    short ADXL343_GetY(struct IAccelerometer *me)
    {
        return ADXL343_ReadAxisValue(me, ADXL343_Reg_Y0);
    }

    short ADXL343_GetZ(struct IAccelerometer *me)
    {
        return ADXL343_ReadAxisValue(me, ADXL343_Reg_Z0);
    }

    void ADXL343_Dispose(struct IAccelerometer *me)
    {
        me->IsDisposed = 1;
    }
#endif

void Initialize_IAccelerometer(struct IAccelerometer *me, struct ISPIM *master, unsigned char ss, float g_Scale)
{    
    me->Master = master;
    me->Pin_SS = ss;
    me->G_Scale = g_Scale;
    me->IsDisposed = 0;
    me->OnInterrupt = 0; 
    
    #ifdef Com_ADXL343
        me->Start = &ADXL343_Start;
        me->HasDevice = &ADXL343_HasDevice;
        me->HasData = &ADXL343_HasData;
        me->GetXYZ = &ADXL343_GetXYZ;
        me->GetX = &ADXL343_GetX;
        me->GetY = &ADXL343_GetY;
        me->GetZ = &ADXL343_GetZ;
        me->Dispose = &ADXL343_Dispose;
    #endif
}

void Initialize_IAccelerometerHandler(struct IAccelerometerHandler *me)
{
    int i;
    for(i = 0; i < IAccelerometer_Amount_Axis; i++)
    {
        me->HasStorage = 0;
        me->Values[i] = 0;
        me->Storage[i] = 0;
    }
}