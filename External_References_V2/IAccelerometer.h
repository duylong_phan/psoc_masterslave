#include "ISPIM.h"

#ifndef IAccelerometer_h
#define IAccelerometer_h
    
    #define IAccelerometer_Amount_Axis          3    
    
    struct IAccelerometer
    {
        unsigned char IsDisposed;
        unsigned char Pin_SS;  
        float G_Scale;
        struct ISPIM *Master;
        
        void (*Start)(struct IAccelerometer *me);
        unsigned char (*HasDevice)(struct IAccelerometer *me);
        unsigned char (*HasData)(struct IAccelerometer *me);
        void (*GetXYZ)(struct IAccelerometer *me, short *data);
        short (*GetX)(struct IAccelerometer *me);
        short (*GetY)(struct IAccelerometer *me);
        short (*GetZ)(struct IAccelerometer *me);
        void (*OnInterrupt)(struct IAccelerometer *me);
        void (*Dispose)(struct IAccelerometer *me);
    };
    
    struct IAccelerometerHandler
    {
        unsigned char HasStorage;
        short Values[IAccelerometer_Amount_Axis];
        short Storage[IAccelerometer_Amount_Axis];
    };
    
    void Initialize_IAccelerometer(struct IAccelerometer *me, struct ISPIM *master, unsigned char ss, float g_Scale);
    void Initialize_IAccelerometerHandler(struct IAccelerometerHandler *me);
#endif