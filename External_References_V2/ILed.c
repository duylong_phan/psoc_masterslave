#include "ILed.h"


void ILed_SetValue(struct ILed *me, unsigned char value)
{
    if(value == 0)
    {
        me->SetLow(me);
    }
    else
    {
        me->SetHigh(me);
    }
}

void ILed_Toggle(struct ILed *me)
{
    me->SetValue(me, !me->GetValue(me));    //NOT
}
    
#if defined(PSOC4_Mode) || defined(PSOC5_Mode)
    unsigned char ILed_PSOC_GetValue(struct ILed *me)
    {
        switch(me->ID)
        {
            case 0:         return LED_1_Read();
            case 1:         return LED_2_Read();
            default:        return 0;
        }    
    }

    void ILed_PSOC_SetHigh(struct ILed *me)
    {
        switch(me->ID)
        {
            case 0:         LED_1_Write(1);     break;
            case 1:         LED_2_Write(1);     break;
            default:        break;
        }
    }

    void ILed_PSOC_SetLow(struct ILed *me)
    {
        switch(me->ID)
        {
            case 0:         LED_1_Write(0);     break;
            case 1:         LED_2_Write(0);     break;
            default:        break;
        }
    }
#endif

void Initialize_ILed(struct ILed *me, unsigned char id, unsigned char value)
{
    me->ID = id;
    me->SetValue = &ILed_SetValue;
    me->Toggle = &ILed_Toggle;
    
    #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
        me->GetValue = &ILed_PSOC_GetValue;
        me->SetHigh = &ILed_PSOC_SetHigh;
        me->SetLow = &ILed_PSOC_SetLow;        
    #endif
    
    me->SetValue(me, value);
}