#include "ISystem.h"

struct ISystem *_service_ISystem;

#if defined(PSOC4_Mode) || defined(PSOC5_Mode)
    void PSOC_DelayMs(int amount)
    {
        CyDelay(amount);
    }

    void PSOC_DelayUs(int amount)
    {
        CyDelayUs(amount);
    }

    void PSOC_Dispose(struct ISystem *me)
    {
        me->IsDiposed = 1;
    }
#endif

#ifdef Arduino_Mode
    
#endif    

void ISystem_Reset_SignedArray(char* data, int length)
{
    int i = 0;
	for (i = 0; i < length; i++)
	{
		data[i] = 0;
	}
}

void ISystem_Reset_UnsignedArray(unsigned char* data, int length)
{
    int i = 0;
	for (i = 0; i < length; i++)
	{
		data[i] = 0;
	}
}

void Initialize_ISystem(struct ISystem *me)
{
    me->IsDiposed = 0;
    
    #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
        me->DelayMs = &PSOC_DelayMs;
        me->DelayUs = &PSOC_DelayUs;
        me->Dispose = &PSOC_Dispose;
    #endif

    #ifdef Arduino_Mode
       
    #endif
    
    me->Reset_SignedArray = &ISystem_Reset_SignedArray;
    me->Reset_UnsignedArray = &ISystem_Reset_UnsignedArray;
}

void SetService_ISystem(struct ISystem *me)
{
    _service_ISystem = me;
}

struct ISystem * GetService_ISystem()
{
    return _service_ISystem;
}