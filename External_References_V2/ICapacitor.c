#include "ICapacitor.h"

void ICapacitorMeasureController_Enter(struct ICapacitorMeasureController *me, unsigned char mode)
{
    switch(mode)
    {
        case CapacitorMeasureController_Charging:
        me->Enter_Charging(me);
        break;
        
        case CapacitorMeasureController_Discharging:
        me->Enter_Discharging(me);
        break;
        
        case CapacitorMeasureController_SPI:
        default:
        me->Enter_SPI(me);
        break;
    }
}

#if defined(PSOC4_Mode) || defined(PSOC5_Mode)
    void ICapacitorMeasureController_PSOC_Enter_SPI(struct ICapacitorMeasureController *me)
    {
        //As output pin
        SPI_MOSI_SetDriveMode(SPI_MOSI_DM_STRONG);
        //As input pin
        SPI_MISO_SetDriveMode(SPI_MISO_DM_DIG_HIZ);
        //avoid charging capacitor at beginning
        SPI_MOSI_Write(0);
        me->Status = CapacitorMeasureController_SPI;
    }

    void ICapacitorMeasureController_PSOC_Enter_Charging(struct ICapacitorMeasureController *me)
    {
        //As output pin
        SPI_MOSI_SetDriveMode(SPI_MOSI_DM_STRONG);
        //As output pin
        SPI_MISO_SetDriveMode(SPI_MOSI_DM_STRONG);  
        //Enable charge on MOSI
        SPI_MOSI_Write(1); 
        //Disable charge on MISO => this leads to voltage divider circuit
        SPI_MISO_Write(0);
        me->Status = CapacitorMeasureController_Charging;
    }

    void ICapacitorMeasureController_PSOC_Enter_Discharging(struct ICapacitorMeasureController *me)
    {
        //As Input pin, indirect disable charge on MOSI
        SPI_MOSI_SetDriveMode(SPI_MOSI_DM_DIG_HIZ);
        me->Status = CapacitorMeasureController_Discharging;
    }

    void ICapacitorMeasureController_PSOC_Dispose(struct ICapacitorMeasureController *me)
    {
        me->IsDisposed = 1;
    }
#endif

void Initialize_ICapacitor(struct ICapacitor *me, unsigned char id, unsigned char isEnabled, unsigned int *value)
{
    me->IsDisposed = 0;
    me->ID = id;
    me->IsEnabled = isEnabled;
    me->HasValue = 0;
    me->Value = value;
}  

void Initialize_ICapacitorMeasureController(struct ICapacitorMeasureController *me)
{
    me->IsDisposed = 0;
    me->Status = CapacitorMeasureController_SPI;    
    me->Enter = &ICapacitorMeasureController_Enter;
    
    #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
        me->Enter_SPI = &ICapacitorMeasureController_PSOC_Enter_SPI;
        me->Enter_Charging = &ICapacitorMeasureController_PSOC_Enter_Charging;
        me->Enter_Discharging = &ICapacitorMeasureController_PSOC_Enter_Discharging;
        me->Dispose = ICapacitorMeasureController_PSOC_Dispose;
    #endif
}