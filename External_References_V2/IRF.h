#include "ISPIM.h"
    
#ifndef IRF_h
#define IRF_h  
    
    /*Important Step
    1. Test connection with multimeter
    2. Test Power supply [1.9, 3.6]
    3. Test Device available width RFM7X_HasDevice
    4. Test Communication with RFM7X_HasData
    */
    
    //---------------Public definition-------------------
    #define RFM7X_Max_Payload               32 
    #define RFM7X_Max_Address_Length        5 
    #define RFM7X_250KBPS			        0x00
    #define RFM7X_1MBPS				        0x01
    #define RFM7X_2MBPS				        0x02
    #define RFM7X_Power_minus_10            0x00
    #define RFM7X_Power_minus_5	            0x01
    #define RFM7X_Power_0 	                0x02
    #define RFM7X_Power_Plus_5       	    0x03
    // auto retransmission delay 1ms, 15 times
    #define RFM7X_Default_Retr              0x3F    
    // data rate 250Kbps, power 5dbm, LNA gain high
    #define RFM7X_Default_RF_Setup          0x27    
    // channel 20
    #define RFM7X_Default_Channel           0x14   

    
    struct IRF
    {
        unsigned char IsDisposed;        
        unsigned char Pin_CE;
        unsigned char Pin_SS;       
        struct ISPIM *Master;        
        
        unsigned char (*StartPin)(struct IRF *me, struct ISPIM *master, unsigned char ce, unsigned char ss);
        unsigned char (*Start)(struct IRF *me, struct ISPIM *master);
        unsigned char (*HasDevice)(struct IRF *me);
        unsigned char (*HasData)(struct IRF *me);
        
        void (*WriteReg)(struct IRF *me, unsigned char reg, unsigned char data);
        void (*WriteRegBuffer)(struct IRF *me, unsigned char reg, const unsigned char *data, int length);
        unsigned char (*ReadReg)(struct IRF *me, unsigned char reg);
        void (*ReadRegBuffer)(struct IRF *me, unsigned char reg, unsigned char *data, int length);
        
        void (*Enter_Tx)(struct IRF *me);
        void (*Enter_Rx)(struct IRF *me);
        void (*Power_Up)(struct IRF *me);
        void (*Power_Down)(struct IRF *me);
        void (*GetStatus)(struct IRF *me, unsigned char *isRx, unsigned char *isOn);
        void (*SetChannel)(struct IRF *me, unsigned char channel);
        void (*SetAddress_Tx)(struct IRF *me, const unsigned char *address);
        void (*SetAddress_Rx)(struct IRF *me, const unsigned char *address);
        void (*SetAddress_Rx_Pipe)(struct IRF *me, unsigned char index,unsigned char address); 
        void (*SetPower)(struct IRF *me, unsigned char power);
        void (*SetDataRate)(struct IRF *me, unsigned char speed);
        void (*Transmit)(struct IRF *me, const unsigned char *data, int length);
        void (*Transmit_Once)(struct IRF *me, const unsigned char *data, int length);        
        unsigned char (*Receive)(struct IRF *me, unsigned char *data, unsigned char *length, unsigned char *pipe);
        void (*Flush_Rx)(struct IRF *me);
        void (*Flush_Tx)(struct IRF *me);
        void (*ClearInterrupt)(struct IRF *me);
        void (*ClearExistedInterrupt)(struct IRF *me);
        void (*GetInterrupt)(struct IRF *me, unsigned char *hasIn, unsigned char *hasOut, unsigned char *hasLost);
        
        void (*OnInterrupted)(struct IRF *me);
        void (*Dispose)(struct IRF *me);
    };
    
    void Initialize_IRF(struct IRF *me);    
#endif