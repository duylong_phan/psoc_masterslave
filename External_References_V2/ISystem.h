#include "Config.h"

#ifndef ISystem_h
#define ISystem_h 
    
    struct ISystem
    {
        unsigned char IsDiposed;
        
        void (*DelayMs)(int amount);
        void (*DelayUs)(int amount);
        void (*Reset_SignedArray)(char* data, int length);
        void (*Reset_UnsignedArray)(unsigned char* data, int length);
        void (*Dispose)(struct ISystem *me);
    };
    
    void Initialize_ISystem(struct ISystem *me);
    void SetService_ISystem(struct ISystem *me);
    struct ISystem * GetService_ISystem();
#endif
    