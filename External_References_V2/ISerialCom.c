#include "ISerialCom.h"

#ifdef PSOC5_Mode
    
    void PSOC5_ISerialCom_Start(struct ISerialCom *me)
    {
        Serial_Start();
    }     
    
    int PSOC5_ISerialCom_GetBytesToRead(struct ISerialCom *me)
    {
        return Serial_GetRxBufferSize();
    }

    void PSOC5_ISerialCom_WriteString(struct ISerialCom *me, const char *message)
    {
        Serial_PutString(message);
    }
    
    void PSOC5_ISerialCom_Write(struct ISerialCom *me, const unsigned char *data, int length)
    {
        Serial_PutArray(data, length);
    }

    void PSOC5_ISerialCom_Read(struct ISerialCom *me, unsigned char *data, int *length)
    {
        int i;
        *length = me->GetBytesToRead(me);
        for(i = 0; i < *length; i++)
        {
            data[i] = Serial_GetByte();
        }   
    }

    void PSOC5_ISerialCom_Dispose(struct ISerialCom *me)
    {
        me->IsDisposed = 1;
    }
#endif

void Initialize_ISerialCom(struct ISerialCom *me, unsigned char id)
{
    me->ID = id;
    me->IsDisposed = 0;
    
    #ifdef PSOC5_Mode
        me->Start = &PSOC5_ISerialCom_Start;        
        me->GetBytesToRead = &PSOC5_ISerialCom_GetBytesToRead;
        me->WriteString = &PSOC5_ISerialCom_WriteString;
        me->Write = &PSOC5_ISerialCom_Write;
        me->Read = &PSOC5_ISerialCom_Read;
        me->Dispose = &PSOC5_ISerialCom_Dispose;
    #endif
}