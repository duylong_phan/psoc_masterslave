#include "Config.h"
#include "ISystem.h"
    
#ifndef ISPIM_h
#define ISPIM_h
    struct ISPIM
    {       
        unsigned char IsDisposed;
        unsigned char Mode;
        
        void (*Start)(struct ISPIM *me);        
        void (*WriteReg)(struct ISPIM *me, unsigned char reg, unsigned char data, unsigned char pin);
        void (*WriteRegBuffer)(struct ISPIM *me, unsigned char reg, const unsigned char *data, int length, unsigned char pin);
        unsigned char (*ReadReg)(struct ISPIM *me, unsigned char reg, unsigned char pin);
        void (*ReadRegBuffer)(struct ISPIM *me, unsigned char reg, unsigned char *data, int length, unsigned char pin);
        void (*Dispose)(struct ISPIM *me);
    };   
    
    void Initialize_ISPIM(struct ISPIM *me, unsigned char mode, struct ISystem *system);
    
    #ifdef PSOC4_Mode  
    void PSOC4_WriteBuffer(unsigned char *data, unsigned char length, unsigned char pin);
    #endif
#endif