#include "IADC.h"

void PSOC5_IADC_Start(struct IADC *me)
{
    ADC_Start();
    ADC_StartConvert();
}

unsigned int PSOC5_IADC_GetValue(struct IADC *me, unsigned char id)
{
    return ADC_GetResult16(id);
}

void PSOC5_IADC_Dispose(struct IADC *me)
{
    me->IsDisposed = 1;
}

void Initialize_IADC(struct IADC *me)
{
    me->IsDisposed = 0;
    
    me->Start = &PSOC5_IADC_Start;
    me->GetValue = &PSOC5_IADC_GetValue;
    me->Dispose = &PSOC5_IADC_Dispose;
}