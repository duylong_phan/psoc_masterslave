#include "ISPIM.h"
#define ISPIM_Switch_Delay      2

struct ISpimHandler
{
    unsigned char SPI_DONE;    
    void (*Start)();
    void (*WriteTxData)(unsigned char data);
    void (*WriteTxArray)(const unsigned char *data, unsigned char length);
    void (*ClearRxBuffer)();
    unsigned char (*ReadTxStatus)();
    unsigned char (*ReadRxData)();    
};

struct ISpimHandler _spimContainer[4];
struct ISystem *_system;

void ISPIM_SetSS(struct ISPIM *me, unsigned char pin, unsigned char value)
{
    #ifdef PSOC5_Mode
        #ifdef SPIM_Mode_Multi
            if(Control_SPI_Bus_Read() != me->Mode)
            {                
                Control_SPI_Bus_Write(me->Mode);  
                _system->DelayUs(ISPIM_Switch_Delay);
            }
        #endif
        
        #ifdef SPIM_MultiSlave
        if(Control_SPI_SS_Read() != pin)
        {            
            Control_SPI_SS_Write(pin);
            _system->DelayUs(ISPIM_Switch_Delay);
        }
        #endif
    #endif  
    
    #if (defined(PSOC5_Mode) || defined(PSOC4_Mode)) && defined(SPIM_Pin)
        //Get Current
        unsigned char tmp = Control_SPI_SS_Read();
        //Clear
        tmp &= ~(1 << pin);
        //Set current Value
        tmp |= (value << pin);
        //Update Status
        Control_SPI_SS_Write(tmp);        
    #endif
}

unsigned char ISPIM_WriteRead(struct ISPIM *me, unsigned char data)
{
    unsigned char value = 0;    
    
    #if defined(PSOC4_Mode) && defined(SPIM_Native)
        unsigned char array[1];
        array[0] = data;
        PSOC4_WriteBuffer(array, 1, 0);
        value = SPIM_SpiUartReadRxData();
    #endif
    
    #if defined(PSOC5_Mode) && defined(SPIM_Native)
        //Write Data
        _spimContainer[me->Mode].WriteTxData(data);
        //wait for interrupt
        while((_spimContainer[me->Mode].ReadTxStatus() & _spimContainer[me->Mode].SPI_DONE) == 0)
        {
            //do nothing
        }
        //Read Data
        value = _spimContainer[me->Mode].ReadRxData();  
    #endif 
    
    #if defined(Arduino_Mode) && defined(SPIM_Native)
        value = SPI.transfer(data);
    #endif
    
    #if (defined(PSOC5_Mode) || defined(PSOC4_Mode)) && defined(SPIM_Pin)        
        unsigned char i;
        for(i = 0; i < 8; i++)
        {
            _system->DelayUs(1);
            if(data & 0x80)
            {
                SPI_MOSI_Write(1);   
            }
            else
            {
                SPI_MOSI_Write(0);
            }        
            
            _system->DelayUs(1);
            SPI_CLK_Write(1);
            
            data  = data << 1;
            data |= SPI_MISO_Read();        
            
            _system->DelayUs(2);
            SPI_CLK_Write(0);  
            SPI_MOSI_Write(0);
        }
        value = data;
    #endif
    
    return value;
}

#if  defined(PSOC5_Mode) && defined(SPIM_Native)
    void PSOC5_SPI_Start(struct ISPIM *me)
    {
        _spimContainer[me->Mode].Start();
    }   
    
    void PSOC5_SPI_WriteReg(struct ISPIM *me, unsigned char reg, unsigned char data, unsigned char pin)
    {
        unsigned char array[2] = {reg, data}; 
        
        ISPIM_SetSS(me, pin, 0);  
        //write buffer
        _spimContainer[me->Mode].WriteTxArray(array, 2);
        //wait interrupt
        while((_spimContainer[me->Mode].ReadTxStatus() & _spimContainer[me->Mode].SPI_DONE) == 0)
        {
            //do nothing
        }
        //clear Rx buffer
        _spimContainer[me->Mode].ClearRxBuffer();   
        ISPIM_SetSS(me, pin, 1);
    }

    void PSOC5_SPI_WriteRegBuffer(struct ISPIM *me, unsigned char reg, const unsigned char *data, int length, unsigned char pin)
    {
        int i = 0;
        unsigned char array[128];
        
        //create buffer
    	array[0] = reg;
    	for(i = 0; i < length; i++)
    	{
    		array[i + 1] = data[i];
    	}         
        
        ISPIM_SetSS(me, pin, 0);
         //write reg + data
        _spimContainer[me->Mode].WriteTxArray(array, length + 1);
        //wait interrupt
        while((_spimContainer[me->Mode].ReadTxStatus() & _spimContainer[me->Mode].SPI_DONE) == 0)
        {
            //do nothing
        }
        //clear Rx buffer
        _spimContainer[me->Mode].ClearRxBuffer(); 
        ISPIM_SetSS(me, pin, 1);
    }

    unsigned char PSOC5_SPI_ReadReg(struct ISPIM *me, unsigned char reg, unsigned char pin)
    {
        unsigned char value;
        unsigned char array[2] = {reg, 0x00};
        
        
        ISPIM_SetSS(me, pin, 0);
        //write buffer
        _spimContainer[me->Mode].WriteTxArray(array, 2);
        //wait interrupt
        while((_spimContainer[me->Mode].ReadTxStatus() & _spimContainer[me->Mode].SPI_DONE) == 0)
        {
            //do nothing
        }
        //discard first byte
        _spimContainer[me->Mode].ReadRxData();
        //read next
        value = _spimContainer[me->Mode].ReadRxData();
        
        ISPIM_SetSS(me, pin, 1);
        return value;
    }

    void PSOC5_SPI_ReadRegBuffer(struct ISPIM *me, unsigned char reg, unsigned char *data, int length, unsigned char pin)
    {
        int i = 0;
        unsigned char array[128];	
    	array[0] = reg;
    	for(i = 0; i < length; i++)
    	{
    		array[i + 1] = 0x00;
    	}  
        
        ISPIM_SetSS(me, pin, 0);
        //write reg + data
        _spimContainer[me->Mode].WriteTxArray(array, length + 1);
        //wait interrupt
        while((_spimContainer[me->Mode].ReadTxStatus() & _spimContainer[me->Mode].SPI_DONE) == 0)
        {
            //do nothing
        }
        //discard first byte
        _spimContainer[me->Mode].ReadRxData();
        //read data array
        for(i = 0; i < length; i++)
    	{
    		data[i] = _spimContainer[me->Mode].ReadRxData();
    	}
        
        ISPIM_SetSS(me, pin, 1);
    }

    void PSOC5_SPI_Dispose(struct ISPIM *me)
    {
        me->IsDisposed = 1;
    }
#endif

#if defined(PSOC4_Mode) && defined(SPIM_Native)
    void PSOC4_SPI_Start(struct ISPIM *me)
    {
        SPIM_Start();
    }
    
    void PSOC4_SPI_WriteBuffer(unsigned char *data, unsigned char length, unsigned char pin)
    {
        //important
        SPIM_ClearMasterInterruptSource(SPIM_INTR_MASTER_SPI_DONE);
        SPIM_SpiUartPutArray(data, length);
        while((SPIM_GetMasterInterruptSource() & SPIM_INTR_MASTER_SPI_DONE) == 0u);        
    }
    
    void PSOC4_SPI_WriteReg(struct ISPIM *me, unsigned char reg, unsigned char data, unsigned char pin)
    {
        unsigned char array[2] = {reg, data};   
        
        ISPIM_SetSS(me, pin, 0);
        //write buffer
        PSOC4_WriteBuffer(array, 2, 0); 
        //clear Rx buffer
        SPIM_SpiUartClearRxBuffer(); 
        ISPIM_SetSS(me, pin, 1);
    }

    void PSOC4_SPI_WriteRegBuffer(struct ISPIM *me, unsigned char reg, const unsigned char *data, int length, unsigned char pin)
    {
        int i = 0;
        unsigned char array[128];
        
        //create buffer
    	array[0] = reg;
    	for(i = 0; i < length; i++)
    	{
    		array[i + 1] = data[i];
    	}
            
        ISPIM_SetSS(me, pin, 0);
        //write reg + data
        PSOC4_WriteBuffer(array, length + 1, 0); 
        //clear Rx buffer
        SPIM_SpiUartClearRxBuffer();
        ISPIM_SetSS(me, pin, 1);
    }

    unsigned char PSOC4_SPI_ReadReg(struct ISPIM *me, unsigned char reg, unsigned char pin)
    {
        unsigned char value;
        unsigned char array[2] = {reg, 0x00};
        
        ISPIM_SetSS(me, pin, 0);
        PSOC4_WriteBuffer(array, 2, 0); 
        SPIM_SpiUartReadRxData();
        value = SPIM_SpiUartReadRxData();
        ISPIM_SetSS(me, pin, 1);
        return value;
    }

    void PSOC4_SPI_ReadRegBuffer(struct ISPIM *me, unsigned char reg, unsigned char *data, int length, unsigned char pin)
    {
        int i = 0;
        unsigned char array[128];	
    	array[0] = reg;
    	for(i = 0; i < length; i++)
    	{
    		array[i + 1] = 0x00;
    	}
        
        ISPIM_SetSS(me, pin, 0);
        PSOC4_WriteBuffer(array, length+1, 0); 
        //discard first byte
        SPIM_SpiUartReadRxData();  
        //read next bytes
        for(i = 0; i < length; i++)
    	{
    		data[i] = SPIM_SpiUartReadRxData();
    	}
        ISPIM_SetSS(me, pin, 1);
    }

    void PSOC4_SPI_Dispose(struct ISPIM *me)
    {
        me->IsDisposed = 1;
    }
#endif

#if defined(Arduino_Mode) && defined(SPIM_Native)
    void Arduino_SPI_Start(struct ISPIM *me)
    {
        SPIM_Start();
    }
    
    void Arduino_SPI_WriteReg(struct ISPIM *me, unsigned char reg, unsigned char data, unsigned char pin)
    {
        unsigned char array[2] = {reg, data};   
        
        ISPIM_SetSS(me, pin, 0);
        //write buffer
        SPIM_PutArray(array, 2);
        //wait interrupt
    	while((SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE) == 0)
        {
            //do nothing, wait for interrupt
        }
        //clear Rx buffer
        SPIM_ClearRxBuffer();  
        ISPIM_SetSS(me, pin, 1);
    }

    void Arduino_SPI_WriteRegBuffer(struct ISPIM *me, unsigned char reg, const unsigned char *data, int length, unsigned char pin)
    {
        int i = 0;
        unsigned char array[128];
        
        //create buffer
    	array[0] = reg;
    	for(i = 0; i < length; i++)
    	{
    		array[i + 1] = data[i];
    	}
            
        ISPIM_SetSS(me, pin, 0);
        //write reg + data
        SPIM_PutArray(array, length + 1);
        //wait interrupt
        while((SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE) == 0)
        {
            //do nothing, wait for interrupt
        }
        //clear Rx buffer
    	SPIM_ClearRxBuffer();
        ISPIM_SetSS(me, pin, 1);
    }

    unsigned char Arduino_SPI_ReadReg(struct ISPIM *me, unsigned char reg, unsigned char pin)
    {
        unsigned char value;
        unsigned char array[2] = {reg, 0x00};
        
        ISPIM_SetSS(me, pin, 0);
        //write buffer
        SPIM_PutArray(array, 2);
        //wait interrupt
    	while((SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE) == 0)
        {
            //do nothing, wait for interrupt
        }
        //discard first byte
        SPIM_ReadRxData();
        //read next
    	value = SPIM_ReadRxData();
        ISPIM_SetSS(me, pin, 1);
        return value;
    }

    void Arduino_SPI_ReadRegBuffer(struct ISPIM *me, unsigned char reg, unsigned char *data, int length, unsigned char pin)
    {
        int i = 0;
        unsigned char array[128];	
    	array[0] = reg;
    	for(i = 0; i < length; i++)
    	{
    		array[i + 1] = 0x00;
    	}
        
        ISPIM_SetSS(me, pin, 0);
        //write buffer
        SPIM_PutArray(array, length + 1);
        //wait interrupt
    	while((SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE) == 0)
        {
            //do nothing, wait for interrupt
        }
        //discard first byte
    	SPIM_ReadRxData();
        //read next bytes
        for(i = 0; i < length; i++)
    	{
    		data[i] = SPIM_ReadRxData();
    	}
        ISPIM_SetSS(me, pin, 1);
    }

    void Arduino_SPI_Dispose(struct ISPIM *me)
    {
        me->IsDisposed = 1;
    }
#endif

#ifdef SPIM_Pin
    void Pin_SPI_Start(struct ISPIM *me)
    {
        
    }
    
    void Pin_SPI_WriteReg(struct ISPIM *me, unsigned char reg, unsigned char data, unsigned char pin)
    {
        ISPIM_SetSS(me, pin, 0);
        ISPIM_WriteRead(me, reg);
        ISPIM_WriteRead(me, data);
        ISPIM_SetSS(me, pin, 1);        
    }
    
    void Pin_SPI_WriteRegBuffer(struct ISPIM *me, unsigned char reg, const unsigned char *data, int length, unsigned char pin)
    {
        int i = 0;
        ISPIM_SetSS(me, pin, 0);        
        ISPIM_WriteRead(me, reg);
        for(i = 0; i < length; i++)
        {
            ISPIM_WriteRead(me, data[i]);
        }        
        ISPIM_SetSS(me, pin, 1);
    }   
        
    unsigned char Pin_SPI_ReadReg(struct ISPIM *me, unsigned char reg, unsigned char pin)
    {
        unsigned char value = 0;
        ISPIM_SetSS(me, pin, 0);        
        
        ISPIM_WriteRead(me, reg);
        value = ISPIM_WriteRead(me, 0x00);
        
        ISPIM_SetSS(me, pin, 1);        
        return value;
    }
    
    void Pin_SPI_ReadRegBuffer(struct ISPIM *me, unsigned char reg, unsigned char *data, int length, unsigned char pin)
    {
        int i = 0;    
        ISPIM_SetSS(me, pin, 0);  
        
        ISPIM_WriteRead(me, reg);
        for(i = 0; i < length; i++)
        {
            data[i] = ISPIM_WriteRead(me, 0x00);
        }
        
        ISPIM_SetSS(me, pin, 1);  
    }
    
    void Pin_SPI_Dispose(struct ISPIM *me)
    {
        me->IsDisposed = 1;
    }
#endif

void Initialize_ISPIM(struct ISPIM *me, unsigned char mode, struct ISystem *system)
{    
    me->IsDisposed = 0;
    me->Mode = mode;  
    _system = system;  
    
    #if defined(PSOC5_Mode) && defined(SPIM_Native)
        me->Start = &PSOC5_SPI_Start;        
        me->WriteReg = &PSOC5_SPI_WriteReg;  
        me->WriteRegBuffer = &PSOC5_SPI_WriteRegBuffer;
        me->ReadReg = &PSOC5_SPI_ReadReg;
        me->ReadRegBuffer = &PSOC5_SPI_ReadRegBuffer;
        me->Dispose = &PSOC5_SPI_Dispose;
        
        #ifdef SPIM_Mode_0
            if(mode == 0)
            {
                _spimContainer[mode].SPI_DONE = SPIM_STS_SPI_DONE;               
                _spimContainer[mode].Start = &SPIM_Start;
                _spimContainer[mode].ReadTxStatus = &SPIM_ReadTxStatus;
                _spimContainer[mode].ReadRxData = &SPIM_ReadRxData;
                _spimContainer[mode].WriteTxData = &SPIM_WriteTxData;
                _spimContainer[mode].WriteTxArray = &SPIM_PutArray;
                _spimContainer[mode].ClearRxBuffer = &SPIM_ClearRxBuffer;
            }            
        #endif
        #ifdef SPIM_Mode_1
            if(mode == 1)
            {
                _spimContainer[mode].SPI_DONE = SPIM_1_STS_SPI_DONE;                
                _spimContainer[mode].Start = &SPIM_1_Start;
                _spimContainer[mode].ReadTxStatus = &SPIM_1_ReadTxStatus;
                _spimContainer[mode].ReadRxData = &SPIM_1_ReadRxData;
                _spimContainer[mode].WriteTxData = &SPIM_1_WriteTxData;
                _spimContainer[mode].WriteTxArray = &SPIM_1_PutArray;
                _spimContainer[mode].ClearRxBuffer = &SPIM_1_ClearRxBuffer;
            }            
        #endif
        #ifdef SPIM_Mode_2
            if(mode == 2)
            {
                _spimContainer[mode].SPI_DONE = SPIM_2_STS_SPI_DONE;                
                _spimContainer[mode].Start = &SPIM_2_Start;
                _spimContainer[mode].ReadTxStatus = &SPIM_2_ReadTxStatus;
                _spimContainer[mode].ReadRxData = &SPIM_2_ReadRxData;
                _spimContainer[mode].WriteTxData = &SPIM_2_WriteTxData;
                _spimContainer[mode].WriteTxArray = &SPIM_2_PutArray;
                _spimContainer[mode].ClearRxBuffer = &SPIM_2_ClearRxBuffer;  
            }            
        #endif        
        #ifdef SPIM_Mode_3
            if(mode == 3)
            {
                _spimContainer[mode].SPI_DONE = SPIM_3_STS_SPI_DONE;                
                _spimContainer[mode].Start = &SPIM_3_Start;
                _spimContainer[mode].ReadTxStatus = &SPIM_3_ReadTxStatus;
                _spimContainer[mode].ReadRxData = &SPIM_3_ReadRxData;
                _spimContainer[mode].WriteTxData = &SPIM_3_WriteTxData;
                _spimContainer[mode].WriteTxArray = &SPIM_3_PutArray;
                _spimContainer[mode].ClearRxBuffer = &SPIM_3_ClearRxBuffer;
            }            
        #endif   
    #endif  
    
    #if defined(PSOC4_Mode) && defined(SPIM_Native)
        me->Start = &PSOC4_SPI_Start;
        me->WriteReg = &PSOC4_SPI_WriteReg;  
        me->WriteRegBuffer = &PSOC4_SPI_WriteRegBuffer;
        me->ReadReg = &PSOC4_SPI_ReadReg;
        me->ReadRegBuffer = &PSOC4_SPI_ReadRegBuffer;
        me->Dispose = &PSOC4_SPI_Dispose;
    #endif   
    
    #if defined(Arduino_Mode) && defined(SPIM_Native)
        me->Start = &Arduino_SPI_Start;
        me->WriteReg = &Arduino_SPI_WriteReg;  
        me->WriteRegBuffer = &Arduino_SPI_WriteRegBuffer;
        me->ReadReg = &Arduino_SPI_ReadReg;
        me->ReadRegBuffer = &Arduino_SPI_ReadRegBuffer;
        me->Dispose = &Arduino_SPI_Dispose;
    #endif  
    
    #ifdef SPIM_Pin
        me->Start = &Pin_SPI_Start;        
        me->WriteReg = &Pin_SPI_WriteReg;  
        me->WriteRegBuffer = &Pin_SPI_WriteRegBuffer;
        me->ReadReg = &Pin_SPI_ReadReg;
        me->ReadRegBuffer = &Pin_SPI_ReadRegBuffer;
        me->Dispose = &Pin_SPI_Dispose;
    #endif
}