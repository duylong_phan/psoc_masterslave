#include <stdio.h>
#include <string.h>

#ifndef Queue_h
#define Queue_h
    
    struct Queue
    {
        //Inner Container for Queue
        unsigned char *Container;
        //Max Amount of Item
        int MaxAmount;
        //Size of given Item in Bytes
        int TypeSize;
        //available Item in Queue
        int Count;
        //Important Index
        int StartIndex, EndIndex;
    };

    struct QueueInfo
    {
        unsigned char IsValid;
        int Count;
        int StartIndex;
        int EndIndex;
    };
    
    //---------------------------Public Method---------------------------------
	/// <summary>
	/// Initialize given Queue Instance
	/// </summary>
	/// <param name="queue">given instance</param>
	/// <param name="container">data container</param>
	/// <param name="maxAmount">given maxAmount</param>
	/// <param name="typeSize">item size in byte</param>
    void Queue_Initialize(struct Queue *queue, unsigned char* container, int maxAmount, int typeSize);    
	
	/// <summary>
	/// Add item to end of Queue
	/// </summary>
	/// <param name="queue">given instance</param>
	/// <param name="item">new item</param>
    void Queue_Enqueue(struct Queue *queue, void *item);
	
	/// <summary>
	/// Return the begin Item of Queue and move to the next item
	/// </summary>
	/// <param name="queue">given instance</param>	
    void* Queue_Dequeue(struct Queue *queue);
	
	/// <summary>
	/// Return the begin Item of Queue and stay there
	/// </summary>
	/// <param name="queue">given data</param>	
    void* Queue_Peek(struct Queue *queue);
    
    void QueueInfo_Initialize(struct QueueInfo *info);
    void QueueInfo_SetInfo(struct QueueInfo *info, struct Queue *queue);
    void QueueInfo_SetQueue(struct QueueInfo *info, struct Queue *queue);
#endif
