#include "PinHandler.h"

void PinHandler_Initialize(struct PinHandler *handler, unsigned char id, unsigned char setBit)   
{
    handler->ID = id; 
    handler->SetBit = setBit;
    handler->ResetBit = 0xFF^setBit;
    handler->Count = 0;
    handler->Command = 0;    
}