#include "HeaderPackage.h"

//---------------Private Field-----------------
struct UsbHandler* HeaderPackage_usbHandler;


//---------------Public Method----------------
void HeaderPackage_Intialize(struct HeaderPackage *package, unsigned char* container)
{
    //Parameter
    Queue_Initialize(&package->Queue, container, Size_HeaderContainer, sizeof(unsigned char));    
    package->HasHeader = 0;
    package->HasLength = 0;
    package->HeaderCount = 0;
    package->Type = 0;
    package->Length = 0;
}

void HeaderPackage_SetUsbHandler(struct UsbHandler *usbHandler)
{
    HeaderPackage_usbHandler = usbHandler;
}

void HeaderPackage_WritePackage(const unsigned char *data, int length)
{    
    unsigned char header[4] = {'#','#','#','#'};    
    
    #ifdef PSOC4_Serial
        Serial_SpiUartPutArray(header, 4); 
        Serial_SpiUartPutArray(data, length);
    #endif
    
     #ifdef PSOC5_Serial
        Serial_PutArray(header, 4); 
        Serial_PutArray(data, length);
    #endif
    
    #ifdef PSOC5_USB
        int i;
        //write header
        for(i = 0; i < 4; i++)
        {
            if(*HeaderPackage_usbHandler->Length >= PSOC5_USB_Size_PayLoad)
            {
                HeaderPackage_Flush();
            }
            HeaderPackage_usbHandler->TransmitPackage[*HeaderPackage_usbHandler->Length] = header[i];
            (*HeaderPackage_usbHandler->Length)++;
        }
    
        //write Payload
        for(i = 0; i < length; i++)
        {
            if(*HeaderPackage_usbHandler->Length >= PSOC5_USB_Size_PayLoad)
            {
                HeaderPackage_Flush();
            }
            HeaderPackage_usbHandler->TransmitPackage[*HeaderPackage_usbHandler->Length] = data[i];
            (*HeaderPackage_usbHandler->Length)++;
        }
    #endif
}

void HeaderPackage_Flush()
{
    #ifdef PSOC4_Serial
        //Do nothing
    #endif
    
    #ifdef PSOC5_Serial
        //Do nothing
    #endif
    
    #ifdef PSOC5_USB
        while(USBFS_GetEPState(PSOC5_USB_In_EP) != USBFS_IN_BUFFER_EMPTY) {}
        USBFS_LoadInEP(PSOC5_USB_In_EP, HeaderPackage_usbHandler->TransmitPackage, PSOC5_USB_Size_Package);
        *HeaderPackage_usbHandler->Length = 0;
    #endif
}

unsigned char HeaderPackage_Parse(struct HeaderPackage *package)
{
    unsigned char hasPackage = 0;
    unsigned char *item = NULL;
    int i = 0;
    
    while(package->Queue.Count > 0 || package->HasLength)
    {
        if(package->HasLength)
        {
            //enough Payload Length
            if(package->Queue.Count >= package->Length)
            {
                for(i = 0; i < package->Length; i++)
                {
                    package->PayLoad[i] = *(unsigned char*)Queue_Dequeue(&package->Queue);
                }
                hasPackage = 1;
                
                //reset
                package->HasLength = 0;                
            }
            //wait new buffer, or process current Package
            break;
        }
        else if(package->HasHeader)
        {
            //enough Type and Length
            if(package->Queue.Count > 1)
            {
                //set
                package->HasLength = 1;
                package->Type = *(unsigned char*)Queue_Dequeue(&package->Queue);
                package->Length = *(unsigned char*)Queue_Dequeue(&package->Queue);
                
                //reset
                package->HasHeader = 0;
            }
            //wait new buffer
            else
            {
                break;
            }         
        }
        else
        {
            //get Byte
            item = (unsigned char*)Queue_Dequeue(&package->Queue);
            //is Header
            if(*item == HeaderPackage_Indicator)
            {
                package->HeaderCount++;
                //Header is available
                if(package->HeaderCount >= 4)
                {
                    package->HasHeader = 1;
                    package->HeaderCount = 0;
                }
            }
            //No Header
            else
            {
                package->HeaderCount = 0;
            }
        }
    }    
    return hasPackage;
}