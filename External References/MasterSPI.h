#include "Config.h"
#include "SubFunctions.h"
   
#ifdef Arduino_Mode    
    #include <SPI.h>
#endif

#ifndef MasterSPI_h
#define MasterSPI_h
//Name conversion
//PSOC4, PSOC5              => SPIM
//PSOC5 Slave control       => SS_Control
    
/// <summary>
/// Write given data and read a byte over SPI Interface
/// </summary>
/// <param name="data">given data</param>
unsigned char MasterSPI_WriteRead(unsigned char data);

/// <summary>
/// Enable Slave with given Pin
/// </summary>
/// <param name="pin">given pin</param>
void MasterSPI_Set_SS(unsigned char pin);

/// <summary>
/// Disable Slave with given Pin
/// </summary>
/// <param name="pin">given pin</param>
void MasterSPI_Reset_SS(unsigned char pin);

/// <summary>
/// Enable slave and Write a Register and its value over SPI
/// </summary>
/// <param name="reg">given Register</param>
/// <param name="data">given data</param>
/// <param name="pin">given pin</param>
void MasterSPI_Write_Reg(unsigned char reg, unsigned char data, unsigned char pin);

/// <summary>
/// Enable slave and Write a Register and its buffer over SPI
/// </summary>
/// <param name="reg">given Register</param>
/// <param name="data">given data</param>
/// <param name="pin">given pin</param>
void MasterSPI_Write_Reg_Buffer(unsigned char reg, const unsigned char *data, int length, unsigned char pin);

/// <summary>
/// Enable slave and read Value of given Register
/// </summary>
/// <param name="reg">given Register</param>
/// <param name="pin">given pin</param>
unsigned char MasterSPI_Read_Reg(unsigned char reg, unsigned char pin);

/// <summary>
/// Enable slave and read Buffer of given Register
/// </summary>
/// <param name="reg">given Register</param>
/// <param name="data">given data</param>
/// <param name="length">given length</param>
/// <param name="pin">given pin</param>
void MasterSPI_Read_Reg_Buffer(unsigned char reg, unsigned char* data, int length, unsigned char pin);

#endif