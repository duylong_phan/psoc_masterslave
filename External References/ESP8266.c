#include "ESP8266.h"

#define Size_ESP8266_Expression     4

struct ESP8266Expression ESP8266_Expressions[Size_ESP8266_Expression];
char ESP8266_Expression_OK[] = "OK";
char ESP8266_Expression_Error[] = "ERROR";
char ESP8266_Expression_HasRequest[] = "+IPD";
char ESP8266_Expression_WaitResponse[] = "OK\r\n>";


//-------------------Software API-----------------------------
void ESP8266Expression_Initialize(struct ESP8266Expression *expression, char *text, unsigned char result)
{        
    expression->Text = text;
    expression->Result = result;
    expression->Count = 0;
    expression->Length = 0;
    while(text[expression->Length] != '\0')
    {
        expression->Length++;
    }
}

void ESP8266Handler_Initialize(struct ESP8266Handler *handler, unsigned char *dataContainer, unsigned char inputPackageCount)
{
    HeaderPackage_Intialize(&handler->InputPackage, dataContainer);
    handler->InputPackageCount = inputPackageCount;
    
    ESP8266Expression_Initialize(&ESP8266_Expressions[0], ESP8266_Expression_OK, ESP8266_Status_Ok);
    ESP8266Expression_Initialize(&ESP8266_Expressions[1], ESP8266_Expression_Error, ESP8266_Status_Error);
    ESP8266Expression_Initialize(&ESP8266_Expressions[2], ESP8266_Expression_HasRequest, ESP8266_Status_HasRequest);
    ESP8266Expression_Initialize(&ESP8266_Expressions[3], ESP8266_Expression_WaitResponse, ESP8266_Status_WaitResponse);  
    handler->Expressions = ESP8266_Expressions;
    handler->ExpressionCount = Size_ESP8266_Expression;
    
    handler->RequestInfo[0] = '+';
    handler->RequestInfo[1] = 'I';
    handler->RequestInfo[2] = 'P';    
    handler->RequestIndex = 3;
    handler->WaitRequestInfo = 0;
    handler->Pipe = 0;
    handler->Length = 0;   
}

void ESP8266Handler_GetRequestInfo(struct ESP8266Handler *handler, unsigned char value)
{
    handler->RequestInfo[handler->RequestIndex] = value;
    handler->RequestIndex++;
}

void ESP8266Handler_ParseRequestInfo(struct ESP8266Handler *handler)
{
    sscanf(handler->RequestInfo, "+IPD,%hu,%hu:", &handler->Pipe, &handler->Length);
    handler->RequestIndex = 3;
}
   
void ESP8266_ProcessInput(struct ESP8266Handler *handler, unsigned char value)
{
    unsigned char status;
    if(handler->Length == 0)
    {
        status = ESP8266_Parse(handler, value );
        if(status == ESP8266_Status_HasRequest)
        {
            handler->WaitRequestInfo = 1;
        }
        if(handler->WaitRequestInfo)
        {
            ESP8266Handler_GetRequestInfo(handler, value); 
            if(value == ':')
            {
                ESP8266Handler_ParseRequestInfo(handler);
                handler->WaitRequestInfo = 0;
            }
        }
    }
    else
    {
        handler->Length--;
        Queue_Enqueue(&handler->InputPackage.Queue, &value);            
    }
}

//--------------------Hardware API-----------------------------
void ESP8266_CheckInput(struct ESP8266Handler *handler)
{
    #ifdef PSOC4_Serial
        while(Serial_SpiUartGetRxBufferSize() > 0)
        {
            //Avoid overflow buffer, inplicit for InputPackage to process input command
            if(handler->InputPackage.Queue.Count >= handler->InputPackageCount)
            {
                break;
            }
            //Get Data from Serial Interface
            ESP8266_ProcessInput(handler, Serial_UartGetChar()); 
        }
    #endif
}

void ESP8266_WriteOut(const char *command)
{
    #ifdef PSOC4_Serial
        Serial_UartPutString(command);
    #endif    
}

void ESP8266_WriteOutBuffer(const unsigned char *buffer, int length)
{
    #ifdef PSOC4_Serial
        Serial_SpiUartPutArray(buffer, length);
    #endif 
}

unsigned char ESP8266_Parse(struct ESP8266Handler *handler, unsigned char value)
{
    int i;
    unsigned char result = ESP8266_Status_Non;
    struct ESP8266Expression *expression;
    for(i = 0; i < handler->ExpressionCount; i++)
    {
        expression = &handler->Expressions[i];
        if(expression->Text[expression->Count] == value)
        {
            expression->Count++;
        }
        else
        {
            expression->Count = 0;
        }
        if(expression->Count >= expression->Length)
        {
            expression->Count = 0;
            result = expression->Result;
            break;
        }
    }
    
    return result;
}

void ESP8266_Reset()
{
    ESP8266_WriteOut("AT+RST\r\n");
}

void ESP8266_CheckDevice()
{
    ESP8266_WriteOut("AT\r\n");
}

void ESP8266_SetEcho(unsigned char value)
{
    switch(value)
    {
        case ESP8266_Echo_Disable:      ESP8266_WriteOut("AT0\r\n");      break;
        case ESP8266_Echo_Enable:
        default:                        ESP8266_WriteOut("AT1\r\n");      break;
    }
}

void ESP8266_SetMode(unsigned char mode)
{
    char command[64];
    sprintf(command, "AT+CWMODE_CUR=%d\r\n", mode);
    ESP8266_WriteOut(command);
}

void ESP8266_SetIpAddress(const char *address)
{
    char command[64];
    sprintf(command, "AT+CIPAP_CUR=\"%s\"\r\n", address);
    ESP8266_WriteOut(command);
}

void ESP8266_SetMacAddress(const char *address)
{
    char command[64];
    sprintf(command, "AT+CIPAPMAC_CUR=\"%s\"\r\n", address);
    ESP8266_WriteOut(command);
}

void ESP8266_SetMuxConnection(unsigned char value)
{
    switch(value)
    {
        case 0:     ESP8266_WriteOut("AT+CIPMUX=0\r\n");    break;
        case 1:
        default:    ESP8266_WriteOut("AT+CIPMUX=1\r\n");    break;   
    }
}

void ESP8266_TurnWlanOn(const char *ssid, const char *pass, unsigned char channel, unsigned char encode)
{
    char command[128];
    sprintf(command, "AT+CWSAP_CUR=\"%s\",\"%s\",%d,%d\r\n", ssid, pass, channel, encode);
    ESP8266_WriteOut(command);
}

void ESP8266_StartWebServer(unsigned short port)
{
    char command[64];
    sprintf(command, "AT+CIPSERVER=1,%d\r\n", port);
    ESP8266_WriteOut(command);
}

void ESP8266_StopWebServer(unsigned short port)
{
    char command[64];
    sprintf(command, "AT+CIPSERVER=0,%d\r\n", port);
    ESP8266_WriteOut(command);
}

void ESP8266_WriteResponse(struct ESP8266Handler *handler, unsigned char *buffer, int length)
{
    unsigned char status;
    char command[32];
    char line1[] = "HTTP/1.0 200 OK\r\n";
    char line2[] = "Content-Type: multipart/form-data; boundary=---------------\r\n";
    char line3[48];
    int i, totalLength;
    
    sprintf(line3, "Content-Length: %d\r\n\r\n", length);
        
    totalLength = 0;
    for(i = 0; ; i++)
    {
        if(line1[i] == '\0')
        {
            break;
        }
    }
    totalLength += i;
    
    for(i = 0; ; i++)
    {
        if(line2[i] == '\0')
        {
            break;
        }
    }    
    totalLength += i;
    
    for(i = 0; ; i++)
    {
        if(line3[i] == '\0')
        {
            break;
        }
    }
    totalLength += i;
    totalLength += length;    
    sprintf(command, "AT+CIPSEND=%d,%d\r\n", handler->Pipe, totalLength);
    
    ESP8266_WriteOut(command);
    Sub_DelayMs(20);
    
    ESP8266_WriteOut(line1);
    ESP8266_WriteOut(line2);
    ESP8266_WriteOut(line3);    
    ESP8266_WriteOutBuffer(buffer, length);
}