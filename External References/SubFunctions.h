#include "Config.h"
#ifndef SubFunctions_h
#define SubFunctions_h
    /// <summary>
	/// Delay the execution for given miliSecond
	/// </summary>
	/// <param name="miliSecond">given miliSecond</param>	
    void Sub_DelayMs(int miliSecond);
	
	/// <summary>
	/// Delay the execution for given microSecond
	/// </summary>
	/// <param name="microSecond">given microSecond</param>
    void Sub_DelayUs(int microSecond);

    /// <summary>
	/// Return checksum XOR for given data container
	/// </summary>
	/// <param name="data">given data container</param>
	/// <param name="length">container length</param>
    unsigned char Sub_GetCheckSum(unsigned char* data, int length);
	
	/// <summary>
	/// Set all item in data container to 0
	/// </summary>
	/// <param name="data">given data container</param>
	/// <param name="length">container length</param>
    void Sub_ResetArray_Unsigned(unsigned char* data, int length);
	
	/// <summary>
	/// Set all item in data container to 0
	/// </summary>
	/// <param name="data">given instance</param>
	/// <param name="data">new item</param>
    void Sub_ResetArray_Signed(char* data, int length);
#endif