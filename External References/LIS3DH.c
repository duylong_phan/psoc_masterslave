#include "LIS3DH.h"

//-------------------Private Function----------------
void LIS3DH_WriteValue(unsigned char reg, unsigned char value, unsigned char pin)
{
	reg |= LIS3DH_Com_Write;
    MasterSPI_Write_Reg(reg, value, pin);
}

unsigned char LIS3DH_ReadValue(unsigned char reg, unsigned char pin)
{
	reg |= LIS3DH_Com_Read;
	return MasterSPI_Read_Reg(reg, pin);;
}


//------------------------API Function----------------------
void LIS3DH_Start(unsigned char pin)
{	
    //enable 3 axis, frequency = 5000
	unsigned char reg1 = LIS3DH_Reg1_EnX | LIS3DH_Reg1_EnY | LIS3DH_Reg1_EnZ | LIS3DH_Reg1_5000;
    //enable high resolution, 16G
	unsigned char reg4 = LIS3DH_Reg4_HighRes | LIS3DH_Reg4_16G;	
    
    //wait to start up
    Sub_DelayMs(10);
    
    //all register shoulbe initialized
	LIS3DH_WriteValue(LIS3DH_TEMP_CFG, 0x00, pin);
	LIS3DH_WriteValue(LIS3DH_Reg1, reg1, pin);
	LIS3DH_WriteValue(LIS3DH_Reg2, 0x00, pin);
	LIS3DH_WriteValue(LIS3DH_Reg3, 0x00, pin);
	LIS3DH_WriteValue(LIS3DH_Reg4, reg4, pin);
	LIS3DH_WriteValue(LIS3DH_Reg5, 0x00, pin);
	LIS3DH_WriteValue(LIS3DH_Reg6, 0x00, pin);
}

unsigned char LIS3DH_HasDevice(unsigned char pin)
{
    unsigned char value;
    value = LIS3DH_ReadValue(LIS3DH_Who_Am_I, pin);
    if(value == 0x33)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

short LIS3DH_ReadParameter(unsigned char valueType, unsigned char pin)
{
	short value = 0;
	short high = 0;
	short low = 0;		

	switch (valueType)
	{
		case LIS3DH_Value_X:			
			high = LIS3DH_ReadValue(LIS3DH_X_H, pin);			
			low = LIS3DH_ReadValue(LIS3DH_X_L, pin);		
			break;

		case LIS3DH_Value_Y:					
			high = LIS3DH_ReadValue(LIS3DH_Y_H, pin);			
			low = LIS3DH_ReadValue(LIS3DH_Y_L, pin);			
			break;

		case LIS3DH_Value_Z:
		default:
			high = LIS3DH_ReadValue(LIS3DH_Z_H, pin);
			low = LIS3DH_ReadValue(LIS3DH_Z_L, pin);
			break;
	}
    
	//combine 2 value
	value = (high << 8) | low;
	return value;
}