#ifndef LIS3DH_h
#define LIS3DH_h
#include "MasterSPI.h"

/*
Error Handler
Name	Pin
MOSI	6
MISO	7
SS		8				has to be 0V for SPI
Vdd		3.3V
Vdd_IO	3.3V			has to be connected
GND		0V

Problem during programming
Chip is broken
High, Low Register wrong
All register should be override => old setting
Skip ADC and Temp
*/

#define LIS3DH_Who_Am_I			0x0F
#define LIS3DH_ADC1_L			0x08
#define LIS3DH_ADC1_H			0x09
#define LIS3DH_ADC2_L			0x0A
#define LIS3DH_ADC2_H			0x0B
#define LIS3DH_ADC3_L			0x0C
#define LIS3DH_ADC3_H			0x0D
#define LIS3DH_X_L				0x28
#define LIS3DH_X_H				0x29
#define LIS3DH_Y_L				0x2A
#define LIS3DH_Y_H				0x2B
#define LIS3DH_Z_L				0x2C
#define LIS3DH_Z_H				0x2D

#define LIS3DH_Com_Read			0x80
#define LIS3DH_Com_Write		0x00

#define LIS3DH_TEMP_CFG			0x1F
#define LIS3DH_Reg1				0x20
#define LIS3DH_Reg2				0x21
#define LIS3DH_Reg3				0x22
#define LIS3DH_Reg4				0x23
#define LIS3DH_Reg5				0x24
#define LIS3DH_Reg6				0x25

#define LIS3DH_CFG_EnableADC	0x80
#define LIS3DH_CFG_EnableTemp	0x40

#define LIS3DH_Reg1_EnX			0x01
#define LIS3DH_Reg1_EnY			0x02
#define LIS3DH_Reg1_EnZ			0x04
#define LIS3DH_Reg1_LowPower	0x08
#define LIS3DH_Reg1_Off			0x00
#define LIS3DH_Reg1_1			0x10
#define LIS3DH_Reg1_10			0x20
#define LIS3DH_Reg1_25			0x30
#define LIS3DH_Reg1_50			0x40
#define LIS3DH_Reg1_100			0x50
#define LIS3DH_Reg1_200			0x60
#define LIS3DH_Reg1_400			0x70
#define LIS3DH_Reg1_1600		0x80
#define LIS3DH_Reg1_5000		0x90

#define LIS3DH_Reg4_HighRes		0x08
#define LIS3DH_Reg4_SPI3		0x01
#define LIS3DH_Reg4_2G			0x00
#define LIS3DH_Reg4_4G			0x10
#define LIS3DH_Reg4_8G			0x20
#define LIS3DH_Reg4_16G			0x30

//parameter
#define LIS3DH_Value_X			0x00
#define LIS3DH_Value_Y			0x01
#define LIS3DH_Value_Z			0x02

void LIS3DH_Start(unsigned char pin);
unsigned char LIS3DH_HasDevice(unsigned char pin);
short LIS3DH_ReadParameter(unsigned char valueType, unsigned char pin);

#endif