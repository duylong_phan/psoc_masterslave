#ifndef PinHandler_h
#define PinHandler_h    
    struct PinHandler
    {
        unsigned char ID;        
        unsigned char Command;
        unsigned short Count;
        unsigned short Duration;
        unsigned char SetBit;
        unsigned char ResetBit;
    };
    
    void PinHandler_Initialize(struct PinHandler *handler, unsigned char id, unsigned char setBit);    
#endif