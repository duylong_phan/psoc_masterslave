
#include <project.h>

#define Header_Start            'a'
#define Header_Stop             'o'
#define CLock_Frequency         1000000

unsigned char Start_PWM()
{
    Rect_PWM_Start();
    LED_Write(1);
                    
    return 1;
}
unsigned char Stop_PWM()
{
    Rect_PWM_Stop();
    LED_Write(0);
   return 0; 
}

int main()
{
    unsigned char command;   
    unsigned short frequency;
    unsigned short period, compare;
    unsigned char isStarted = 0;
    unsigned char width = 0;
    
    CyGlobalIntEnable; 
    Serial_Start();
    
    for(;;)
    {
        if(Serial_SpiUartGetRxBufferSize() > 2)
        {
            command = Serial_UartGetByte();
            switch(command)
            {          
                //Frequency
                case 1:
                    frequency = (Serial_UartGetByte() << 8) + Serial_UartGetByte();                    
                    period = CLock_Frequency / frequency;                    
                    compare = period/2 - 1;
                    
                    Rect_PWM_WritePeriod(period);
                    Rect_PWM_WriteCompare(compare);
                break;
                
                //Duty cycle
                case 0:
                default:
                    //Get width
                    Serial_UartGetByte();
                    width = 100 - Serial_UartGetByte(); 
                    period = Rect_PWM_ReadPeriod();
                    //calculate Compare
                    compare = (width/100.0)* period;
                    Rect_PWM_WriteCompare(compare);
                break;
            }
        }
        
        if(Trigger_Pin_Read() && isStarted == 0)
        {
            isStarted = Start_PWM();              
        }
        if(Trigger_Pin_Read() == 0 && isStarted)
        {
            isStarted = Stop_PWM();
        }
    }
}

//                switch(command)
//                {
//                    case Header_Start:
//                    if(isStarted == 0)
//                    {
//                        isStarted = Start_PWM();
//                    }                    
//                    break;
//                    
//                    case Header_Stop:
//                    if(isStarted > 0)
//                    {
//                        isStarted = Stop_PWM();
//                    }                    
//                    break;
//                    
//                    default:
//                    //No thing
//                    break;
