#include "project.h"

int main(void)
{    
    CyGlobalIntEnable;   
    CyDelay(500);
    
    //Execute App 1
    if(Command_1_Read() == 0)
    {
        Bootloader_Exit(Bootloader_EXIT_TO_BTLDB_1);
    }
    //Execute App 2
    else if(Command_2_Read() == 0)
    {
        Bootloader_Exit(Bootloader_EXIT_TO_BTLDB_2);
    }
    //Write Bootloadable App
    else
    {   
        PWM_Start();
        Serial_Start();
        Bootloader_Start();        
    }
}
