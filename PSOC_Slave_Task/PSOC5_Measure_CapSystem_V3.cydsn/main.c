#include "Config.h"
#include "../../External_References_V2/IDefinitions.h"
#include "../../External_References_V2/IQueue.h"
#include "../../External_References_V2/ISystem.h"
#include "../../External_References_V2/IRF.h"
#include "../../External_References_V2/ITimer.h"
#include "../../External_References_V2/ILed.h"
#include "../../External_References_V2/ICapacitor.h"
#include "../../External_References_V2/IADC.h"

/*Program MicroController
via Debug Interface: 
+ plug in Cable 
+ remove BootLoadable from TopDesign.cysch (Important)
+ Click Program Button or Ctrl+F5

via UART Interface: 
+ (BootLoadable muss be available in TopDesign.cysch)
+ Open Menu/Tool/BootLoader Host
+ Select COMX, Baudrate 115200
+ Select *_2.cydsn from CortexM3\ARM_GCC_493\Debug
+ Command1 => off && Command2 => off
+ Power up the System
+ Click Program Button on BootLoader Host
+ Power down, Command2 => on, Power on => ready to use
*/


//--------------------------Local Define--------------------
#define System_Amount_Cap               4
#define System_Amount_Led               2
#define System_Cap_Charged              1035
#define System_Cap_Discharged           2
#define System_Duration_Factor          2
    
//Extra Operation state
#define State_EnableCharge              8
#define State_Charging                  9
#define State_EnableDischarge           10
#define State_Discharging               11

union CapacitorResult
{
    unsigned char Array[Size_Cap_Sample];
    unsigned int Value[System_Amount_Cap];
};

//--------------------------Source--------------------------
unsigned char __stateContainer[Size_StateContainer];
struct ILed __ledContainer[System_Amount_Led];
struct ICapacitor  __capContainer[System_Amount_Cap];
union CapacitorResult __capResults;
struct ISystem __system;
struct ISPIM __spim;
struct IADC __adc;
struct IRF __rf;
struct IQueue __stateQueue;
struct RfHandler __rfHandler;
struct ITimer __timer;
struct ICapacitorMeasureController __capController;

//---------------------------Global Variable--------------------
struct ISystem *_system;
struct ISPIM *_spim;
struct IADC *_adc;
struct IRF *_rf;
struct ILed *_led1, *_led2;
struct RfHandler *_rfHandler;
struct IQueue *_stateQueue;
struct ITimer *_timer;
struct ICapacitorMeasureController *_capController;
unsigned char _waitRF;

unsigned char RF_Channel = 30;
unsigned char RF_Rx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};
unsigned char RF_Tx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};

//----------------------------Prototype Method--------------------
void OnTimer_MiliSecondTick(struct ITimer *me);
void OnRf_Interrupted(struct IRF *me);
void Do_EnableCharge();
void Do_Charging();
void Do_EnableDischarge();
void Do_Discharging();

//----------------------------Main Method--------------------
int main(void)
{
    Initialize_SW();
    Initialize_HW();    
    for(;;)
    {
        Single_Operation();
    }
}

//----------------------------Interrupt Method--------------------
CY_ISR(Triggered_RF_Inter)
{
    _rf->OnInterrupted(_rf);
}

CY_ISR(Triggered_Timer_Inter)
{
    _timer->TickCount++;
    _timer->OnMiliSecondTick(_timer);
}

//----------------------------Initialization Method--------------------
void Initialize_SW()
{
    int i = 0;
    //Imporant => enable System to charge capacitors
    unsigned char next = State_EnableCharge; 
    
    //System
    _system = &__system;
    Initialize_ISystem(_system);    
    SetService_ISystem(_system);
        
    //SPI Master
    _spim = &__spim;    
    Initialize_ISPIM(_spim, 0, _system);    
            
    //ADC
    _adc = &__adc;
    Initialize_IADC(_adc);    
    
    //Timer
    _timer = &__timer;
    Initialize_ITimer(_timer);   
    _timer->OnMiliSecondTick = &OnTimer_MiliSecondTick;
                
    //RF Module
    _rf = &__rf;
    Initialize_IRF(_rf);
    _rf->OnInterrupted = &OnRf_Interrupted;
    
    //Capacitor
    for(i = 0; i < System_Amount_Cap; i++)
    {
        Initialize_ICapacitor(&__capContainer[i], i, 1, &__capResults.Value[i]);
    } 
    _system->Reset_UnsignedArray(__capResults.Array, Size_Cap_Sample);
    _capController = &__capController;
    Initialize_ICapacitorMeasureController(_capController); 
    
    //Led
    for(i = 0; i < System_Amount_Led; i++)
    {
       Initialize_ILed(&__ledContainer[i], i, 0);  
    }  
    _led1 = &__ledContainer[0];
    _led2 = &__ledContainer[1];
    
    //Queue
    _stateQueue = &__stateQueue;    
    Initialize_IQueue(_stateQueue, __stateContainer, Size_StateContainer, sizeof(unsigned char)); 
    
    //RF Handler
    _rfHandler = &__rfHandler;
    _rfHandler->Pipe = 0;
    _rfHandler->Length = 0;
    _rfHandler->LostCount = 0;
    _rfHandler->Mode = RF_Transmitter;
    _system->Reset_UnsignedArray(_rfHandler->TransmitPackage, Size_RF_Package);
    _system->Reset_UnsignedArray(_rfHandler->ReceivePackage, Size_RF_Package); 
    
    _waitRF = 0;
    _stateQueue->Enqueue(_stateQueue, &next);    
}

void Initialize_HW()
{
    CyGlobalIntEnable;   
        
    //Capacitor    
    _capController->Enter_SPI(_capController);
    
    //SPI
    _spim->Start(_spim);  
    
    //ADC
    _adc->Start(_adc);
    
    //Counter    
    Duration_Counter_Start();
    
    //RF    
    _rf->StartPin(_rf, _spim, 0, 0); 
    _rf->Enter_Tx(_rf);
    _rf->SetChannel(_rf, RF_Channel);
    _rf->SetAddress_Tx(_rf, RF_Tx_Address);
    _rf->SetAddress_Rx(_rf, RF_Rx_Address);     
    
    //Enable RF Interrrupt Pin tracking
    Inter_RF_StartEx(Triggered_RF_Inter);
    
    //Enable Timer tracking
    Inter_Timer_StartEx(Triggered_Timer_Inter);
    Timer_PWM_Start();
}

void Single_Operation()
{    
    unsigned char state = (_stateQueue->Count > 0) ? *(unsigned char*)_stateQueue->Dequeue(_stateQueue) :
                                                     State_Wait_Input;
    switch(state)
    {   
        case State_RF_Transmit:         Do_RF_Transmit();       break;        
        case State_Generate_Output:     Do_Generate_Output();   break;        
        case State_EnableCharge:        Do_EnableCharge();      break;            
        case State_Charging:            Do_Charging();          break;            
        case State_EnableDischarge:     Do_EnableDischarge();   break;        
        case State_Discharging:         Do_Discharging();       break;
        case State_Wait_Input:
        default:                        Do_Wait_Input();        break;
    }    
}

//---------------------------Operation Method--------------------
void OnTimer_MiliSecondTick(struct ITimer *me)
{
    unsigned char next = State_Wait_Input;    
    
    //Imporant => enable System to charge the capacitor
    if(_waitRF && me->TickCount > 20)
    {
        _waitRF = 0;
        next = State_EnableCharge;
        _stateQueue->Enqueue(_stateQueue, &next); 
    }
    me->TickCount++;
}

void OnRf_Interrupted(struct IRF *me)
{
    //interrupt is ignored 
}

void Do_RF_Transmit()
{ 
    _rf->ClearInterrupt(_rf);   
    _rf->Transmit(_rf, _rfHandler->TransmitPackage, Size_RF_Package);
    //set
    _timer->TickCount = 0;
    _waitRF = 1;
    _led2->SetHigh(_led2);
}

void Do_Generate_Output()
{
     unsigned char nextState = State_RF_Transmit; 
    
    //Reset   
    _capController->Enter_SPI(_capController);
    _system->Reset_UnsignedArray(_rfHandler->TransmitPackage, Size_RF_Package);
    
    //generate package
    _rfHandler->TransmitPackage[Index_TaskID] = TaskID_CapMeasure;
    _rfHandler->TransmitPackage[Index_Type] = Type_Cap_Done;
    memcpy(&_rfHandler->TransmitPackage[Index_Payload], __capResults.Array, Size_Cap_Sample);
        
    
    //request transmit data
    _stateQueue->Enqueue(_stateQueue, &nextState);
}

void Do_EnableCharge()
{
    unsigned char hasIn, hasOut, hasLost; 
    unsigned char next = State_Charging;
    int i = 0;
    
    //Check if Package is transmitted successfully
    _rf->GetInterrupt(_rf, &hasIn, &hasOut, &hasLost);
    if(hasOut)
    {
        _led2->SetLow(_led2);
    }    
    
    //Reset Cap Data
    for(i = 0; i < System_Amount_Cap; i++)
    {
        __capContainer[i].HasValue = 0;
        *(__capContainer[i].Value) = 0;
    }
    
    //Reset Counter
    Duration_Counter_WriteCounter(0);    
        
    //Set Pin for charing operation
    _capController->Enter_Charging(_capController);    
    
    //Charging was enabled
    LED_1_Write(1);
    _led1->SetHigh(_led1);
    
    //Request Next State
    _stateQueue->Enqueue(_stateQueue, &next);
}

void Do_Charging()
{
    unsigned char next = State_Charging;
    int i = 0, readyCount = 0, enableCount = 0;       
    unsigned int tmpValue = 0;     
    
    for(i = 0; i < System_Amount_Cap; i++)
    {
        //Not enabled => skip
        if(__capContainer[i].IsEnabled == 0)
        {
            continue;
        }
        
        //Check Cap Level
        if(__capContainer[i].HasValue == 0)
        {
            tmpValue = _adc->GetValue(_adc, i);
            if(tmpValue >= System_Cap_Charged)
            {
                __capContainer[i].HasValue = 1;    
                *(__capContainer[i].Value) = System_Duration_Factor * Duration_Counter_ReadCounter();                     
            }
        }
        
        //update Amount
        enableCount++;
        if(__capContainer[i].HasValue)
        {
            readyCount++;
        }   
    }
    
    //Charging is done ?
    if(enableCount == readyCount)
    {
        //request EnableDischarge
        next = State_EnableDischarge;
    }     
    _stateQueue->Enqueue(_stateQueue, &next);    
}

void Do_EnableDischarge()
{
    unsigned char nextState = State_Discharging;
        
    //Set Pin
    _capController->Enter_Discharging(_capController);
    
    //Discharging is enabled
    _led1->SetLow(_led1);
    
    //Request Next State
    _stateQueue->Enqueue(_stateQueue, &nextState);
}

void Do_Discharging()
{
    unsigned char next = State_Discharging;
    int i = 0, readyCount = 0, enableCount = 0;    
    unsigned int tmpValue = 0;
    
    for(i = 0; i < System_Amount_Cap; i++)
    {
        //Not enabled => skip
        if(__capContainer[i].IsEnabled == 0)
        {
            continue;
        }  
        
        //Check Cap Level, update Amount
        enableCount++;
        tmpValue = _adc->GetValue(_adc, i);
        if(tmpValue < System_Cap_Discharged)
        {
            readyCount++;
        }
    }
    
    //Discharge is done?
    if(enableCount == readyCount)
    {
        //Request Generate_Output
        next = State_Generate_Output; 
    } 
    _stateQueue->Enqueue(_stateQueue, &next);
}

void Do_Wait_Input()
{
    _system->DelayUs(50);
}