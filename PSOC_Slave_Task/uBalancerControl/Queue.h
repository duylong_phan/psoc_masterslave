#include <stdio.h>
#include <string.h>

#ifndef Queue_h
#define Queue_h

    #define Queue_MaxAmount             64    
        
    struct Queue
    {
        //Raw array of address
        void *Array[Queue_MaxAmount];
        //Max Amount of Item
        int MaxAmount;
        //Size of given Item in Bytes
        int TypeSize;
        //available Item in Queue
        int Count;
        //Important Index
        int StartIndex, EndIndex;
    };

    void Queue_Initialize(struct Queue *queue, unsigned char* container, int maxAmount, int typeSize);    
    void Queue_Enqueue(struct Queue *queue, void *item);
    void* Queue_Dequeue(struct Queue *queue);
    void* Queue_Peek(struct Queue *queue);

#endif
