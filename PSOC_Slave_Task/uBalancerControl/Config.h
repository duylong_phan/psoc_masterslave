#ifndef Config_h
#define Config_h    
    //--------------- microcontroller--------------------
    //#define PSOC4_Mode                      1
    //#define PSOC5_Mode                      1
    #define Arduino_Mode                    1
    
	//------------------Components-----------------------
	//#define Com_NRF24						1
	#define Com_RFM73						1
	
    //--------------- IO Interface --------------------    
    //#define PSOC4_Serial                    1
    //#define PSOC5_Serial                    1
        
    //#define PSOC5_USB                       1
    #define PSOC5_USB_In_EP                 1
    #define PSOC5_USB_Out_EP                2
    #define PSOC5_USB_Size_Package          64
    #define PSOC5_USB_Size_PayLoad          63
    #define PSOC5_USB_Index_Length          63

	#define Arduino_SPI_CLK					12
	#define Arduino_SPI_MISO				11
	#define Arduino_SPI_MOSI				10

    //--------------- IO Setting --------------------    
    #define MasterSPI_Native                1
    //#define MasterSPI_Pin                   1
    //#define MasterSPI_MultiSlave            1 
    
    

    //----------------Operation State----------------------
    #define State_Sleep                     0
    #define State_Wait_Input                1
    #define State_RF_Interrupt              2
    #define State_RF_Receive                3
    #define State_RF_Transmit               4
    #define State_Task                      5
    #define State_Process_Input             6
    #define State_Generate_Output           7
       
        
    //-------------------Common-----------------------------
    //Buffer size
    #define Size_RF_Package                 32
    #define Size_RF_PayLoad                 30
    #define Size_StateContainer             20  

    //Operation mode
    #define Oper_Normal                     0x00
    #define Oper_Debug                      0x80

    //Task status
    #define Task_Disable                    0
    #define Task_Enable                     1
    
    //Timer
    #define Max_TickCount                   10000
    
    //RF operation
    #define RF_Off                          0
    #define RF_Receiver                     1
    #define RF_Transmitter                  2
    #define Max_LostCount                   2
    
    
    //PC Command Type
    #define PC_Error                        0
    #define PC_Start                        1
    #define PC_Stop                         2
    #define PC_RF_Transmit                  3
	#define PC_Press_Pressed                50
	#define PC_Press_Sample                 51
	#define PC_Cap_Done                     60
	#define PC_uBalance_Status				61

    //-----------------------Header Transceiver Package------------------    
    //PC Package
    #define Index_PC_Command                0
    #define Index_PC_Size                   1
    
    //RF Package
    #define Index_TaskID                    0
    #define Index_Type                      1
    #define Index_Payload                   2    

    
    //Task ID
    #define TaskID_Dummy                    0
    #define TaskID_Press                    1
    #define TaskID_CapMeasure               2
    #define TaskID_AccMeasure               3
	#define TaskID_uBalance					4

	
	//-----------------------Task Specific-----------------------------
      
	//Press
	#define Type_Press_Non                  0
    #define Type_Press_Pressed              1    
    #define Pos_Press_ID                    2
    #define Pos_Press_Amount                3
    #define Pos_Press_Sample                4
    #define Size_Press_Sample               20
    
	//Cap Measure
	#define Type_Cap_Non                    0
    #define Type_Cap_Done                   1
    #define Pos_Cap_Sample                  2    
    #define Size_Cap_Sample                 16    
    
	//Acc Measure
	

	//uBalance
	#define Type_uBalance_Ping				0
	#define Type_uBalance_StartTrans		1
	#define Type_uBalance_Actions			2
	#define Type_uBalance_EndTrans			3
	#define Type_uBalance_TransDone			4
	#define Type_uBalance_TransError		5
	#define Type_uBalance_StartOper			6
	#define Type_uBalance_StopOper			7
	#define Type_uBalance_OperDone			8
	#define Type_uBalance_OperError			9
	#define Type_uBalance_NoAction			10
	#define Type_uBalance_ActionWorking		11

    //---------------------Initialize Method--------------------
    void Initialize_HW();
    void Initialize_SW();
    void Single_Operation();    
    
    //--------------------Interrupt Method----------------------
    void On_RF_Inter();
    void On_Timer_Inter();    
    
    //------------------Operation Methods-----------------------
    void Do_Sleep();
    void Do_Wait_Input();    
    void Do_RF_Interrupt();
    void Do_RF_Transmit();
    void Do_RF_Receive();
    void Do_Task();
    void Do_Process_Input();
    void Do_Generate_Output();
    
    //------------------others function--------------------------
    unsigned char HasInput();
    
    //------------------Notification Methods-------------------
    void Notify_Beep(int miliSecond);
    void Toggle_LED(int miliSecond);
    
#endif
