#ifndef MasterSPI_h
#define MasterSPI_h
    
#include "Config.h"
#include "SubFunctions.h"
    
//Name conversion
//PSOC4, PSOC5              => SPIM
//PSOC5 Slave control       => SS_Control
#if defined(PSOC4_Mode) || defined(PSOC5_Mode)
    #include <project.h>
#endif

#ifdef Arduino_Mode
    #include <Arduino.h>
    #include <SPI.h>
#endif

//---------------------Basic---------------------
unsigned char MasterSPI_WriteRead(unsigned char data);
void MasterSPI_Set_SS(unsigned char pin);
void MasterSPI_Reset_SS(unsigned char pin);

//---------------------Advance---------------------
void MasterSPI_Write_Reg(unsigned char reg, unsigned char data, unsigned char pin);
void MasterSPI_Write_Reg_Buffer(unsigned char reg, unsigned char *data, int length, unsigned char pin);

unsigned char MasterSPI_Read_Reg(unsigned char reg, unsigned char pin);
void MasterSPI_Read_Reg_Buffer(unsigned char reg, unsigned char* data, int length, unsigned char pin);

#endif