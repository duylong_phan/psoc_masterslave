#ifndef _RFM73_H_
#define _RFM73_H_

#include "MasterSPI.h"
	
#define RFM73_MAX_PACKET_LEN            32
#define RFM73_Max_Lost                  20
#define RFM73_Address_Length            5 
	
//-------------------Usage----------------------------
//Always transmit, receive with RFM73_MAX_PACKET_LEN => 32
//Dummy: check RFM73_HasData(), RFM73_Receive()
//Complex: Interrtupt, process, and important => RFM73_ClearInterrupt()
//----------------------------------------------------
	 
//speed
#define RFM73_250KBPS			       0x00
#define RFM73_1MBPS				       0x01
#define RFM73_2MBPS				       0x02
    
//Power
#define RFM73_Power_minus_10            0x00
#define RFM73_Power_minus_5	            0x01
#define RFM73_Power_0 	                0x02
#define RFM73_Power_Plus_5       	    0x03
    
//-------------------------Common method------------------------
unsigned char RFM73_Start_Pin(unsigned char ce, unsigned char ss);
unsigned char RFM73_Start();
unsigned char RFM73_HasDevice();
unsigned char RFM73_HasData();
void RFM73_Receive(unsigned char *data);
void RFM73_Transmit(unsigned char *data, int length);
void RFM73_Transmit_Once(unsigned char *data, int length);
    
//------------------------Operation------------------------
void RFM73_Mode_Rx();
void RFM73_Mode_Tx();
unsigned char RFM73_GetMode();
void RFM73_PowerUp();
void RFM73_PowerDown();


//-----------------------Transmition Media------------------
void RFM73_SetAddress_Tx(unsigned char *address);
void RFM73_SetAddress_Rx(unsigned char *address);
void RFM73_SetChannel(unsigned char channel);
void RFM73_SetPower(unsigned char power);
void RFM73_SetDataRate(unsigned char speed);


//----------------------Memory------------------------------
void RFM73_Flush_Rx();
void RFM73_Flush_Tx();
void RFM73_ClearInterrupt();
void RFM73_GetInterrupt(unsigned char *hasIn, unsigned char *hasOut, unsigned char *hasLost);
#endif
	
	