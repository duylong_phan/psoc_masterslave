#include "Config.h"

#if defined(PSOC4_Mode) || defined(PSOC5_Mode)
    #include <project.h>
#endif

#ifdef Arduino_Mode
    #include <Arduino.h>
#endif
    

#ifndef SubFunctions_h
#define SubFunctions_h
    //------------------------------------Abstract Method-------------------------------
    void Sub_DelayMs(int miliSecond);
    void Sub_DelayUs(int microSecond);

    //---------------------------------------Method-------------------------------------
    unsigned char Sub_GetCheckSum(unsigned char* data, int length);
    void Sub_ResetArray_Unsigned(unsigned char* data, int length);
    void Sub_ResetArray_Signed(char* data, int length);
#endif