#include "Queue.h"

void Queue_Initialize(struct Queue *queue, unsigned char* container, int maxAmount, int typeSize)
{
    int i = 0;    
    
    //Parameter
    queue->MaxAmount = maxAmount;
    queue->TypeSize = typeSize;
    queue->Count = 0;
    queue->StartIndex = 0;
    queue->EndIndex = 0;
    
    //Buffer  
    for(i = 0; i < maxAmount; i++)
    {
        queue->Array[i] = (container + i*typeSize);
    }    
}

void Queue_Enqueue(struct Queue *queue, void *item)
{
    //Overflow Array Index => reset
    if(queue->StartIndex >= queue->MaxAmount)
    {
        queue->StartIndex = 0;
    }
    if(queue->EndIndex >= queue->MaxAmount)
    {
        queue->EndIndex = 0;
    }

    //Overflow Count => increase EndIndex
    if(queue->EndIndex == queue->StartIndex && queue->Count > 0)
    {
        queue->EndIndex++;
    }

    //copy byte by byte, Update Index
    memcpy(queue->Array[queue->StartIndex], item, queue->TypeSize);
    queue->StartIndex++;

    //Update Count
    if(queue->Count < queue->MaxAmount)
    {
        queue->Count++;
    }
}

void* Queue_Dequeue(struct Queue *queue)
{
    //Overflow Max Amount
    if(queue->EndIndex >= queue->MaxAmount)
    {
        queue->EndIndex = 0;
    }

    //Update Counter
    if(queue->Count > 0)
    {
        queue->Count--;
    }

    //Get item, update EndIndex
    return queue->Array[queue->EndIndex++];
}

void* Queue_Peek(struct Queue *queue)
{
    //Overflow Max Amount
    if(queue->EndIndex > queue->MaxAmount)
    {
        queue->EndIndex = 0;
    }
    return queue->Array[queue->EndIndex];
}
