/*----------Note---------


Interval calculation
-------------------------
x = (time_s * Clock_Hz / clock_Divider) - 1
*/

#include <Arduino.h>
#include <SPI.h>
#include "Config.h"
#include "Queue.h"
#include "RFM73.h"

#pragma region Macro Define

#define Interval_MaxCounter		65535		//[0;65535]
#define Interval_50us			12
#define Interval_100us			24

#define Size_ChannelContainer	30
#define Size_Channel			3

#define Info_Channel1			1 << 0
#define Info_Channel2			1 << 1
#define Info_Channel3			1 << 2
#define Info_Trigger			1 << 3		
#define Info_Delay				1 << 4

//Only forLeonardo
//#define Arduino_Leo				1
#ifdef Arduino_Leo
#define Pin_LED					13
#endif


#define Pin_RF_CE				2
#define Pin_RF_SS				3
#define Pin_Power_12V			4
#define Pin_Power_5V			5
#define Pin_C1_5V				A4			//18
#define Pin_C1_12V				A5			//19
#define Pin_C2_5V				A1			//15
#define Pin_C2_12V				A0			//14
#define Pin_C3_5V				A3			//17
#define Pin_C3_12V				A2			//16  
#pragma endregion

#pragma region Struct, Union

	struct ChannelAction
	{
		unsigned char Info;
		unsigned char Width;
		unsigned short Index_5V;
		unsigned short Index_12V;
		unsigned short Length;
		unsigned short Amount;
	};

	struct ChannelHandler
	{
		unsigned char IsEnabled;
		unsigned char Pin_5V;
		unsigned char Pin_12V;
		unsigned short Count;
		unsigned short Amount;
		unsigned short Width;
		struct ChannelAction *Action;
		struct Queue Queue;
	};

#pragma endregion

#pragma region Function Prototype
	//ChannelHandler
	void Initialize_Handler(int index, struct ChannelAction* container, unsigned char index_5V, unsigned char index_12V);
	void Do_Operation(struct ChannelHandler *handler);
	void Notify_Stop();

	//Package Type
	void On_Ping();
	void On_StartTrans();
	void On_Actions();
	void On_EndTrans();
	void On_StartOper();
	void On_StopOper();

#pragma endregion

#pragma region Buffer
	unsigned char __stateContainer[Size_StateContainer];
	struct ChannelAction __channelContainer1[Size_ChannelContainer], __channelContainer2[Size_ChannelContainer], __channelContainer3[Size_ChannelContainer];
#pragma endregion

#pragma region Global Variable

	struct ChannelHandler handler[3];
	struct Queue stateQueue;
	unsigned char isWorking, hasActions, rfMode;
	unsigned char transmitPackage[Size_RF_Package], receivePackage[Size_RF_Package];
	int lostCount;

	unsigned char RF_Channel = 20;
	unsigned char RF_Rx_Address[5] = { 0x34, 0x43, 0x10, 0x10, 0x01 };
	unsigned char RF_Tx_Address[5] = { 0x34, 0x43, 0x10, 0x10, 0x01 };

#pragma endregion

#pragma region Arduino Function

	ISR(TIMER1_OVF_vect)
	{
		//-------Important:set next Timer Ticked------------------
		TCNT1 = Interval_MaxCounter - Interval_100us;

		//-----------------------Action---------------------------
		On_Timer_Inter();
	}

	void Initialize_OutPin(unsigned char pin, unsigned char value)
	{
		pinMode(pin, OUTPUT);
		digitalWrite(pin, value);
	}

	void setup()
	{
		Initialize_HW();
		Initialize_SW();
	}

	void loop()
	{
		Single_Operation();		
	}
#pragma endregion

#pragma region Initialize function

	void Initialize_HW()
	{
		//------------------Pin------------------
		#ifdef Arduino_Leo
		Initialize_OutPin(Pin_LED, LOW);
		#endif 
		//RF
		Initialize_OutPin(Pin_RF_SS, HIGH);
		Initialize_OutPin(Pin_RF_CE, LOW);
		//Power
		Initialize_OutPin(Pin_Power_5V, HIGH);
		Initialize_OutPin(Pin_Power_12V, HIGH);
		//Trigger
		Initialize_OutPin(Pin_C1_5V, LOW);
		Initialize_OutPin(Pin_C1_12V, LOW);
		Initialize_OutPin(Pin_C2_5V, LOW);
		Initialize_OutPin(Pin_C2_12V, LOW);
		Initialize_OutPin(Pin_C3_5V, LOW);
		Initialize_OutPin(Pin_C3_12V, LOW);

		//------------------SPI------------------
		SPI.begin();
		//RF
		RFM73_Start_Pin(Pin_RF_CE, Pin_RF_SS);
		RFM73_SetChannel(RF_Channel);
		RFM73_SetAddress_Tx(RF_Tx_Address);
		RFM73_SetAddress_Rx(RF_Rx_Address);
		RFM73_Mode_Rx();

		//Timer
		noInterrupts();
		TCCR1A = 0;
		TCCR1B = 0;
		TIMSK1 |= (1 << TOIE1);
		TCCR1B |= (1 << CS11) | (1 << CS10);			//Clock=16Mhz / Divider=64
		TCNT1 = Interval_MaxCounter - Interval_100us;
		interrupts();
	}

	void Initialize_SW()
	{
		//Flag
		isWorking = 0;
		hasActions = 0;
		rfMode = RF_Receiver;

		//Count
		lostCount = 0;

		//Struct, Union
		Queue_Initialize(&stateQueue, __stateContainer, Size_StateContainer, sizeof(unsigned char));
		Initialize_Handler(0, __channelContainer1, Pin_C1_5V, Pin_C1_12V);
		Initialize_Handler(1, __channelContainer2, Pin_C2_5V, Pin_C2_12V);
		Initialize_Handler(2, __channelContainer3, Pin_C3_5V, Pin_C3_12V);

		/*Test
		unsigned char nextState = State_Task;
		struct ChannelAction action;

		Queue_Enqueue(&stateQueue, &nextState);
		action.Info = (1 << Info_Channel1) | (1 << Info_Trigger);
		action.Index_12V = 0;
		action.Index_5V = 0xFFFF;
		action.Width = 10;
		action.Length = 30;
		action.Repeat = 0x7D0;
		Queue_Enqueue(&handler[0].Queue, &action);

		action.Info = (1 << Info_Channel1) | (1 << Info_Trigger);
		action.Index_12V = 0;
		action.Index_5V = 4;
		action.Width = 20;
		action.Length = 40;
		action.Repeat = 0x7D0;
		Queue_Enqueue(&handler[0].Queue, &action);*/
	}

	void Single_Operation()
	{
		unsigned char state = (stateQueue.Count > 0) ? *(unsigned char*)Queue_Dequeue(&stateQueue) : State_Wait_Input;

		switch (state)
		{
		case State_RF_Receive:
			Do_RF_Receive();
			break;

		case State_RF_Transmit:
			Do_RF_Transmit();
			break;

		case State_Process_Input:
			Do_Process_Input();
			break;

		case State_Task:
			Do_Task();
			break;

		case State_Wait_Input:
		default:
			Do_Wait_Input();
			break;
		}
	}

#pragma endregion

#pragma region Interrupt Method

	void On_Timer_Inter()
	{
		int i;
		unsigned char hasJob = 0;
		if (isWorking == 0)
		{
			return;
		}

		for (i = 0; i < Size_Channel; i++)
		{
			if (handler[i].IsEnabled)
			{
				Do_Operation(&handler[i]);
				hasJob = 1;
			}
		}

		if (hasJob == 0)
		{
			isWorking = 0;
			hasActions = 0;
			Notify_Stop();
		}
	}

#pragma endregion

#pragma region Operation function	

	void Do_RF_Transmit()
	{
		if (rfMode != RF_Transmitter)
		{
			rfMode = RF_Transmitter;
			RFM73_Mode_Tx();
		}

		RFM73_Transmit(transmitPackage, Size_RF_Package);
	}

	void Do_RF_Receive()
	{
		unsigned char nextState = State_Process_Input;

		RFM73_Receive(receivePackage);
		Queue_Enqueue(&stateQueue, &nextState);		
	}

	void Do_Process_Input()
	{
		switch (receivePackage[Index_Type])
		{
		case Type_uBalance_Ping:
			On_Ping();
			break;

		case Type_uBalance_StartTrans:
			On_StartTrans();
			break;

		case Type_uBalance_Actions:
			On_Actions();
			break;

		case Type_uBalance_EndTrans:
			On_EndTrans();
			break;

		case Type_uBalance_StartOper:
			On_StartOper();
			break;

		case Type_uBalance_StopOper:
			On_StopOper();
			break;

		default:
			//ignore
			break;
		}
	}
	
	void Do_Task()
	{
		int i;	
		//Get Action, and enable Handler
		for (i = 0; i < Size_Channel; i++)
		{
			if (handler[i].Queue.Count > 0)
			{
				handler[i].IsEnabled = 1;
				handler[i].Action = (struct ChannelAction*)Queue_Dequeue(&handler[i].Queue);				
			}
		}

		isWorking = 1;
		#ifdef Arduino_Leo
		digitalWrite(Pin_LED, HIGH);
		#endif 
	}

	void Do_Wait_Input()
	{
		unsigned char nextState = State_Wait_Input;
		unsigned char hasIn, hasOut, hasLost;

		//Delay due to Power Supply for RF Module
		delay(25);

		//Check RF Interrupt
		RFM73_GetInterrupt(&hasIn, &hasOut, &hasLost);
		if (hasIn || hasOut || hasLost)
		{
			RFM73_ClearInterrupt();
		}

		//-------------------Handle Interrupt status-------------------
		//new package
		if (hasIn)
		{
			nextState = State_RF_Receive;
		}
		//sent done
		else if (hasOut)
		{
			lostCount = 0;
			//Notify Ok
		}
		//sent error
		else if(hasLost)
		{
			lostCount++;
			if (lostCount > Max_LostCount)
			{
				lostCount = 0;
				//Notify error
			}
			else
			{
				//try to send again
				nextState = State_RF_Transmit;
			}
		}

		//Check RF status
		if (rfMode == RF_Transmitter && nextState != State_RF_Transmit)
		{
			rfMode = RF_Receiver;
			RFM73_Mode_Rx();
		}

		//Request nextState
		Queue_Enqueue(&stateQueue, &nextState);
	}
#pragma endregion

#pragma region Channel Function
	void Initialize_Handler(int index, struct ChannelAction* container, unsigned char index_5V, unsigned char index_12V)
	{
		handler[index].IsEnabled = 0;
		handler[index].Pin_5V = index_5V;
		handler[index].Pin_12V = index_12V;
		handler[index].Count = 0;
		handler[index].Width = 0;
		handler[index].Amount = 0;
		handler[index].Action = NULL;

		Queue_Initialize(&handler[index].Queue, (unsigned char*)container, Size_ChannelContainer, sizeof(struct ChannelAction));
	}

	void Do_Operation(struct ChannelHandler *handler)
	{
		//For trigger Action
		if (handler->Action->Info & Info_Trigger)
		{
			if (handler->Count == handler->Action->Index_5V)
			{
				digitalWrite(handler->Pin_5V, HIGH);
				digitalWrite(handler->Pin_12V, LOW);
				handler->Width = 0;
			}
			if (handler->Count == handler->Action->Index_12V)
			{
				digitalWrite(handler->Pin_5V, LOW);
				digitalWrite(handler->Pin_12V, HIGH);
				handler->Width = 0;
			}

			if (handler->Width == handler->Action->Width)
			{
				digitalWrite(handler->Pin_5V, LOW);
				digitalWrite(handler->Pin_12V, LOW);
			}
			else if (handler->Width < handler->Action->Width)
			{
				handler->Width++;
			}
		}

		//-----------------------Check Count-------------------------------
		handler->Count++;
		//Count reached => Repeat++
		if (handler->Count >= handler->Action->Length)
		{
			handler->Count = 0;
			handler->Amount++;
		}
		//Repeat reached => next Action
		if (handler->Amount >= handler->Action->Amount)
		{
			handler->Amount = 0;
			//waiting Action => load action
			if (handler->Queue.Count > 0)
			{
				handler->Action = (struct ChannelAction*)Queue_Dequeue(&handler->Queue);
			}
			//No Action => disable
			else
			{
				handler->IsEnabled = 0;
			}
		}
	}

	void Notify_Stop()
	{
		unsigned char nextState = State_RF_Transmit;

		transmitPackage[Index_TaskID] = TaskID_uBalance;
		transmitPackage[Index_Type] = Type_uBalance_OperDone;
		Queue_Enqueue(&stateQueue, &nextState);
		#ifdef Arduino_Leo
		digitalWrite(Pin_LED, LOW);
		#endif 
	}
	
#pragma endregion

	//Check Slave is online
	void On_Ping()
	{
		unsigned char nextState = State_RF_Transmit;
		//response Master
		transmitPackage[Index_TaskID] = TaskID_uBalance;		
		if (isWorking)
		{
			transmitPackage[Index_Type] = Type_uBalance_ActionWorking;
		}
		else if (hasActions)
		{
			transmitPackage[Index_Type] = Type_uBalance_TransDone;
		}
		else
		{
			transmitPackage[Index_Type] = Type_uBalance_Ping;
		}
		
		Queue_Enqueue(&stateQueue, &nextState);
	}

	//reset parameter of handler, prepare for new action
	void On_StartTrans()
	{
		int i;		
		hasActions = 0;			
		for (i = 0; i < Size_Channel; i++)
		{			
			handler[i].Count = 0;
			handler[i].Width = 0;
			handler[i].Amount = 0;
			handler[i].IsEnabled = 0;
			handler[i].Action = NULL;
			while (handler[i].Queue.Count > 0)
			{
				Queue_Dequeue(&handler[i].Queue);
			}
		}
	}

	void On_Actions()
	{
		int i;
		struct ChannelAction *tmpValue = NULL;
		struct ChannelAction *rootAction = (struct ChannelAction*)&receivePackage[Index_Payload];

		for (i = 0; i < 3; i++)
		{
			//Take action at Index i
			tmpValue = rootAction + i;
			if (tmpValue->Info & Info_Channel1)
			{
				Queue_Enqueue(&handler[0].Queue, tmpValue);
			}
			else if(tmpValue->Info & Info_Channel2)
			{
				Queue_Enqueue(&handler[1].Queue, tmpValue);
			}
			else if(tmpValue->Info & Info_Channel3)
			{
				Queue_Enqueue(&handler[2].Queue, tmpValue);
			}
			else
			{
				//Ignore
			}	
		}
	}

	void On_EndTrans()
	{
		unsigned char nextState = State_RF_Transmit;
		int i, tmpValue;
		int amount = receivePackage[Index_Payload] + (receivePackage[Index_Payload + 1] << 8);

		//Get Action Amount
		tmpValue = 0;
		for (i = 0; i < Size_Channel; i++)
		{
			tmpValue += handler[i].Queue.Count;
		}

		//Check Amount
		if (tmpValue == amount)
		{
			hasActions = 1;
		}

		//response Master
		transmitPackage[Index_TaskID] = TaskID_uBalance;
		transmitPackage[Index_Type] = (hasActions) ? Type_uBalance_TransDone : Type_uBalance_TransError;
		Queue_Enqueue(&stateQueue, &nextState);
	}

	//Check action, enable Task if everything is ok
	void On_StartOper()
	{
		unsigned char nextState = State_Task;
		if (hasActions == 0)
		{
			nextState = State_RF_Transmit;
			transmitPackage[Index_TaskID] = TaskID_uBalance;
			transmitPackage[Index_Type] = Type_uBalance_NoAction;
		}
		else if (isWorking)
		{
			nextState = State_RF_Transmit;
			transmitPackage[Index_TaskID] = TaskID_uBalance;
			transmitPackage[Index_Type] = Type_uBalance_ActionWorking;
		}
		Queue_Enqueue(&stateQueue, &nextState);
	}

	//disable isWorking Flag
	void On_StopOper()
	{
		int i;
		for (i = 0; i < Size_Channel; i++)
		{
			//disable
			handler[i].IsEnabled = 0;
			//output low
			digitalWrite(handler[i].Pin_5V, LOW);
			digitalWrite(handler[i].Pin_12V, LOW);
		}
	}