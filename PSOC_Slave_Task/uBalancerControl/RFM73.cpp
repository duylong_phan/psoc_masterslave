#include "rfm73.h"

/*Last Edit
RFM73_Flush_Rx();
RFM73_ClearInterrupt();

RFM73_Flush_Tx();
RFM73_ClearInterrupt();

to bottom of function
*/


//----------------------SPI Command----------------------
#define RFM73_CMD_R_RX_PAYLOAD         		0x61         //receive payload
#define RFM73_CMD_W_TX_PAYLOAD         		0xA0         //transmit payload
#define RFM73_CMD_FLUSH_TX             		0xE1         //Flush Tx buffer
#define RFM73_CMD_FLUSH_RX             		0xE2         //Flush RX buffer
#define RFM73_CMD_REUSE_TX_PL          		0xE3         //start continuous retransmission
#define RFM73_CMD_W_TX_PAYLOAD_NOACK   		0xB0         //send without ack
#define RFM73_CMD_W_ACK_PAYLOAD        		0xA8         //send with ack
#define RFM73_CMD_ACTIVATE             		0x50         //toggle feature
#define RFM73_CMD_R_RX_PL_WID          		0x60         //read received Length
#define RFM73_CMD_NOP                  		0xFF         //read status
#define RFM73_CMD_READ_REG             		0x00         //read flag for register
#define RFM73_CMD_WRITE_REG            		0x20         //write flag for register
    
//-------------------Operation Register-----------------
#define RFM73_REG_CONFIG               		0x00
#define RFM73_REG_EN_AA                		0x01
#define RFM73_REG_EN_RXADDR            		0x02
#define RFM73_REG_SETUP_AW             		0x03
#define RFM73_REG_SETUP_RETR           		0x04
#define RFM73_REG_RF_CH                		0x05
#define RFM73_REG_RF_SETUP             		0x06
#define RFM73_REG_STATUS               		0x07
#define RFM73_REG_OBSERVE_TX           		0x08
#define RFM73_REG_CD                   		0x09
#define RFM73_REG_RX_ADDR_P0           		0x0A
#define RFM73_REG_RX_ADDR_P1           		0x0B
#define RFM73_REG_RX_ADDR_P2           		0x0C
#define RFM73_REG_RX_ADDR_P3           		0x0D
#define RFM73_REG_RX_ADDR_P4           		0x0E
#define RFM73_REG_RX_ADDR_P5           		0x0F
#define RFM73_REG_TX_ADDR              		0x10
#define RFM73_REG_RX_PW_P0             		0x11
#define RFM73_REG_RX_PW_P1             		0x12
#define RFM73_REG_RX_PW_P2             		0x13
#define RFM73_REG_RX_PW_P3             		0x14
#define RFM73_REG_RX_PW_P4             		0x15
#define RFM73_REG_RX_PW_P5             		0x16
#define RFM73_REG_FIFO_STATUS          		0x17
#define RFM73_REG_DYNPD                		0x1C
#define RFM73_REG_FEATURE              		0x1D

//Interrupt status
#define RFM73_Int_RX_DR                     0x40
#define RFM73_Int_TX_DS                     0x20
#define RFM73_Int_MAX_RT                    0x10

#define STATUS_TX_FULL                      0x01

//FIFO_STATUS
#define RFM73_FIFO_TX_REUSE                 0x40
#define RFM73_FIFO_TX_FULL                  0x20
#define RFM73_FIFO_TX_EMPTY                 0x10

#define RFM73_FIFO_RX_FULL                  0x02
#define RFM73_FIFO_RX_EMPTY                 0x01


#define RFM73_Default_Channel               20
#define RFM73_Default_Retr                  0xF1

// Bank0 register initialization values
#define RFM73_BANK0_ENTRIES 10
//[reg][init_Value]
const unsigned char Bank0_Reg[RFM73_BANK0_ENTRIES][2]={
   {  0, 0x0F }, // receive, enabled, CRC 2, enable interupts
   {  1, 0x3F }, // auto-ack on all pipes enabled
   {  2, 0x03 }, // Enable pipes 0 and 1
   {  3, 0x03 }, // 5 bytes addresses
   {  4, RFM73_Default_Retr }, // auto retransmission delay 500 us, 15 times
   {  5, RFM73_Default_Channel }, // channel 10
   {  6 ,0x07 }, // data rate 1Mbit, power 5dbm, LNA gain high
   {  7, 0x07 }, // why write this at all?? but seems required to work...
   {  8, 0x00 }, // clear Tx packet counters
   { 23, 0x00 }, // fifo status
};

//magic bank1 register initialization values 
//Chip set up, and manager
const unsigned long Bank1_Reg[] = {
   0xE2014B40,
   0x00004BC0,
   0x028CFCD0,
   0x41390099,
   0x1B8296D9, 
   0xA67F0624,
   0x00000000,
   0x00000000,
   0x00000000,
   0x00000000,
   0x00000000,
   0x00000000,
   0x00127300,
   0x36B48000 };

// more magic bank1 register initialization values
//Chip set up, and manager
unsigned char Bank1_Reg14[] = {
   0x41, 0x20, 0x08, 0x04, 0x81, 0x20, 0xCF, 0xF7, 0xFE, 0xFF, 0xFF }; 

// default receive address data pipe 0:
// just a bunch of bytes, nothing magical
unsigned char RFM73_Address_Rx[RFM73_Address_Length]={ 0x34, 0x43, 0x10, 0x10, 0x01 };
unsigned char RFM73_Address_Tx[RFM73_Address_Length]={ 0x34, 0x43, 0x10, 0x10, 0x01 };
unsigned char RFM73_CE = 0;
unsigned char RFM73_SS = 0;


//--------------------------Basic Method------------------------
void RFM73_Write_CE(unsigned char value)
{
    #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
        RF_CE_Write(value);
    #endif
    
    #ifdef Arduino_Mode
        digitalWrite(RFM73_CE, value);
    #endif  
}

void RFM73_Write_SS(unsigned char value)
{
    #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
        //HW automatic
    #endif
    
    #ifdef Arduino_Mode
        digitalWrite(RFM73_SS, value);
    #endif 
}

void RFM73_Write_Reg(unsigned char reg, unsigned char data)
{
	//Only for R/W register
    if(reg < RFM73_CMD_WRITE_REG)
    {
        reg |= RFM73_CMD_WRITE_REG;
    }
	
    MasterSPI_Write_Reg(reg, data, RFM73_SS);   
}

void RFM73_Write_Reg_Buffer(unsigned char reg, unsigned char *data, int length)
{
	 //Only for R/W register
    if(reg < RFM73_CMD_WRITE_REG)
    {
        reg |= RFM73_CMD_WRITE_REG;
    }
    MasterSPI_Write_Reg_Buffer(reg, data, length, RFM73_SS);      
}

unsigned char RFM73_Read_Reg(unsigned char reg)
{
	unsigned char data;
    //Only for R/W register
    if(reg < RFM73_CMD_READ_REG)
    {
        reg |= RFM73_CMD_READ_REG;
    } 
    data = MasterSPI_Read_Reg(reg, RFM73_SS);  	
   	return data;     
}

void RFM73_Read_Reg_Buffer(unsigned char reg, unsigned char *data, int length)
{
	 //Only for R/W register
    if(reg < RFM73_CMD_READ_REG)
    {
        reg |= RFM73_CMD_READ_REG;
    } 	
    MasterSPI_Read_Reg_Buffer(reg, data, length, RFM73_SS);   
}

void RFM73_CheckBank(unsigned char value)
{
    unsigned char status = 0x80 & RFM73_Read_Reg(RFM73_REG_STATUS);
    if(((status && (value == 0)) || ((status == 0) && value)))
    {
        RFM73_Write_Reg(RFM73_CMD_ACTIVATE, 0x53);
    }
}

void RFM73_Init_Bank()
{
    int i,k;
    unsigned char buffer[12];
    //start
    RFM73_CheckBank(1);
    
    for(i = 0; i < 9;i++)
    {
        for(k = 0; k < 4; k++)
        {
            buffer[k] = (Bank1_Reg[i] >> (8*k)) & 0xFF;
        }
        RFM73_Write_Reg_Buffer(i, buffer, 4);
    }
    
    for(i = 9; i < 14; i++)
    {
        for(k = 0; k < 4; k++)
        {
            buffer[k] = (Bank1_Reg[i] >> (8*(3 - k))) & 0xFF;
        }
        RFM73_Write_Reg_Buffer(i, buffer, 4);        
    }
    
    RFM73_Write_Reg_Buffer(14, Bank1_Reg14, 11);   
    
    for(i = 0; i < 4; i++)
    {
        buffer[i] = (Bank1_Reg[4] >> (8*i)) & 0xFF;
    }
    
    buffer[0] |= 0x06;
    RFM73_Write_Reg_Buffer(4,buffer,4);
    
    buffer[0] &= 0xF9;
    RFM73_Write_Reg_Buffer(4,buffer,4);
    
    //stop
    RFM73_CheckBank(0);
}

void RFM73_Transmit_Data(unsigned char reg, unsigned char *data, int length)
{
    unsigned char buffer[RFM73_MAX_PACKET_LEN];
    int i;
    
    for(i = 0; i < RFM73_MAX_PACKET_LEN; i++)
    {
        if(i < length)
        {
            buffer[i] = data[i];
        }
        else
        {
            buffer[i] = 0;
        }
    }
    
    RFM73_Write_Reg_Buffer(reg, buffer, RFM73_MAX_PACKET_LEN);
}

void RFM73_Set_Payload_Size(unsigned char pipe, unsigned int length)
{
    unsigned char tmpValue;
    
    if(length > RFM73_MAX_PACKET_LEN)
    {
        length = RFM73_MAX_PACKET_LEN;
    }
        
    tmpValue = RFM73_Read_Reg(RFM73_REG_DYNPD);
    
    //use default length
    if(length == 0)
    {
        //set bit of pipe #
        tmpValue |= 1 << pipe;
    }
    //use customed length
    else
    {
        //clear bit of pipe #
        tmpValue &= ~(1 << pipe);
    }
        
    RFM73_Write_Reg(RFM73_REG_DYNPD, tmpValue);
    RFM73_Write_Reg(RFM73_REG_RX_PW_P0 + pipe, length);
}

//-------------------------Common method------------------------

unsigned char RFM73_Start_Pin(unsigned char ce, unsigned char ss)
{
    RFM73_CE = ce;
    RFM73_SS = ss;
    return RFM73_Start();
}

unsigned char RFM73_Start()
{
    int i;
    
    RFM73_Write_CE(0);
    
    //start up delay
    Sub_DelayMs(200);    
    RFM73_CheckBank(0);
    
    for(i = 0; i < RFM73_BANK0_ENTRIES; i++)
    {
        RFM73_Write_Reg(Bank0_Reg[i][0], Bank0_Reg[i][1]);
    }
    RFM73_Init_Bank();
    
    RFM73_SetChannel(RFM73_Default_Channel);
    RFM73_SetAddress_Rx(RFM73_Address_Rx);
    RFM73_SetAddress_Tx(RFM73_Address_Tx);
    
    //enable extra Feature
    RFM73_Write_Reg(RFM73_CMD_ACTIVATE, 0x73);
    RFM73_Write_Reg(RFM73_REG_DYNPD, 0x3F);
    RFM73_Write_Reg(RFM73_REG_FEATURE, 0x07);
    RFM73_Set_Payload_Size(0,0);
    RFM73_Set_Payload_Size(1,0);
    
    RFM73_Mode_Rx();
    
    return RFM73_HasDevice();
}

unsigned char RFM73_HasDevice()
{
    unsigned char hasDevice = 0;;
    unsigned char tmpValue = RFM73_Read_Reg(RFM73_REG_RF_CH);
    //double check if the channel is connect
    if(tmpValue == RFM73_Default_Channel)
    {
        tmpValue = RFM73_Read_Reg(RFM73_REG_SETUP_RETR);
        if(tmpValue == RFM73_Default_Retr)
        {
            hasDevice = 1;
        }
    } 
    return hasDevice;    
}

unsigned char RFM73_HasData()
{
    unsigned char length = RFM73_Read_Reg(RFM73_CMD_R_RX_PL_WID);
    return length;
}
void RFM73_Receive(unsigned char *data)
{
    RFM73_Read_Reg_Buffer(RFM73_CMD_R_RX_PAYLOAD, data, RFM73_MAX_PACKET_LEN);
}

void RFM73_Transmit(unsigned char *data, int length)
{
    RFM73_Transmit_Data(RFM73_CMD_W_TX_PAYLOAD, data, length);
}
void RFM73_Transmit_Once(unsigned char *data, int length)
{
    RFM73_Transmit_Data(RFM73_CMD_W_TX_PAYLOAD_NOACK, data, length);
}
    
//------------------------Operation------------------------
void RFM73_Mode_Rx()
{
    unsigned char tmpValue;
    
    RFM73_Write_CE(0);
    tmpValue = RFM73_Read_Reg(RFM73_REG_CONFIG);
    tmpValue |= 0x01;   //set RX bit
    tmpValue |= 0x02;   //PWR bit up
    RFM73_Write_Reg(RFM73_REG_CONFIG, tmpValue);
    RFM73_Write_CE(1);

	RFM73_Flush_Rx();
	RFM73_ClearInterrupt();
}

void RFM73_Mode_Tx()
{
    unsigned char tmpValue; 

    RFM73_Write_CE(0);
    tmpValue = RFM73_Read_Reg(RFM73_REG_CONFIG);
    tmpValue &= 0xFE;   // clear RX bit
    tmpValue |= 0x02;   //set PWR up bit
    RFM73_Write_Reg(RFM73_REG_CONFIG, tmpValue);
    RFM73_Write_CE(1);

	RFM73_Flush_Tx();
	RFM73_ClearInterrupt();
}

unsigned char RFM73_GetMode()
{
    unsigned char isRx = RFM73_Read_Reg(RFM73_REG_CONFIG);
    isRx &= 0x01;
    return isRx;    
}

void RFM73_PowerUp()
{
    unsigned char tmpValue;
    RFM73_Write_CE(0);
    tmpValue = RFM73_Read_Reg(RFM73_REG_CONFIG);;
    tmpValue |= 0x02;   //set PWR up bit
    RFM73_Write_Reg(RFM73_REG_CONFIG, tmpValue);
    RFM73_Write_CE(1);
}

void RFM73_PowerDown()
{
    unsigned char tmpValue;
    RFM73_Write_CE(0);
    tmpValue = RFM73_Read_Reg(RFM73_REG_CONFIG);;
    tmpValue &= 0xFD;   //clear PWR up bit
    RFM73_Write_Reg(RFM73_REG_CONFIG, tmpValue);
}


//-----------------------Transmition Media------------------
//no switch mode
void RFM73_SetAddress_Tx(unsigned char *address)
{
    RFM73_Write_Reg_Buffer(RFM73_REG_TX_ADDR, address, RFM73_Address_Length);
}

//no switch mode
void RFM73_SetAddress_Rx(unsigned char *address)
{    
    RFM73_Write_Reg_Buffer( RFM73_REG_RX_ADDR_P0, address, RFM73_Address_Length);  
    RFM73_Write_Reg_Buffer( RFM73_REG_RX_ADDR_P1, address, RFM73_Address_Length);  
}

void RFM73_SetChannel(unsigned char channel)
{
    channel &= 0x7E;
    RFM73_Write_Reg(RFM73_REG_RF_CH, channel);
}
void RFM73_SetPower(unsigned char power)
{
    unsigned char tmpValue = RFM73_Read_Reg(RFM73_REG_RF_SETUP);
    
    if(power > RFM73_Power_Plus_5)
    {
        power = RFM73_Power_Plus_5;
    }
    
    RFM73_Write_CE(0);
    
    //reset default setting, 2MBPS, LNA low
    tmpValue &= 0x09;
    tmpValue |= 0x30;
    //set bit 1, 2
    tmpValue = power << 1;    
    RFM73_Write_Reg(RFM73_REG_RF_SETUP, tmpValue);
    
    RFM73_Write_CE(1);
}

void RFM73_SetDataRate(unsigned char speed)
{
    unsigned char tmpValue = RFM73_Read_Reg(RFM73_REG_RF_SETUP);
    
    RFM73_Write_CE(0);
    //keep power, filter setting
    tmpValue &= 0x07;
    
    switch(speed)
    {
        case RFM73_250KBPS:
        tmpValue |= 0x20;
        break;
        
        case RFM73_2MBPS:
        tmpValue |= 0x08;
        break;
        
        case RFM73_1MBPS:
        default:
        //do nothing
        break;
    } 
    RFM73_Write_Reg(RFM73_REG_RF_SETUP, tmpValue);
    RFM73_Write_CE(1);
}


//----------------------Memory------------------------------
void RFM73_Flush_Rx()
{
    RFM73_Write_Reg(RFM73_CMD_FLUSH_RX, 0);
}

void RFM73_Flush_Tx()
{
    RFM73_Write_Reg(RFM73_CMD_FLUSH_TX, 0);
}

void RFM73_ClearInterrupt()
{    
   	RFM73_Write_Reg( RFM73_REG_STATUS , RFM73_Read_Reg(RFM73_REG_STATUS));
}

void RFM73_GetInterrupt(unsigned char *hasIn, unsigned char *hasOut, unsigned char *hasLost)
{
    unsigned char status;
    
    //wait chip to update status
    Sub_DelayUs(50);
    status = RFM73_Read_Reg( RFM73_REG_STATUS );
    *hasIn = status & RFM73_Int_RX_DR;
    *hasOut = status & RFM73_Int_TX_DS;
    *hasLost = status & RFM73_Int_MAX_RT;
}