//#include "Config.h"
//#include "../../External References/Definitions.h"
//#include "../../External References/Queue.h"
//#include "../../External References/SubFunctions.h"
//#include "../../External References/RFM7X.h"
//
///*
//Controllable RfTrans Firmware: Support Command from MicroControllerApp
//1. Copy all
//2. Replace code in main.c
//3. Select all, remove Comment
//4. Build, and program into device
//*/
//
//
////--------------------------Struct, Union----------------------
//struct RfTransHandler
//{
//    unsigned char IsEnabled;
//    unsigned char Type;
//    unsigned char Status;    
//    unsigned char Interval;
//    unsigned char CurrentPackageID;
//    unsigned char IsStarted;
//    int Duration;
//};
//
////--------------------------Buffer--------------------------
//unsigned char __stateContainer[Size_StateContainer];
//
////--------------------------Setting----------------------------
//static const unsigned char SettingBuffer[30] = 
//{
//    0x00, 0x00, 0x00, 0x00, 0x00,
//    0x00, 0x00, 0x00, 0x00, 0x00,
//    0x00, 0x00, 0x00, 0x00, 0x00,
//    0x00, 0x00, 0x00, 0x00, 0x00,
//    0x00, 0x00, 0x00, 0x00, 0x00,
//    0x00, 0x00, 0x00, 0x00, 0x00
//};
//
//unsigned char RF_Channel = 20;
//unsigned char RF_Rx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};
//unsigned char RF_Tx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};
//
////---------------------------Global Variable--------------------
//struct Queue stateQueue;
//struct RfHandler rfHandler;
//struct RfTransHandler rfTransHandler;
//int tickCount;
//
////----------------------------Main Method-----------------------
//int main()
//{  
//    Initialize_HW();
//    Initialize_SW();
//    for(;;)
//    {
//        Single_Operation();    
//    }
//}
//
//CY_ISR(Triggered_RF_Inter)
//{
//    On_RF_Inter();
//}
//
//CY_ISR(Triggered_Timer_Inter)
//{
//    On_Timer_Inter();
//}
//
////---------------------------Initialize HW, SW-------------------
////Initialize Hardware Components and APIs
//void Initialize_HW()
//{
//    //Important => always call this
//    CyGlobalIntEnable; 
//           
//    //Enable SPI
//    SPIM_Start();
//    RFM7X_Start();   
//    RFM7X_SetChannel(RF_Channel);
//    RFM7X_SetAddress_Tx(RF_Tx_Address);
//    RFM7X_SetAddress_Rx(RF_Rx_Address);             
//    RFM7X_Mode_Rx();
//    
//    //Enable RF Interrrupt
//    RF_Inter_StartEx(Triggered_RF_Inter);
//    
//    //Enable Timer
//    Timer_Inter_StartEx(Triggered_Timer_Inter);
//    Timer_PWM_Start();
//}
//
////Initialize Software components
//void Initialize_SW()
//{    
//    //----Counter----
//    tickCount = 0;    
//    
//    //-----handler----
//    rfHandler.Pipe = 0;
//    rfHandler.Length = 0;
//    rfHandler.LostCount = 0;
//    rfHandler.Mode = RF_Receiver;
//    Sub_ResetArray_Unsigned(rfHandler.TransmitPackage, Size_RF_Package);
//    Sub_ResetArray_Unsigned(rfHandler.ReceivePackage, Size_RF_Package);
//    rfHandler.TransmitPackage[Index_TaskID] = TaskID_RfTrans;
//        
//    rfTransHandler.Type = 0;
//    rfTransHandler.Status = 0;
//    rfTransHandler.CurrentPackageID = 0;   
//    rfTransHandler.IsStarted = 0;
//    
//    //----Queue----
//    Queue_Initialize(&stateQueue, __stateContainer, Size_StateContainer, sizeof(unsigned char));    
//}
//
////Life circle of the Microcontroller
//void Single_Operation()
//{
//    unsigned char state = (stateQueue.Count > 0) ? *(unsigned char*)Queue_Dequeue(&stateQueue) : State_Wait_Input;
//    switch(state)
//    {
//        case State_RF_Interrupt:        Do_RF_Interrupt();      break;
//        case State_RF_Receive:          Do_RF_Receive();        break;
//        case State_RF_Transmit:         Do_RF_Transmit();       break;
//        case State_Process_Input:       Do_Process_Input();     break;        
//        case State_Task:                Do_Task();              break;
//        case State_Wait_Input:
//        default:                        Do_Wait_Input();             break;
//    }
//}
//
//
////--------------------Interrupt Method------------------
//void On_RF_Inter()
//{
//    unsigned char nextState = State_RF_Interrupt;
//    Queue_Enqueue(&stateQueue, &nextState);
//}
//
//void On_Timer_Inter()
//{
//    unsigned char nextState = State_Task;
//    
//    //Operation is started
//    if(rfTransHandler.IsEnabled)
//    {
//        //Specific interval, and interval is matched
//        if(rfTransHandler.Interval != 0 && (tickCount % rfTransHandler.Interval) == 0)
//        {
//            Queue_Enqueue(&stateQueue, &nextState);
//        }
//    }   
//    
//    tickCount++;
//    //only when Operation is not enable
//    if(tickCount >= TickCount_Minute && rfTransHandler.IsEnabled == 0)
//    {
//        tickCount = 0;
//    }
//}
//
////---------------------------Operation Method--------------------
////On RF Interrupt, or requested
//void Do_RF_Interrupt()
//{
//    unsigned char hasIn, hasOut, hasLost;
//    unsigned char nextState = State_Sleep;
//    
//    //Get Interrupt
//    RFM7X_GetInterrupt(&hasIn, &hasOut, &hasLost); 
//    
//    //When transmision completed
//    if(hasOut)
//    {
//        rfHandler.LostCount = 0;
//        LED_2_Write(0);
//    }
//    //When package lost
//    else if(hasLost)
//    {
//        rfHandler.LostCount++; 
//        if(rfHandler.LostCount >= Max_LostCount)
//        {          
//            //Discard and reset
//            RFM7X_Flush_Tx();
//            rfHandler.LostCount = 0;
//        }
//        else
//        {
//            //Request retransmision
//            nextState = State_RF_Transmit;
//        }
//    }  
//    
//    //To Rx, if no Transmit is requested, and no Operation
//    if(rfTransHandler.IsEnabled == 0 && rfHandler.Mode == RF_Transmitter && nextState != State_RF_Transmit)
//    {        
//        rfHandler.Mode = RF_Receiver;
//        RFM7X_Mode_Rx();
//    }
//    
//    //As soon as posible option, when Operation started, do nothing
//    if(rfTransHandler.IsEnabled && rfTransHandler.Interval == 0 && nextState == State_Sleep)
//    {
//        nextState = State_Task;  
//    }
//    
//    //Request next operation state
//    Queue_Enqueue(&stateQueue, &nextState);
//}
//
////Read Package from Master
//void Do_RF_Receive()
//{
//    unsigned char nextState = State_Process_Input; 
//    unsigned char hasData = 0;
//    
//    //alway check if there is an available Package
//    hasData = RFM7X_Receive(rfHandler.ReceivePackage, &rfHandler.Length, &rfHandler.Pipe);
//    if(hasData)
//    {
//        Queue_Enqueue(&stateQueue, &nextState);    
//        LED_2_Write(0);
//    }  
//}
//
////Transmit package to Master
//void Do_RF_Transmit()
//{
//    //To Tx, if current mode is Rx
//    if(rfHandler.Mode != RF_Transmitter)
//    {
//        rfHandler.Mode = RF_Transmitter;        
//        RFM7X_Mode_Tx();
//    }
//    else
//    {
//        RFM7X_ClearInterrupt();
//    }   
//    //Before transmitting package, always clear Interrupt, explicit or indirect in RFM7X_Mode_Tx
//    //to allow transmision, ignore current State of RF Module
//    RFM7X_Transmit(rfHandler.TransmitPackage, Size_RF_Package);
//    LED_2_Write(1);
//}
//
////Process Input Package from Master
//void Do_Process_Input()
//{
//    unsigned char nextState = State_Sleep;
//    if(rfHandler.ReceivePackage[Index_TaskID] != TaskID_RfTrans)
//    {
//        return;
//    }
//    
//    switch(rfHandler.ReceivePackage[Pos_RfTrans_Status])
//    {
//        case Status_RfTrans_Ping:
//        //response
//        rfHandler.TransmitPackage[Index_Type] = Type_RfTrans_Status;
//        rfHandler.TransmitPackage[Pos_RfTrans_Status] = (rfTransHandler.IsEnabled) ? Status_RfTrans_OperWorking : Status_RfTrans_Ping;        
//        nextState = State_RF_Transmit;        
//        break;
//        
//        case Status_RfTrans_WriteSetting:
//        //update setting
//        rfTransHandler.Interval = rfHandler.ReceivePackage[Pos_RfTrans_TransInterval];
//        rfTransHandler.Duration = 1000*(rfHandler.ReceivePackage[Pos_RfTrans_Duration] + rfHandler.ReceivePackage[Pos_RfTrans_Duration + 1] * 256);        
//        //response
//        rfHandler.TransmitPackage[Index_Type] = Type_RfTrans_Status;
//        rfHandler.TransmitPackage[Pos_RfTrans_Status] = Status_RfTrans_WriteSettingDone;
//        nextState = State_RF_Transmit;        
//        break;  
//        
//        case Status_RfTrans_StartOper:
//        //set, reset
//        rfTransHandler.IsEnabled = 1;
//        rfTransHandler.CurrentPackageID = 1;
//        rfTransHandler.IsStarted = 1;
//        tickCount = 0;
//        //Notify        
//        LED_1_Write(1);
//        nextState = State_Task;
//        break;  
//        
//        case Status_RfTrans_StopOper:
//        //Implement!!!    
//        break;  
//        
//        default:
//        //ignore
//        break;
//    }
//    
//    //request next State
//    Queue_Enqueue(&stateQueue, &nextState);
//}
//
////Do Task
//void Do_Task()
//{
//    unsigned char nextState = State_RF_Transmit;
//    //set
//    rfHandler.TransmitPackage[Index_Type] = Type_RfTrans_Package;
//    rfHandler.TransmitPackage[Pos_RfTrans_PackageID] = rfTransHandler.CurrentPackageID;
//    rfTransHandler.CurrentPackageID++;
//    
//    //Aknowledge on started
//    if(rfTransHandler.IsStarted)
//    {
//        //reset
//        rfTransHandler.IsStarted = 0;
//         //Notify Operation Status
//        rfHandler.TransmitPackage[Index_Type] |= Type_RfTrans_Status;
//        rfHandler.TransmitPackage[Pos_RfTrans_Status] = Status_RfTrans_OperWorking;
//    }
//    
//    //Check if Operation is done
//    if(tickCount > rfTransHandler.Duration)
//    {
//        //Notify Operation Status
//        rfHandler.TransmitPackage[Index_Type] |= Type_RfTrans_Status;
//        rfHandler.TransmitPackage[Pos_RfTrans_Status] = Status_RfTrans_OperDone;
//        //reset
//        tickCount = 0;
//        rfTransHandler.IsEnabled = 0;
//        LED_1_Write(0);        
//    }
//    
//    //request next State
//    Queue_Enqueue(&stateQueue, &nextState);
//}
//
////Do nothing
//void Do_Wait_Input()
//{    
//    unsigned char nextState = State_Sleep;
//    //When Input from Master
//    //return when available
//    if(rfHandler.Mode == RF_Receiver && RFM7X_HasData())
//    {
//        nextState = State_RF_Receive;
//        Queue_Enqueue(&stateQueue, &nextState);
//        return;
//    }
//    
//    //nothing to do
//    //Important => CPU will process data in Background
//    Sub_DelayUs(100);
//}