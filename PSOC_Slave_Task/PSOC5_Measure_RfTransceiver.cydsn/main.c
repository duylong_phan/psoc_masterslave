/*For Test RF: Command from MicroControllerApp not supported
1. Copy all
2. Replace code in main.x
3. Select all and remove Comment
4. Build, and program into device
*/


#include "Config.h"
#include "../../External References/Definitions.h"
#include "../../External References/Queue.h"
#include "../../External References/SubFunctions.h"
#include "../../External References/RFM7X.h"

//--------------------------Struct, Union----------------------
struct RfTransHandler
{
    unsigned char IsEnabled;
    unsigned char Type;
    unsigned char Status;    
    unsigned char Interval;
    unsigned char CurrentPackageID;
    unsigned char IsStarted;
    int Duration;
};

//--------------------------Buffer--------------------------
unsigned char __stateContainer[Size_StateContainer];

//--------------------------Setting----------------------------
const unsigned char SettingBuffer[30] = 
{
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00
};

unsigned char RF_Channel = 20;
unsigned char RF_Rx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};
unsigned char RF_Tx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};

//---------------------------Global Variable--------------------
struct Queue stateQueue;
struct RfHandler rfHandler;
struct RfTransHandler rfTransHandler;
int tickCount;

//----------------------------Main Method-----------------------
int main()
{  
    Initialize_HW();
    Initialize_SW();
    for(;;)
    {
        Single_Operation();    
    }
}

CY_ISR(Triggered_RF_Inter)
{
    On_RF_Inter();
}

CY_ISR(Triggered_Timer_Inter)
{
    On_Timer_Inter();
}

//---------------------------Initialize HW, SW-------------------
//Initialize Hardware Components and APIs
void Initialize_HW()
{
    //Important => always call this
    CyGlobalIntEnable; 
           
    //Enable SPI
    SPIM_Start();
    RFM7X_Start();   
    RFM7X_SetChannel(RF_Channel);
    RFM7X_SetAddress_Tx(RF_Tx_Address);
    RFM7X_SetAddress_Rx(RF_Rx_Address);             
    RFM7X_Mode_Tx();
    
    //Enable RF Interrrupt
    RF_Inter_StartEx(Triggered_RF_Inter);
    
    //Enable Timer
    Timer_Inter_StartEx(Triggered_Timer_Inter);
    Timer_PWM_Start();
}

//Initialize Software components
void Initialize_SW()
{
    //begin with RF_Transmit, and continue to transmit with Do_RF_Interrupt
    unsigned char nextState = State_RF_Transmit;
    //----Counter----
    tickCount = 0;    
    
    //-----handler----
    rfHandler.Pipe = 0;
    rfHandler.Length = 0;
    rfHandler.LostCount = 0;
    rfHandler.Mode = RF_Transmitter;
    Sub_ResetArray_Unsigned(rfHandler.TransmitPackage, Size_RF_Package);
    Sub_ResetArray_Unsigned(rfHandler.ReceivePackage, Size_RF_Package);
    rfHandler.TransmitPackage[Index_TaskID] = TaskID_RfTrans;
        
    rfTransHandler.Type = 0;
    rfTransHandler.Status = 0;
    rfTransHandler.CurrentPackageID = 1;   
    rfTransHandler.IsStarted = 0;
    
    //----Queue----
    Queue_Initialize(&stateQueue, __stateContainer, Size_StateContainer, sizeof(unsigned char));
    Queue_Enqueue(&stateQueue, &nextState);
}

//Life circle of the Microcontroller
void Single_Operation()
{
    unsigned char state = (stateQueue.Count > 0) ? *(unsigned char*)Queue_Dequeue(&stateQueue) : State_Wait_Input;
    switch(state)
    {
        case State_RF_Interrupt:        Do_RF_Interrupt();      break;        
        case State_RF_Transmit:         Do_RF_Transmit();       break;        
        case State_Wait_Input:
        default:                        Do_Wait_Input();             break;
    }
}

//--------------------Interrupt Method------------------
void On_RF_Inter()
{
    unsigned char nextState = State_RF_Interrupt;
    Queue_Enqueue(&stateQueue, &nextState);
    LED_2_Write(0);
}

void On_Timer_Inter()
{
    tickCount++;   
    
    //Notify system is running
    if((tickCount % 100) == 0)
    {
        LED_1_Write(!LED_1_Read());
    }
    
    if(tickCount >= TickCount_Minute)
    {
        tickCount = 0;
    }
}

//---------------------------Operation Method--------------------
//On RF Interrupt, or requested
void Do_RF_Interrupt()
{    
    unsigned char nextState = State_RF_Transmit;
    //Request next operation state
    Queue_Enqueue(&stateQueue, &nextState);
}

//Transmit package to Master
void Do_RF_Transmit()
{   
    //set
    rfHandler.TransmitPackage[Index_Type] = Type_RfTrans_Package;
    rfHandler.TransmitPackage[Pos_RfTrans_PackageID] = rfTransHandler.CurrentPackageID;
    rfTransHandler.CurrentPackageID++;
    
    //Enable Transmittion
    RFM7X_ClearInterrupt();
    RFM7X_Transmit(rfHandler.TransmitPackage, Size_RF_Package);
    LED_2_Write(1);
}

//Do nothing
void Do_Wait_Input()
{    
    CyDelayUs(10);
}