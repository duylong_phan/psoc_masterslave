#include "Config.h"

#ifndef CYAPICALLBACKS_H
#define CYAPICALLBACKS_H
    
    #define CY_BOOT_INT_DEFAULT_HANDLER_EXCEPTION_ENTRY_CALLBACK        1
    
    void CyBoot_IntDefaultHandler_Exception_EntryCallback();
    
#endif /* CYAPICALLBACKS_H */   
/* [] */
