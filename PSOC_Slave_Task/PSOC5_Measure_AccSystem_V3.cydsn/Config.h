#ifndef Config_h
#define Config_h  
    //--------------- microcontroller--------------------
    //#define PSOC4_Mode                      1
    #define PSOC5_Mode                      1
    //#define Arduino_Mode                    1
    
    
    #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
        #include <project.h>
    #endif

    #ifdef Arduino_Mode
        #include <Arduino.h>
    #endif
    
    //------------------Components-----------------------
	//#define Com_NRF24						1	
    #define Com_RFM75                       1
    #define Com_ADXL343                     1
    
    //--------------- SPI Setting --------------------    
    #define SPIM_Native                 1
    //#define SPIM_Pin                   1    
    #define SPIM_Mode_0                 1
//    #define SPIM_Mode_1                 1
//    #define SPIM_Mode_2                 1
    #define SPIM_Mode_3                 1
    #define SPIM_MultiSlave             1 
    #define SPIM_Mode_Multi             1
#endif