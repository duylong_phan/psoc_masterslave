#include "Config.h"
#include "../../External_References_V2/IDefinitions.h"
#include "../../External_References_V2/IQueue.h"
#include "../../External_References_V2/ISystem.h"
#include "../../External_References_V2/IRF.h"
#include "../../External_References_V2/ITimer.h"
#include "../../External_References_V2/IAccelerometer.h"
#include "../../External_References_V2/ILed.h"

/*Program MicroController
via Debug Interface: 
+ plug in Cable 
+ remove BootLoadable from TopDesign.cysch (Important)
+ Click Program Button or Ctrl+F5

via UART Interface: 
+ (BootLoadable muss be available in TopDesign.cysch)
+ Open Menu/Tool/BootLoader Host
+ Select COMX, Baudrate 115200
+ Select *_1.cydsn from CortexM3\ARM_GCC_493\Debug
+ Command1 => off && Command2 => off
+ Power up the System
+ Click Program Button on BootLoader Host
+ Power down, Command1 => on, Power on => ready to use
*/

//--------------------------Local Define--------------------
#define System_Amount_Acc               4
#define System_Amount_Led               2
#define System_Amount_Output            25

//--------------------------Source--------------------------
struct IAccelerometer __accContainer[System_Amount_Acc];
struct IAccelerometerHandler __accHandlerContainer[System_Amount_Acc];
struct ILed __ledContainer[System_Amount_Led];
unsigned char __stateContainer[Size_StateContainer];
struct ISystem __system;
struct ISPIM __spim, __spim3;
struct IRF __rf;
struct IQueue __stateQueue;
struct RfHandler __rfHandler;
struct ITimer __timer;

//---------------------------Global Variable--------------------
struct ISystem *_system;
struct ISPIM *_spim, *_spim3;
struct IRF *_rf;
struct ILed *_led1, *_led2;
struct RfHandler *_rfHandler;
struct IQueue *_stateQueue;
struct ITimer *_timer;

unsigned char RF_Channel = 30;
unsigned char RF_Rx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};
unsigned char RF_Tx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};

unsigned char _collectedValues;

//----------------------------Prototype Method--------------------
void OnTimer_MiliSecondTick(struct ITimer *me);
void OnRf_Interrupted(struct IRF *me);

//----------------------------Main Method--------------------
int main(void)
{
    Initialize_SW();
    Initialize_HW();    
    for(;;)
    {
        Single_Operation();
    }
}

//----------------------------Interrupt Method--------------------
CY_ISR(Triggered_RF_Inter)
{
    _rf->OnInterrupted(_rf);
}

CY_ISR(Triggered_Timer_Inter)
{
    _timer->TickCount++;
    _timer->OnMiliSecondTick(_timer);
}

//----------------------------Initialization Method--------------------
void Initialize_SW()
{
    int i = 0;
    
    //System
    _system = &__system;
    Initialize_ISystem(_system);    
    SetService_ISystem(_system);
    
    //SPI Master
    _spim = &__spim;
    _spim3 = &__spim3;
    Initialize_ISPIM(_spim, 0, _system);
    Initialize_ISPIM(_spim3, 3, _system);    
            
    //Timer
    _timer = &__timer;
    Initialize_ITimer(_timer);   
    _timer->OnMiliSecondTick = &OnTimer_MiliSecondTick;

        
    //RF Module
    _rf = &__rf;
    Initialize_IRF(_rf);
    _rf->OnInterrupted = &OnRf_Interrupted;
    
    //Accelerometer
    for(i = 0; i < System_Amount_Acc; i++)
    {
        Initialize_IAccelerometer(&__accContainer[i], _spim3, i, 0.48828); 
        Initialize_IAccelerometerHandler(&__accHandlerContainer[i]);
    }  
    
    //Led
    for(i = 0; i < System_Amount_Led; i++)
    {
       Initialize_ILed(&__ledContainer[i], i, 0);  
    }  
    _led1 = &__ledContainer[0];
    _led2 = &__ledContainer[1];
    
    //Queue
    _stateQueue = &__stateQueue;    
    Initialize_IQueue(_stateQueue, __stateContainer, Size_StateContainer, sizeof(unsigned char)); 
    
    //RF Handler
    _rfHandler = &__rfHandler;
    _rfHandler->Pipe = 0;
    _rfHandler->Length = 0;
    _rfHandler->LostCount = 0;
    _rfHandler->Mode = RF_Transmitter;
    _system->Reset_UnsignedArray(_rfHandler->TransmitPackage, Size_RF_Package);
    _system->Reset_UnsignedArray(_rfHandler->ReceivePackage, Size_RF_Package); 
    
    _collectedValues = 0;
}

void Initialize_HW()
{
    int i = 0;
    
    CyGlobalIntEnable;   
        
    //SPI
    _spim->Start(_spim);
    _spim3->Start(_spim3);

    
    //RF    
    _rf->StartPin(_rf, _spim, 0, 4); 
    _rf->Enter_Tx(_rf);
    _rf->SetChannel(_rf, RF_Channel);
    _rf->SetAddress_Tx(_rf, RF_Tx_Address);
    _rf->SetAddress_Rx(_rf, RF_Rx_Address);  
    
    //Accelerometer  
    for(i = 0; i < System_Amount_Acc; i++)
    {
        __accContainer[i].Start(&__accContainer[i]);
    }
    
    //Enable RF Interrrupt Pin tracking
    Inter_RF_StartEx(Triggered_RF_Inter);
    
    //Enable Timer tracking
    Inter_Timer_StartEx(Triggered_Timer_Inter);
    Timer_PWM_Start();
}

void Single_Operation()
{   
    unsigned char state = (_stateQueue->Count > 0) ? *(unsigned char*)_stateQueue->Dequeue(_stateQueue) :
                                                     State_Wait_Input;
    switch(state)
    {
        case State_RF_Interrupt:        Do_RF_Interrupt();      break;
        case State_RF_Receive:          Do_RF_Receive();        break;
        case State_RF_Transmit:         Do_RF_Transmit();       break;
        case State_Process_Input:       Do_Process_Input();     break;
        case State_Generate_Output:     Do_Generate_Output();   break;
        case State_Task:                Do_Task();              break;
        case State_Wait_Input:
        default:                        Do_Wait_Input();        break;
    }
}

//---------------------------Operation Method--------------------
void OnTimer_MiliSecondTick(struct ITimer *me)
{
    unsigned char next = State_Task;
    if((me->TickCount % 2) == 0)
    {
        _stateQueue->Enqueue(_stateQueue, &next);
    }    
}

void OnRf_Interrupted(struct IRF *me)
{
    unsigned char next = State_RF_Interrupt;
    _stateQueue->Enqueue(_stateQueue, &next);
}

void Do_RF_Interrupt()
{
    //Due to Multiple SPI Mode 
    //=> Interrupt Value may invalid if the Package is retransmitted
    //=> more time is required
//    unsigned char hasIn, hasOut, hasLost;    
//    
//    _system->DelayUs(50);
//    _rf->GetInterrupt(_rf, &hasIn, &hasOut, &hasLost);
//    if(hasOut)
//    {
//        _led2->SetLow(_led2);
//    }
//    else if(hasLost)
//    {
//        _rf->Flush_Tx(_rf);
//    }
//    if(_rfHandler->Mode == RF_Transmitter)
//    {
//        _rfHandler->Mode = RF_Receiver;
//        _rf->Enter_Rx(_rf);
//    }
}

void Do_RF_Receive()
{
    //do nothing
}

void Do_RF_Transmit()
{
    //Check if Package is sent successfully
    unsigned char hasIn, hasOut, hasLost;  
    _rf->GetInterrupt(_rf, &hasIn, &hasOut, &hasLost);
    //Transmition is ok
    if(hasOut)
    {
        _led2->Toggle(_led2);
    }
    //Transmition failed
    else
    {
        _led2->SetHigh(_led2);
    }
    //Transmition failed, clear buffer
    if(hasLost)
    {
        _rf->Flush_Tx(_rf);
    }
    
    //Clear interrupt and Transmit Package
    _rf->ClearInterrupt(_rf);    
    _rf->Transmit(_rf, _rfHandler->TransmitPackage, Size_RF_Package);   
}

void Do_Process_Input()
{
    //do nothing
}

void Do_Generate_Output()
{
    int i;
    unsigned char offset, sampleSize;
    unsigned char next = State_RF_Transmit;  
    
    _rfHandler->TransmitPackage[Index_TaskID] = TaskID_AccMeasure;
    _rfHandler->TransmitPackage[Index_Type] = 0;
    
    sampleSize = IAccelerometer_Amount_Axis * sizeof(short);    
    for(i = 0; i < System_Amount_Acc; i++)
    {        
        offset = Index_Payload + (i * sampleSize);
        memcpy(&_rfHandler->TransmitPackage[offset], __accHandlerContainer[i].Storage, sampleSize);
    }
    _stateQueue->Enqueue(_stateQueue, &next);    
}

void Do_Task()
{
    int i, k;
    short delta;
    unsigned char next = State_Generate_Output;  
    
    //read measured Value from each Sensor
    for(i = 0; i < System_Amount_Acc; i++)
    {
        __accContainer[i].GetXYZ(&__accContainer[i], __accHandlerContainer[i].Values);
        
        //first Sample
        if(__accHandlerContainer[i].HasStorage == 0)
        {
            __accHandlerContainer[i].HasStorage = 1;
            for(k = 0; k < IAccelerometer_Amount_Axis; k++)
            {
                __accHandlerContainer[i].Storage[k] = __accHandlerContainer[i].Values[k];
            }
        }
        //other sample
        else
        {            
            for(k = 0; k < IAccelerometer_Amount_Axis; k++)
            {                
                //x' = x2 - x1
                delta = __accHandlerContainer[i].Values[k] - __accHandlerContainer[i].Storage[k];
                //x = x + 0.01*x'
                __accHandlerContainer[i].Storage[k] += (short)(0.01 * delta);
            }
        }   
    }
        
    //Check Amount
    _collectedValues++;   
    if(_collectedValues >= System_Amount_Output)
    {
        _collectedValues = 0;
        _stateQueue->Enqueue(_stateQueue, &next);
    } 
}

void Do_Wait_Input()
{
    _system->DelayUs(50);
}