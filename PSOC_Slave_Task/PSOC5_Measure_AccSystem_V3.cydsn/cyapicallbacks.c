#include "cyapicallbacks.h"

void CyBoot_IntDefaultHandler_Exception_EntryCallback()
{
    LED_1_Write(0);
    LED_2_Write(0);
    for(;;)
    {
        LED_1_Write(!LED_1_Read());
        LED_2_Write(!LED_2_Read());
        CyDelay(100);
    }
}