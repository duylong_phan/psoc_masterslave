#include "Config.h"
#include "../../External References/Definitions.h"
#include "../../External References/Queue.h"
#include "../../External References/SubFunctions.h"
#include "../../External References/RFM7X.h"

/* Condition Value, were determined by measurement    
    
    3.3V----R_c------|---|
                     C   R_d
                     |   |
                      GND
    this configuration leads to voltage divider, therefore 63% of Vdd is no more value
    by measuring with Ok_Charge, the duration is smaller than it was expected,
    it equals to half of real value => simply multiple with Ok_Duration_Factor
    ----------------------------------------------------
    R_charge = 10k
    R_discharge = 4.7k
    for other value, parameter must be measured again
*/
#define Ok_Charge                       1035
#define Ok_Discharge                    2
#define Ok_Duration_Factor              2

//Extra Operation state
#define State_EnableCharge              8
#define State_Charging                  9
#define State_EnableDischarge           10
#define State_Discharging               11

/*Edit here in order to enable Capacitor
Bit                 J
0                   1
1                   2
2                   3
3                   4
*/
#define Size_Cap_Amount                 4
#define Enable_Capacitor                0x0F


//--------------------------Struct, Union----------------------
union MeasureBuffer
{
    unsigned char Array[Size_Cap_Sample];
    unsigned int Number[Size_Cap_Amount];
};

struct CapacitorHandler
{
    union MeasureBuffer Buffer;
    unsigned char HasValue[Size_Cap_Amount];
    unsigned char IsEnabled[Size_Cap_Amount];
};

struct TaskHandler
{
    unsigned char WaitRF;    
    int TickCount;
};

//-----------------------------Buffer----------------------------
unsigned char __stateContainer[Size_StateContainer];

//---------------------------Global Variable--------------------
struct Queue stateQueue;
struct RfHandler rfHandler;
struct CapacitorHandler capHandler;
struct TaskHandler taskHandler;

unsigned char RF_Channel = 20;
unsigned char RF_Rx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};
unsigned char RF_Tx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};

//--------------------------Function Prototype-------------------------
void Do_EnableCharge();
void Do_Charging();
void Do_EnableDischarge();
void Do_Discharging();
void Pin_OnSPI();
void Pin_OnCharging();
void Pin_OnOnDischarging();

//----------------------------Main Method-----------------------
int main()
{    
    Initialize_HW();
    Initialize_SW();
    Initialize_Parameters();    
    for(;;)
    {       
        Single_Operation();
    }    
}

//-----------------------------------Interrupt-----------------------------------
CY_ISR(On_TimerTicked)
{
    On_Timer_Inter();
}

//-----------------------State Operation function----------------------
void Initialize_HW()
{
    //Always enabled
    CyGlobalIntEnable; 
    
    //Enable ADC
    ADC_Start();
    ADC_StartConvert();
            
    //Enable RF module, use direct pin, no support from Hardware SPI Interface
    RFM7X_Start();  
    RFM7X_SetChannel(30);
    //default
    //RFM7X_SetChannel(RF_Channel);
    RFM7X_SetAddress_Tx(RF_Tx_Address);
    RFM7X_SetAddress_Rx(RF_Rx_Address); 
    RFM7X_Mode_Tx();
    
    //Enable Charging duration tracker  
    Duration_Counter_Start();
    Duration_PWM_Start();
        
    //Enable Interrupt    
    Timer_PWM_Start();
    Timer_Int_StartEx(On_TimerTicked);
}

void Initialize_SW()
{
    int i = 0;
        
    //Handler
    rfHandler.Mode = RF_Transmitter;        
    rfHandler.LostCount = 0;   
    rfHandler.Length = 0;
    rfHandler.Pipe = 0;
    Sub_ResetArray_Unsigned(rfHandler.ReceivePackage, Size_RF_Package);
    Sub_ResetArray_Unsigned(rfHandler.TransmitPackage, Size_RF_Package);
    
    taskHandler.WaitRF = 0;
    taskHandler.TickCount = 0;
    
    
    //Queue
    Queue_Initialize(&stateQueue, __stateContainer, Size_StateContainer, sizeof(unsigned char));
    
    //Capacitor
    for(i = 0; i < Size_Cap_Amount; i++)
    {
        capHandler.HasValue[i] = 0;
        capHandler.IsEnabled[i] = (Enable_Capacitor & (1 << i)) ? 1 : 0;
    }  
}

void Initialize_Parameters()
{
    //Request Charge as first operation after HW, SW are initialized
    unsigned char nextState = State_EnableCharge;
    Queue_Enqueue(&stateQueue, &nextState);
}

//Single Operation of the Microcontroller
void Single_Operation()
{
    /*No Interrupt, or Receive RF funtions are supported
      ---------------------------------------------------
      When these 2 functions are used, the result is unstable Operation of the system.
      Because SPI communition, and charging/discharging are done on the same pins
      This leads to uncompitable interface with SPI Communication.    
    
      Therefore MasterSPI_Pin is used, for direct pin to control
      High value of Resister is used for discharging => longer time for new measurement
    */   
    
    unsigned char state = (stateQueue.Count > 0) ? *(unsigned char*)Queue_Dequeue(&stateQueue) : State_Wait_Input;               
    switch(state)
    {        
        case State_RF_Transmit:             Do_RF_Transmit();           break;        
        case State_EnableCharge:            Do_EnableCharge();          break;            
        case State_Charging:                Do_Charging();              break;            
        case State_EnableDischarge:         Do_EnableDischarge();       break;        
        case State_Discharging:             Do_Discharging();           break;            
        case State_Generate_Output:         Do_Generate_Output();       break;        
        case State_Wait_Input:
        default:                            Do_Wait_Input();            break;
    }
}

//--------------------Interrupt Method------------------
void On_Timer_Inter()
{
    unsigned char nextState = State_Wait_Input;
    taskHandler.TickCount++;
    
    //wait for RF transmition done
    if(taskHandler.WaitRF && taskHandler.TickCount > 20)
    {
        taskHandler.WaitRF = 0;
        nextState = State_EnableCharge;
        Queue_Enqueue(&stateQueue, &nextState);
        LED_2_Write(0);
    }
    
    //Reset Counter
    if(taskHandler.TickCount >= TickCount_Minute)
    {
        taskHandler.TickCount = 0;
    }
}

//--------------------Operation Method------------------
void Do_RF_Transmit()
{
    if(rfHandler.Mode != RF_Transmitter)
    {
        rfHandler.Mode = RF_Transmitter;
        RFM7X_Mode_Tx();
    }
    else
    {
        RFM7X_ClearInterrupt();
    }
    
    RFM7X_Transmit(rfHandler.TransmitPackage, Size_RF_Package);
    LED_2_Write(1);
}

void Do_EnableCharge()
{
    unsigned char nextState = State_Charging;
    int i = 0;
    
    //Reset Cap Data
    for(i = 0; i < Size_Cap_Amount; i++)
    {
        capHandler.HasValue[i] = 0;
        capHandler.Buffer.Number[i] = 0;
    }
    
    //Reset Counter
    Duration_Counter_WriteCounter(0);    
        
    //Set Pin for charing operation
    Pin_OnCharging();
    
    //Charging was enabled
    LED_1_Write(1);
    
    //Request Next State
    Queue_Enqueue(&stateQueue, &nextState);
}

void Do_Charging()
{
    unsigned char nextState = State_Charging;
    int i = 0, readyCount = 0, enableCount = 0;       
    short tmpValue = 0;     
    
    for(i = 0; i < Size_Cap_Amount; i++)
    {
        //Not enabled => skip
        if(capHandler.IsEnabled[i] == 0)
        {
            continue;
        }
        
        //Check Cap Level
        if(capHandler.HasValue[i] == 0)
        {
            tmpValue = ADC_GetResult16(i);
            if(tmpValue >= Ok_Charge)
            {
                capHandler.HasValue[i] = 1;                
                capHandler.Buffer.Number[i] = Ok_Duration_Factor*Duration_Counter_ReadCounter();
            }
        }
        
        //update Amount
        enableCount++;
        if(capHandler.HasValue[i])
        {
            readyCount++;
        }   
    }
    
    //Charging is done ?
    if(enableCount == readyCount)
    {
        //request EnableDischarge
        nextState = State_EnableDischarge;
    } 
    Queue_Enqueue(&stateQueue, &nextState);
}

void Do_EnableDischarge()
{
    unsigned char nextState = State_Discharging;
        
    //Set Pin
    Pin_OnOnDischarging();
    
    //Discharging is enabled
    LED_1_Write(0);
    
    //Request Next State
    Queue_Enqueue(&stateQueue, &nextState);
}

void Do_Discharging()
{
    unsigned char nextState = State_Discharging;
    int i = 0, readyCount = 0, enableCount = 0;    
    short tmpValue = 0;
    
    for(i = 0; i < Size_Cap_Amount; i++)
    {
        //Not enabled => skip
        if(capHandler.IsEnabled[i] == 0)
        {
            continue;
        }  
        
        //Check Cap Level, update Amount
        enableCount++;
        tmpValue = ADC_GetResult16(i);
        if(tmpValue < Ok_Discharge)
        {
            readyCount++;
        }
    }
    
    //Discharge is done?
    if(enableCount == readyCount)
    {
        //Request Generate_Output
        nextState = State_Generate_Output; 
    } 
    Queue_Enqueue(&stateQueue, &nextState);
}

void Do_Generate_Output()
{
    unsigned char nextState = State_RF_Transmit; 
    
    //Reset
    Pin_OnSPI();
    Sub_ResetArray_Unsigned(rfHandler.TransmitPackage, Size_RF_Package);
    
    //generate package
    rfHandler.TransmitPackage[Index_TaskID] = TaskID_CapMeasure;
    rfHandler.TransmitPackage[Index_Type] = Type_Cap_Done;
    memcpy(&rfHandler.TransmitPackage[Index_Payload], capHandler.Buffer.Array, Size_Cap_Sample);
    
    //Important => Wait for RF    
    taskHandler.WaitRF = 1;
    taskHandler.TickCount = 0;
    
    //request transmit data
    Queue_Enqueue(&stateQueue, &nextState);
}

void Do_Wait_Input()
{
      //Test Connection
//    unsigned char nextState = State_Generate_Output;
//    Queue_Enqueue(&stateQueue, &nextState);
//    CyDelay(200);
    
    //Do nothing
    Sub_DelayUs(100);
}

//------------------------Sub Function---------------------
void Pin_OnSPI()
{
    //As output pin
    SPI_MOSI_SetDriveMode(SPI_MOSI_DM_STRONG);
    //As input pin
    SPI_MISO_SetDriveMode(SPI_MISO_DM_DIG_HIZ);
    //avoid charging capacitor at beginning
    SPI_MOSI_Write(0);
}

void Pin_OnCharging()
{
    //As output pin
    SPI_MOSI_SetDriveMode(SPI_MOSI_DM_STRONG);
    //As output pin
    SPI_MISO_SetDriveMode(SPI_MOSI_DM_STRONG);  
    //Enable charge on MOSI
    SPI_MOSI_Write(1); 
    //Disable charge on MISO => this leads to voltage divider circuit
    SPI_MISO_Write(0);
}

void Pin_OnOnDischarging()
{
    //As Input pin, indirect disable charge on MOSI
    SPI_MOSI_SetDriveMode(SPI_MOSI_DM_DIG_HIZ);
}