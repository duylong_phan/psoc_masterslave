#include "Config.h"
#include "../../External References/Definitions.h"
#include "../../External References/Queue.h"
#include "../../External References/StreamBuffer.h"
#include "../../External References/SubFunctions.h"
#include "../../External References/RFM7X.h"

/* -----------Pin Configuration for different version-----------
N.A     muss be unlock
O.K     let it stay the same, do not interrupt system Operation
--      matched

                    V.2                                     V.3
RF_CE               --                                      --
RF_SS               --                                      --
SPI_CLK             --                                      --
SPI_MOSI            --                                      --
SPI_MISO            --                                      --
RF_Inter            O.K                                     P15.5
A1                  P3.3                                    P0.3
A1_N                P3.2                                    P0.2
B1                  P3.4                                    P0.1
B1_N                P3.5                                    P0.0
A2                  N.A                                     P12.0 
A2_N                N.A                                     P12.1
B2                  N.A                                     P3.7
B2_N                N.A                                     P3.6
IC_Up_Enable        O.K                                     P12.3
LED_1               P3.6(Open drain, drive high)            P3.4(Strong)
LED_2               N.A                                     P3.5(Strong)
Ana_Bat             O.K                                     P1.6
Ana_100V            O.K                                     P1.7
*/

//-------------------------------Control Register----------------------------
//Set Bit
#define Motor_Ctr_Inverse                   0x01
#define Motor_Ctr_EnableSignal              0x02
#define Motor_Ctr_EnableOut                 0x04
#define Motor_Ctr_Reset                     0x08
#define Motor_Ctr_Delay                     0x10

//Clear bit
#define Motor_Ctr_Clear_Inverse             0xFE
#define Motor_Ctr_Clear_EnableSignal        0xFD
#define Motor_Ctr_Clear_EnableOut           0xFB
#define Motor_Ctr_Clear_Reset               0xF7
#define Motor_Ctr_Clear_Delay               0xEF

#define Setting_MotorCtr_PWM_Period     0
#define Setting_MotorCtr_PWM_Compare1   2
#define Setting_MotorCtr_PWM_Compare2   4
#define Setting_MotorCtr_SampleFreq     6

//-------------------------------Others Definition----------------------------
#define Size_Motor                          2
#define Size_MotorAction                    14
#define Size_ActionContainer                32   
#define Index_Battery                       0
#define Index_100V                          1

//--------------------------Struct, Union----------------------

//Size of struct should be 2*n => otherwise struct padding
struct MotorAction
{
    unsigned char Control1;
    unsigned char Control2;    
    unsigned short Amount;
    unsigned short DelayAmount;
    unsigned short TriggerAmount;
    unsigned short PwmPeriod;
    unsigned short Increment;
    unsigned short NoUse;
};

struct TaskHandler
{
    unsigned char CanAutoStart;
    unsigned char CanWaitAction;
    unsigned char CurrentAna;
    unsigned short AutoStartDelay;
    unsigned short SampleOffset;
    unsigned short Last_Battery;
    unsigned short Last_100V;
    unsigned int TickCount;
    unsigned int SampleFlag; 
    unsigned int LastSampleFlag;
    unsigned int LastPwmFreq;
};

struct MotorHandler
{
    //Store requested Action
    struct Queue ActionQueue;
    //Store Queue Information
    struct QueueInfo ActionQueueInfo;
    //Information about current Action
    struct MotorAction *CurrentAction;
    //Working Flag
    unsigned char IsWorking;
    //Done Flag
    unsigned char HasActionDone;        
    //Delay is enabled in between
    unsigned char RequestDelay;
    //Invert is enabled
    unsigned char RequestInvert;
    //Increment is enabled
    unsigned char RequestIncrement;
    //Reverse Trigger is available
    unsigned char BackwardPulse;
    //Backward after the Current Package is done
    unsigned char BackwardPackage;
    //Current Action
    unsigned short Amount;
    //Action Index
    unsigned char ActionIndex;
    //Increment offset
    short CurrentOffset;
};

//-----------------------------Buffer----------------------------
unsigned char __stateContainer[Size_StateContainer];
struct MotorAction __actionContainer[Size_Motor][Size_ActionContainer];

//--------------------------Setting----------------------------
static const unsigned char SettingBuffer[Size_SettingBuffer] = 
{
    0xCF, 0x03, 0xD6, 0x02, 0xF4,
    0x00, 0x32, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00
};

static const unsigned short *Setting_PWM_Period = (const unsigned short*)&SettingBuffer[Setting_MotorCtr_PWM_Period];
static const unsigned short *Setting_PWM_Compare1 = (const unsigned short*)&SettingBuffer[Setting_MotorCtr_PWM_Compare1];
static const unsigned short *Setting_PWM_Compare2 = (const unsigned short*)&SettingBuffer[Setting_MotorCtr_PWM_Compare2];
static const unsigned char *Setting_SampleFreq = &SettingBuffer[Setting_MotorCtr_SampleFreq];

//---------------------------Global Variable--------------------
struct Queue stateQueue;
struct RfHandler rfHandler;
struct TaskHandler taskHandler;
struct MotorHandler motorHandler[Size_Motor];

unsigned char RF_Channel = 20;
unsigned char RF_Rx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};
unsigned char RF_Tx_Address[5] = {0x34, 0x43, 0x10, 0x10, 0x01};

//---------------------------Function Prototypes---------------------------
//Interrupt Method
void On_Motor0_Done();
void On_Motor1_Done();

//Process Input Method
void On_GetDeviceStatus();
void On_StartActionTransmision();
void On_ActionTransmision();
void On_EndActionTransmision();
void On_StartOperation();
void On_StopOperation();
void On_RealActionTransmision();
void On_ToggleSample();
void On_ReadSetting();
void On_WriteSetting();
void On_ReadPwm();
void On_WritePwm();
void On_RealBothAction();

//Interrupt Jobs Method
void CheckRfHandlerStatus();
void CheckAutoStart();
void CheckDataSample();
void GetPwmFrequency();
void GetAdcValue();

//Request Action
void Request_PulseSignal(int index);
void Request_DelaySignal(int index);
void Request_InvertPulseSignal(int index);

//Sub Method
void Response_GetDeviceStatus(unsigned int pcAction, unsigned int status);
void AddAction(struct MotorAction *action);
void LoadNextAction(int index);
void SetupCounter(int index, unsigned int amount, unsigned char isDelay);
void ResetSetupPwmSignal();
void SetupPwmSignal(unsigned short period);
void StartAction(int index, unsigned char isDelay, unsigned char isInvert, unsigned char isOut);
void StopAction(int index);

//----------------------------Main Method-----------------------
int main()
{  
    Initialize_HW();
    Initialize_SW();
    Initialize_Parameters();
    for(;;)
    {        
        Single_Operation();    
    }
}

CY_ISR(Triggered_RF_Inter)
{
    On_RF_Inter();
}

CY_ISR(Triggered_Timer_Inter)
{
    On_Timer_Inter();
}

CY_ISR(Triggered_Motor0_Inter)
{
    On_Motor0_Done();
}

CY_ISR(Triggered_Motor1_Inter)
{
    On_Motor1_Done();
}

//---------------------------Initialize HW, SW-------------------
//Initialize Hardware Components and APIs
void Initialize_HW()
{
    //Always enabled
    CyGlobalIntEnable;   
    
    //Enable ADC
    ADC_Start();
    ADC_StartConvert();
    Ana_Mux_Start();
    Ana_Mux_Select(Index_Battery);
    
    //Enable SPI => RF
    SPIM_Start();
    RFM7X_Start();
    
    RFM7X_SetChannel(30);
    //default
    //RFM7X_SetChannel(RF_Channel);
    RFM7X_SetAddress_Tx(RF_Tx_Address);
    RFM7X_SetAddress_Rx(RF_Rx_Address);   
    
    //Enable Motor
    Signal_PWM_Start();
    ResetSetupPwmSignal();
    Motor_Counter_0_Start();
    Motor_Counter_0_WriteCounter(0);
    Motor_Counter_1_Start();
    Motor_Counter_1_WriteCounter(0);    
    
    //Timer
    Timer_PWM_Start();
    
    //PWM Frequency Counter
    Signal_Freq_Counter_Start();
    Signal_Freq_Counter_WriteCounter(0);
    
    //Interrupt
    RF_Inter_StartEx(Triggered_RF_Inter);
    Timer_Inter_StartEx(Triggered_Timer_Inter);
    Motor0_Inter_StartEx(Triggered_Motor0_Inter);
    Motor1_Inter_StartEx(Triggered_Motor1_Inter);
}

//Initialize Software components
void Initialize_SW()
{
    int i;
    //-------Handler--------
    //Task
    taskHandler.TickCount = 0;
    taskHandler.CanAutoStart = 0;
    taskHandler.CanWaitAction = 0;
    taskHandler.SampleFlag = 0;
    taskHandler.SampleOffset = 1000 / *Setting_SampleFreq;
    taskHandler.CurrentAna = Index_Battery;
    taskHandler.Last_Battery = 0;
    taskHandler.Last_100V = 0;
    
    //RF
    rfHandler.Pipe = 0;
    rfHandler.Length = 0;
    rfHandler.LostCount = 0;
    rfHandler.Mode = RF_Receiver;    
    rfHandler.CheckInput = 0;
    rfHandler.WaitOutput = 0;
    Sub_ResetArray_Unsigned(rfHandler.TransmitPackage, Size_RF_Package);
    Sub_ResetArray_Unsigned(rfHandler.ReceivePackage, Size_RF_Package);  
    //Motor
    for(i = 0; i < Size_Motor; i++)
    {
        motorHandler[i].IsWorking = 0;        
        Queue_Initialize(&motorHandler[i].ActionQueue, (unsigned char*)__actionContainer[i], Size_ActionContainer, sizeof(struct MotorAction));
        QueueInfo_Initialize(&motorHandler[i].ActionQueueInfo);
    }
    
    //--------Queue--------
    Queue_Initialize(&stateQueue, __stateContainer, Size_StateContainer, sizeof(unsigned char));  
}

//Intialize Parameter: set, reset HW and SW Components
void Initialize_Parameters()
{

}

//Life circle of the Microcontroller
void Single_Operation()
{
    unsigned char state = (stateQueue.Count > 0) ? *(unsigned char*)Queue_Dequeue(&stateQueue) : State_Wait_Input;
    switch(state)
    {
        case State_RF_Interrupt:        Do_RF_Interrupt();      break;
        case State_RF_Receive:          Do_RF_Receive();        break;
        case State_RF_Transmit:         Do_RF_Transmit();       break;
        case State_Process_Input:       Do_Process_Input();     break;
        case State_Generate_Output:     Do_Generate_Output();   break;
        case State_Task:                Do_Task();              break;
        case State_Wait_Input:
        default:                        Do_Wait_Input();        break;
    }
}

//---------------------------Operations Methods-------------------
//On RF Interrupt, or requested
void Do_RF_Interrupt()
{
    unsigned char nextState = State_Wait_Input;
    unsigned char hasIn, hasOut, hasLost;
    
    Sub_DelayUs(50);
    RFM7X_GetInterrupt(&hasIn, &hasOut, &hasLost);
    
    //Transmision sucessfully
    if(hasOut)
    {
        rfHandler.LostCount = 0;
        LED_2_Write(0);
    }
    //Transmision failed
    else if(hasLost)
    {
        rfHandler.LostCount++;
        //reach Lost Max ?
        if(rfHandler.LostCount >= Max_LostCount)
        {
            rfHandler.LostCount = 0;
            RFM7X_Flush_Tx();
        }
        //request retransmision
        else
        {
            nextState = State_RF_Transmit;
        }
    }    
    
    //Reset to Rx, if no more transmision request is available
    if(rfHandler.Mode == RF_Transmitter && nextState != State_RF_Transmit)
    {
        rfHandler.Mode = RF_Receiver;
        RFM7X_Mode_Rx();
    }
    Queue_Enqueue(&stateQueue, &nextState);
}
 
//Read Package from Master
void Do_RF_Receive()
{
    unsigned char nextState = State_Process_Input;
    unsigned char hasData;    
    hasData = RFM7X_Receive(rfHandler.ReceivePackage, &rfHandler.Length, &rfHandler.Pipe);
    if(hasData)
    {
        Queue_Enqueue(&stateQueue, &nextState);
        LED_2_Write(0);
    }
}

//Transmit package to Master
void Do_RF_Transmit()
{
    //WaitOutput is enabled
    if(rfHandler.WaitOutput)
    {  
        return;
    }
    
    //To Tx, if current mode is Rx
    if(rfHandler.Mode != RF_Transmitter)
    {
        rfHandler.Mode = RF_Transmitter;        
        RFM7X_Mode_Tx();
    }
    //Clear Interrupt
    else
    {
        RFM7X_ClearInterrupt();
    }   
    //Before transmitting package, always clear Interrupt, explicit or indirect in RFM7X_Mode_Tx
    //to allow transmision, ignore current State of RF Module
    RFM7X_Transmit(rfHandler.TransmitPackage, Size_RF_Package);
    LED_2_Write(1);
}

//Process Input Package from Master
void Do_Process_Input()
{
    if(rfHandler.ReceivePackage[Index_TaskID] != TaskID_MotorCtr)
    {
        return;
    }
    
    switch(rfHandler.ReceivePackage[Index_Type])
    {
        case Type_MotorCtr_DeviceStatus:        On_GetDeviceStatus();           break;
        case Type_MotorCtr_StartTrans:          On_StartActionTransmision();    break;
        case Type_MotorCtr_Action:              On_ActionTransmision();         break;
        case Type_MotorCtr_EndTrans:            On_EndActionTransmision();      break;
        case Type_MotorCtr_StartOper:           On_StartOperation();            break;
        case Type_MotorCtr_StopOper:            On_StopOperation();             break;
        case Type_MotorCtr_RealAction:          On_RealActionTransmision();     break;        
        case Type_MotorCtr_ToggleSample:        On_ToggleSample();              break;
        case Type_MotorCtr_ReadSetting:         On_ReadSetting();               break;
        case Type_MotorCtr_WriteSetting:        On_WriteSetting();              break;  
        case Type_MotorCtr_ReadPwm:             On_ReadPwm();                   break;
        case Type_MotorCtr_WritePwm:            On_WritePwm();                  break;
        case Type_MotorCtr_RealBothAction:      On_RealBothAction();            break;
        default:                                /*Do nothing*/                  break;
    }
}

//Generate Data Sample Package to transmit to Master
void Do_Generate_Output()
{
    unsigned char nextState = State_RF_Transmit;
    
    //Sample is disable
    if(taskHandler.SampleFlag == 0)
    {
        return;
    }
    
    //Reset Buffer
    Sub_ResetArray_Unsigned(rfHandler.TransmitPackage, Size_RF_Package);
    
    //Set Header
    rfHandler.TransmitPackage[Index_TaskID] = TaskID_MotorCtr;
    rfHandler.TransmitPackage[Index_Type] = Type_MotorCtr_DataSample;
    
    //SampleFlag => Always
    memcpy(&rfHandler.TransmitPackage[Pos_MotorCtr_SampleFlag], &taskHandler.SampleFlag, sizeof(unsigned int));
    
    //Battery
    if((taskHandler.SampleFlag & (1 << Sample_Bit_MotorCtr_Battery)) != 0)
    {
        memcpy(&rfHandler.TransmitPackage[Pos_MotorCtr_Sample_Battery], &taskHandler.Last_Battery, sizeof(unsigned short));
    }
    
    //Pulse
    if((taskHandler.SampleFlag & (1 << Sample_Bit_MotorCtr_Pulse)) != 0)
    {
        memcpy(&rfHandler.TransmitPackage[Pos_MotorCtr_Sample_Pulse], &taskHandler.Last_100V, sizeof(unsigned short));
    }
    
    //PWM Frequency
    if((taskHandler.SampleFlag & (1 << Sample_Bit_MotorCtr_PWM_Freq)) != 0)
    {       
        memcpy(&rfHandler.TransmitPackage[Pos_MotorCtr_Sample_PWM_Freq], &taskHandler.LastPwmFreq, sizeof(unsigned int));
    }
    
    //Request next state
    Queue_Enqueue(&stateQueue, &nextState);
}

//Do specific Task
void Do_Task()
{   
    unsigned int pcAction = 0, status = 0;
    int i = 0, stopCount = 0;
    
    for(i = 0; i < Size_Motor; i++)
    {
        //Motor is not enabled => Skip
        if(motorHandler[i].IsWorking == 0)
        {
           stopCount++;
           continue;
        }
        
        //Current Motor is still working => Skip
        if(motorHandler[i].HasActionDone == 0)
        {
            continue;
        }
        //Important => Reset Flag
        motorHandler[i].HasActionDone = 0;
        
        //Last action is done
        if(motorHandler[i].Amount == 0)
        {
            //Run forever => Max amount
            if((motorHandler[i].CurrentAction->Control2 & MotorCtr_Control2_RunForever) != 0)
            {
                motorHandler[i].Amount = 0xFFFF;
            }
            //BackwardPackage is enabled
            else if(motorHandler[i].BackwardPackage)
            {
                //Important => Reset Backward Flag
                motorHandler[i].BackwardPackage = 0;
                //Important => Invert RequestInvert
                motorHandler[i].RequestInvert = (motorHandler[i].RequestInvert == 0) ? 1 : 0;
                //Important => Set Amount of Current Action
                motorHandler[i].Amount = motorHandler[i].CurrentAction->Amount;
            }
            //remainder Action => Load next action
            else if(motorHandler[i].ActionQueue.Count != 0)
            {
                LoadNextAction(i);                 
            }            
            //No more action => Reset IsWorking Flag, increment stopCount, skip
            else
            {
                motorHandler[i].IsWorking = 0;
                stopCount++;
                continue;   
            }    
        }
        
        //Trigger Action
        if((motorHandler[i].CurrentAction->Control1 & MotorCtr_Control1_Trigger) != 0)
        { 
            //Delay + BackwardPulse
            if(motorHandler[i].RequestDelay && motorHandler[i].BackwardPulse)
            {
                //Do Request
                switch(motorHandler[i].ActionIndex)
                {
                    case 1:
                    case 3:     Request_DelaySignal(i);              break;                    
                    case 2:     Request_InvertPulseSignal(i);        break;                    
                    case 0:
                    default:    Request_PulseSignal(i);              break;
                }
                //Increment Index, overflow => reset Index, decrement Amount
                motorHandler[i].ActionIndex++;
                if(motorHandler[i].ActionIndex > 3)
                {
                    motorHandler[i].ActionIndex = 0;
                    motorHandler[i].Amount--;
                }
            }
            //Delay only
            else if(motorHandler[i].RequestDelay)
            {
                //Do Request
                switch(motorHandler[i].ActionIndex)
                {
                    case 1:     Request_DelaySignal(i);              break;
                    case 0:
                    default:    Request_PulseSignal(i);              break;
                }
                //Increment Index, overflow => reset Index, decrement Amount
                motorHandler[i].ActionIndex++;
                if(motorHandler[i].ActionIndex > 1)
                {
                    motorHandler[i].ActionIndex = 0;
                    motorHandler[i].Amount--;
                }
            }
            //BackwardPulse only
            else if(motorHandler[i].BackwardPulse)
            {
                //Do Request
                switch(motorHandler[i].ActionIndex)
                {
                    case 1:     Request_InvertPulseSignal(i);        break;
                    case 0:
                    default:    Request_PulseSignal(i);              break;
                }
                //Increment Index, overflow => reset Index, decrement Amount
                motorHandler[i].ActionIndex++;
                if(motorHandler[i].ActionIndex > 1)
                {
                    motorHandler[i].ActionIndex = 0;
                    motorHandler[i].Amount--;
                }
            }
            //only pulse
            else
            {
                Request_PulseSignal(i);
                motorHandler[i].Amount--;
            }
        }
        //Delay Action
        else if((motorHandler[i].CurrentAction->Control1 & MotorCtr_Control1_Delay) != 0)
        {
            Request_DelaySignal(i);            
            //decrease amount
            motorHandler[i].Amount--;
        }        
    }
    
    //Operation is done ?
    if(stopCount == Size_Motor)
    {
        pcAction |= 1 << PC_Action_Bit_AppStatus;
        status |= 1 << Status_Bit_MotorCtr_OperDone;
        //Notify Operation stop
        LED_1_Write(0);
        //Transmit device status
        Response_GetDeviceStatus(pcAction, status);
    }
}

//Do nothing
void Do_Wait_Input()
{
    unsigned char nextState = State_Wait_Input;
    
    if(rfHandler.CheckInput)
    {
        rfHandler.CheckInput = 0;
        //Check RF status when RF is in Rx mode
        if(rfHandler.Mode == RF_Receiver && RFM7X_HasData())
        {
            nextState = State_RF_Receive;
            Queue_Enqueue(&stateQueue, &nextState);
            LED_2_Write(1);
            return;
        }  
    }  
    
    //Do nothing
    Sub_DelayUs(10);
}

//--------------------Interrupt Method------------------
void On_RF_Inter()
{
    //Request RF Interrupt
    unsigned char nextState = State_RF_Interrupt;
    Queue_Enqueue(&stateQueue, &nextState);
}

void On_Timer_Inter()
{
    taskHandler.TickCount++;   
    
    //Check, update Flag
    CheckRfHandlerStatus();
    CheckAutoStart();
    CheckDataSample();
    
    //Get Value
    GetPwmFrequency();
    GetAdcValue();    
    
    //Reset TickCount after 1 Minute
    if(taskHandler.TickCount >= TickCount_Minute)
    {
        taskHandler.TickCount = 0;
    }
}

void On_Motor0_Done()
{
    //Request Task update
    unsigned char nextState = State_Task;
    Queue_Enqueue(&stateQueue, &nextState);
    
    //Set Flag
    motorHandler[0].HasActionDone = 1;
}

void On_Motor1_Done()
{
    //Request Task update
    unsigned char nextState = State_Task;
    Queue_Enqueue(&stateQueue, &nextState);
    
    //Set Flag
    motorHandler[1].HasActionDone = 1;
}

//---------------------------Process Input Methods-------------------

void On_GetDeviceStatus()
{
    unsigned int pcAction = 0, status = 0;    
    int i = 0;    
    
    pcAction = 1 << PC_Action_Bit_AppStatus;
    
    //Set Parameter    
    for(i = 0; i < Size_Motor; i++)
    {
        if(motorHandler[i].IsWorking)
        {
            status |= 1 <<  Status_Bit_MotorCtr_OperWorking;
        }
        if(motorHandler[i].ActionQueue.Count > 0)
        {
            status |= 1 << Status_Bit_MotorCtr_HasActions;
        }
    }    
    Response_GetDeviceStatus(pcAction, status);   
}

void On_StartActionTransmision()
{
    int i;  
    unsigned char isWorking = 0;
    
    //Disable Data Sample during Action Transmition
    taskHandler.LastSampleFlag = taskHandler.SampleFlag;
    taskHandler.SampleFlag = 0;
    
    //Important => Enable Receive Action
    taskHandler.CanWaitAction = 1;
    
    //--------Reset MotorHandler---------
    for(i = 0; i < Size_Motor; i++)
    {
        if(motorHandler[i].IsWorking)
        {
            isWorking = 1;
        }
        //Remove Actions
        while(motorHandler[i].ActionQueue.Count > 0)
        {
            Queue_Dequeue(&motorHandler[i].ActionQueue);
        } 
    }
    
    //System is working => Stop
    if(isWorking)
    {
        On_StopOperation();
    }
}

void On_ActionTransmision()
{
    struct MotorAction *action = (struct MotorAction *)&rfHandler.ReceivePackage[Index_Payload];    
    int i = 0;
   
    //Only when StartTrans was received
    if(taskHandler.CanWaitAction == 0)
    {
        return;
    }
    
    //Read each Action in received package
    for(i = 0; i < 2; i++)
    {
        AddAction(action + i);
    }
}

void On_EndActionTransmision()
{    
    unsigned int pcAction = 0, status = 0;
    unsigned char tmpValue = 0;
    int amount = 0, i = 0;
    
    //Only when StartTrans was received
    if(taskHandler.CanWaitAction == 0)
    {
        return;
    }
    
    //Get Amount
    for(i = 0; i < Size_Motor; i++)
    {
        amount += motorHandler[i].ActionQueue.Count;
    }
    
    //Restore SampleFlag, in case System Sample is enabled previously
    taskHandler.SampleFlag = taskHandler.LastSampleFlag;
    
    //Amount matched
    if(amount == *(unsigned short *)&rfHandler.ReceivePackage[Pos_MotorCtr_EndTransAmount])
    {
        pcAction |= 1 << PC_Action_Bit_AppStatus;
        status |= 1 << Status_Bit_MotorCtr_TransDone; 
                
        //Store Actions ?
        tmpValue = rfHandler.ReceivePackage[Pos_MotorCtr_EndTransCommand] & MotorCtr_EndTrans_StoreActions;
        for(i = 0; i < Size_Motor; i++)
        {
            //Save information of Queue
            if(tmpValue != 0)
            {
                QueueInfo_SetInfo(&motorHandler[i].ActionQueueInfo, &motorHandler[i].ActionQueue);
            }
            //Clear Information Flag
            else
            {
                motorHandler[i].ActionQueueInfo.IsValid = 0;
            }
        }
        
        //AutoStart with Delay ?
        if((rfHandler.ReceivePackage[Pos_MotorCtr_EndTransCommand] & MotorCtr_EndTrans_StartAfter) != 0)
        {
            taskHandler.CanAutoStart = 1;
            taskHandler.AutoStartDelay = *(unsigned short *)&rfHandler.ReceivePackage[Pos_MotorCtr_EndTransDelay];
        }
    }
    //Invalid Amount
    else
    {
        pcAction |= 1 << PC_Action_Bit_InfoMessage;
        status |= 1 << Status_Bit_MotorCtr_TransError;
    }
    //Transmit device status
    Response_GetDeviceStatus(pcAction, status);
}

void On_StartOperation()
{
    int i;
    unsigned char nextState = State_Task;
    unsigned char isWorking = 0, canStart = 0;
    unsigned int pcAction = 0, status = 0;
    
    //Load first Action from Motor Action Queue, and enable operation
    for(i = 0; i < Size_Motor; i++)
    {
        //System is working => Invalid => break
        if(motorHandler[i].IsWorking)
        {
            isWorking = 1;
            break;
        }
        
        //use last Action Queue, when it is stored
        if(motorHandler[i].ActionQueueInfo.IsValid)
        {
            QueueInfo_SetQueue(&motorHandler[i].ActionQueueInfo, &motorHandler[i].ActionQueue);
        }
        
        //Check and Load Action from Queue
        if(motorHandler[i].ActionQueue.Count > 0)
        {
            LoadNextAction(i);
            motorHandler[i].IsWorking = 1;
            //Important => Enable Counter, and Control Register update
            motorHandler[i].HasActionDone = 1;
            canStart = 1;
        }
    }
    
    //System is working => Cancel Start request
    if(isWorking)
    {
        pcAction |= 1 << PC_Action_Bit_InfoMessage;
        status |= 1 << Status_Bit_MotorCtr_OperWorking;
    }
    //System is not working
    else
    {
        //Action is loaded, ready to work
        if(canStart)
        {
            //Notify Motor Operation is started
            LED_1_Write(1);    
            //Reset next state
            Queue_Enqueue(&stateQueue, &nextState);
            pcAction |= 1 << PC_Action_Bit_AppStatus;
            status |= 1 << Status_Bit_MotorCtr_OperWorking;
        }
        //No action is available
        else
        {
            pcAction |= 1 << PC_Action_Bit_InfoMessage;
            status |= 1 << Status_Bit_MotorCtr_NoActions;
        }
    }
    
    //Notify User
    Response_GetDeviceStatus(pcAction, status);
}

void On_StopOperation()
{
    unsigned char nextState = State_Task, canStop = 0;
    unsigned int pcAction = 0, status = 0;
    int i;   
    
    for(i = 0; i < Size_Motor; i++)
    {
        if(motorHandler[i].IsWorking)
        {
            motorHandler[i].IsWorking = 0;
            StopAction(i);
            canStop = 1;
        }   
    }
    
    //Check if System is stopped
    if(canStop)
    {
        //Status will be updated in Do_Task() 
        Queue_Enqueue(&stateQueue, &nextState);
    }
    //Notify system is not operating
    else
    {
        pcAction |= 1 << PC_Action_Bit_InfoMessage;
        status |= 1 << Status_Bit_MotorCtr_NoOperWorking;
        Response_GetDeviceStatus(pcAction, status);
    }    
}

void On_RealActionTransmision()
{
    struct MotorAction *action = (struct MotorAction *)&rfHandler.ReceivePackage[Index_Payload];
    unsigned int pcAction = 0, status = 0;
    int i;
    
    //Refuse action, if system is operating
    if(LED_1_Read() != 0)
    {
        pcAction |= 1 << PC_Action_Bit_InfoMessage;
        status |= 1 << Status_Bit_MotorCtr_OperWorking;
        Response_GetDeviceStatus(pcAction, status);        
    }
    //System is free, ready to work
    else
    {
        //Disable read Infor => lose Action storage
        for(i = 0; i < Size_Motor; i++)
        {
            motorHandler[i].ActionQueueInfo.IsValid = 0;
            while(motorHandler[i].ActionQueue.Count > 0)
            {
                Queue_Dequeue(&motorHandler[i].ActionQueue);
            }
        }        
        AddAction(action);
        On_StartOperation();
    }
}

void On_ToggleSample()
{
    unsigned int pcAction = 0, status = 0;
    if(taskHandler.SampleFlag == 0)
    {
        taskHandler.SampleFlag = *(unsigned int *)&rfHandler.ReceivePackage[Pos_MotorCtr_SampleFlag];
        taskHandler.SampleOffset = 1000 /  rfHandler.ReceivePackage[Pos_MotorCtr_SampleFrequency];
        status |= 1 << Status_Bit_MotorCtr_SampleOn;
    }
    else
    {
        taskHandler.SampleFlag = 0;        
        status |= 1 << Status_Bit_MotorCtr_SampleOff;
    }
    
    pcAction |= 1 << PC_Action_Bit_AppStatus;
    Response_GetDeviceStatus(pcAction, status);
}

void On_ReadSetting()
{
    
}

void On_WriteSetting()
{
    
}

void On_ReadPwm()
{
    unsigned char nextState = State_RF_Transmit;
    unsigned short period = Signal_PWM_ReadPeriod();
    
    //set package
    rfHandler.TransmitPackage[Index_TaskID] = TaskID_MotorCtr;
    rfHandler.TransmitPackage[Index_Type] = Type_MotorCtr_ReadPwm;
    memcpy(&rfHandler.TransmitPackage[Pos_MotorCtr_Pwm_Period], &period, sizeof(unsigned short));
    
    //Request deday
    rfHandler.WaitOutput = RF_SwitchWait;
    
    //Request nextState
    Queue_Enqueue(&stateQueue, &nextState);
}

void On_WritePwm()
{
    unsigned short period =  *(unsigned short *)&rfHandler.ReceivePackage[Pos_MotorCtr_Pwm_Period];
    SetupPwmSignal(period);
}

void On_RealBothAction()
{
    struct MotorAction *action = (struct MotorAction *)&rfHandler.ReceivePackage[Index_Payload];    
    unsigned int pcAction = 0, status = 0;
    int i = 0;
    
    //Read each Action in received package
    for(i = 0; i < 2; i++)
    {
        AddAction(action + i);
    }
    
    //Refuse action, if system is operating
    if(LED_1_Read() != 0)
    {
        pcAction |= 1 << PC_Action_Bit_InfoMessage;
        status |= 1 << Status_Bit_MotorCtr_OperWorking;
        Response_GetDeviceStatus(pcAction, status);        
    }
    //System is free, ready to work
    else
    {
        //Disable read Infor => lose Action storage
        for(i = 0; i < Size_Motor; i++)
        {
            motorHandler[i].ActionQueueInfo.IsValid = 0;
            while(motorHandler[i].ActionQueue.Count > 0)
            {
                Queue_Dequeue(&motorHandler[i].ActionQueue);
            }
        }  
        //set Action
        for(i = 0; i < 2; i++)
        {
            AddAction(action + i);
        }
        
        On_StartOperation();
    }
}

//------------------------------Interrupt Jobs-----------------------
void CheckRfHandlerStatus()
{
    unsigned char nextState = State_Wait_Input;
    rfHandler.CheckInput = 1;    
    if(rfHandler.WaitOutput > 0)
    {
        rfHandler.WaitOutput--;
        //Important => Request again for RF_Transmit
        if(rfHandler.WaitOutput == 0)
        {
            nextState = State_RF_Transmit;
            Queue_Enqueue(&stateQueue, &nextState);
        }
    }    
}

void CheckAutoStart()
{
    if(taskHandler.CanAutoStart)
    {
        if(taskHandler.AutoStartDelay == 0)
        {
            //Ipmportant => Reset
            taskHandler.CanAutoStart = 0;
            On_StartOperation();
        }
        else
        {
            taskHandler.AutoStartDelay--;
        }
    }
}

void CheckDataSample()
{
    unsigned char nextState = State_Wait_Input;
    if( (taskHandler.TickCount % taskHandler.SampleOffset) == 0 && nextState == State_Wait_Input)
    {
        nextState = State_Generate_Output;
        Queue_Enqueue(&stateQueue, &nextState);
    }
}

void GetPwmFrequency()
{
    //Store current PWM Freq Counter => 10Hz => Multiply with 10
    if((taskHandler.TickCount % 100) == 0)
    {
        taskHandler.LastPwmFreq = Signal_Freq_Counter_ReadCounter() * 10;
        Signal_Freq_Counter_WriteCounter(0);
    }
}

void GetAdcValue()
{
    unsigned short anaValue = ADC_GetResult16();
    anaValue = ADC_CountsTo_mVolts(anaValue);
    
    switch(taskHandler.CurrentAna)
    {
        case Index_100V:
        taskHandler.Last_100V = anaValue;
        taskHandler.CurrentAna = Index_Battery;
        break;
        
        case Index_Battery:
        default:
        taskHandler.Last_Battery = anaValue;
        taskHandler.CurrentAna = Index_100V;
        break;
    }
    Ana_Mux_Select(taskHandler.CurrentAna);
}


//------------------------------Request Action------------------------------
void Request_PulseSignal(int index)
{
    unsigned short amount = motorHandler[index].CurrentAction->TriggerAmount;
    unsigned short requestInvert = (motorHandler[index].CurrentAction->Control1 & MotorCtr_Control1_InvertDir) ? 1 : 0;
    //Enable increment ?
    if(motorHandler[index].RequestIncrement)
    {
        amount += motorHandler[index].CurrentOffset;
        //Forward in AfterPackage, Always for AfterPulse
        if(requestInvert == motorHandler[index].RequestInvert)
        {
            motorHandler[index].CurrentOffset += motorHandler[index].CurrentAction->Increment;
        }
        //Backward in AfterPackage
        else
        {
            motorHandler[index].CurrentOffset -= motorHandler[index].CurrentAction->Increment;
        }
    }    
    SetupCounter(index, amount, 0);
    StartAction(index, 0, motorHandler[index].RequestInvert, 1);
}

void Request_DelaySignal(int index)
{
    //No out signal when delay
    SetupCounter(index, motorHandler[index].CurrentAction->DelayAmount, 1);
    StartAction(index, 1, 0, 0);
}

void Request_InvertPulseSignal(int index)
{
    //Important => Invert result value
    unsigned char isInvert = motorHandler[index].RequestInvert ? 0 : 1;
    unsigned short amount = motorHandler[index].CurrentAction->TriggerAmount;
    
    //Enable increment ? => Invert => minus
    if(motorHandler[index].RequestIncrement)
    {
        //Offset is always same as Forward => this method is only call in AfterPulse
        amount += motorHandler[index].CurrentOffset;
    }   
    SetupCounter(index, amount, 0);
    StartAction(index, 0, isInvert, 1);
}

//--------------------------------Sub Function-----------------------
void Response_GetDeviceStatus(unsigned int pcAction, unsigned int status)
{
    unsigned char nextState = State_RF_Transmit;
    
    //Notify device is available
    status |= 1 << Status_Bit_MotorCtr_Available;
    
    //Generate RF Package
    rfHandler.TransmitPackage[Index_TaskID] = TaskID_MotorCtr;
    rfHandler.TransmitPackage[Index_Type] = Type_MotorCtr_DeviceStatus;
    memcpy(&rfHandler.TransmitPackage[Pos_MotorCtr_PcAction], &pcAction, sizeof(unsigned int));
    memcpy(&rfHandler.TransmitPackage[Pos_MotorCtr_Status], &status, sizeof(unsigned int));
    
    //Request nextState
    Queue_Enqueue(&stateQueue, &nextState);  
    //Wait Master to switch to Rx
    rfHandler.WaitOutput = RF_SwitchWait;
}

void AddAction(struct MotorAction *action)
{
    if((action->Control1 & MotorCtr_Control1_Motor1) != 0)
    {
        Queue_Enqueue(&motorHandler[0].ActionQueue, action);
    }
    if((action->Control1 & MotorCtr_Control1_Motor2) != 0)
    {
        Queue_Enqueue(&motorHandler[1].ActionQueue, action);
    }
}

void LoadNextAction(int index)
{
    //Property of Action cannot be edit => in case it will be used later    
    unsigned char canBackward = 0;
    motorHandler[index].CurrentAction = (struct MotorAction *)Queue_Dequeue(&motorHandler[index].ActionQueue);                
    motorHandler[index].RequestDelay = (motorHandler[index].CurrentAction->Control1 & MotorCtr_Control1_HasDelay) ? 1 : 0;   
    motorHandler[index].RequestInvert = (motorHandler[index].CurrentAction->Control1 & MotorCtr_Control1_InvertDir) ? 1 : 0;   
    motorHandler[index].RequestIncrement = (motorHandler[index].CurrentAction->Control2 & MotorCtr_Control2_HasIncrement) ? 1 : 0;   
    motorHandler[index].Amount = motorHandler[index].CurrentAction->Amount;    
    motorHandler[index].ActionIndex = 0;    
    motorHandler[index].CurrentOffset = 0;
    
    canBackward = (motorHandler[index].CurrentAction->Control2 & MotorCtr_Control2_HasBackward) ? 1 : 0;;
    //Enable Backward => both have invert value
    if(canBackward)
    {
        motorHandler[index].BackwardPackage = (motorHandler[index].CurrentAction->Control2 & MotorCtr_Control2_ActerPackage) ? 1 : 0;
        //Important => Invert result
        motorHandler[index].BackwardPulse = (motorHandler[index].CurrentAction->Control2 & MotorCtr_Control2_ActerPackage) ? 0 : 1;
    }
    //Disable backward => Reset both
    else
    {
        motorHandler[index].BackwardPackage = 0;
        motorHandler[index].BackwardPulse = 0;
    }
    
    //Set PWM if Action is trigger => only once at begin
    if((motorHandler[index].CurrentAction->Control1 & MotorCtr_Control1_Trigger) != 0)
    {
        //PWM Parameter is enabled
        if( (motorHandler[index].CurrentAction->Control2 & MotorCtr_Control2_HasPWM) != 0)
        {
            SetupPwmSignal(motorHandler[index].CurrentAction->PwmPeriod);
        }
        //No PWM Parameter => use default
        else
        {
            ResetSetupPwmSignal();
        }
    }        
}

//Reset Counter, and assign new Comare Value
void SetupCounter(int index, unsigned int amount, unsigned char isDelay)
{   
    //Trigger on last Pulse => Stop current PWM Signal => Lost last Pulse
    //+1 in order to neglect this effect
    //Only for Trigger Signal
    if(isDelay == 0)
    {
        amount++;
    }    
    switch(index)
    {
        case 0:    
        Motor_Counter_0_WriteCounter(0);
        Motor_Counter_0_WriteCompare(amount);           
        break;
        
        case 1:         
        Motor_Counter_1_WriteCounter(0);
        Motor_Counter_1_WriteCompare(amount);           
        break;
        
        default:        
        //ignore
        break;        
    }
}

void ResetSetupPwmSignal()
{
    //set value only value is different
    if(Signal_PWM_ReadPeriod() != *Setting_PWM_Period)
    {
        Signal_PWM_WritePeriod(*Setting_PWM_Period);
    }
    if(Signal_PWM_ReadCompare1() != *Setting_PWM_Compare1)
    {
        Signal_PWM_WriteCompare1(*Setting_PWM_Compare1);
    }
    if(Signal_PWM_ReadCompare2() != *Setting_PWM_Compare2)
    {
        Signal_PWM_WriteCompare2(*Setting_PWM_Compare2);
    }
}

void SetupPwmSignal(unsigned short period)
{
    unsigned short compare1 = period * 3 / 4;
    unsigned short compare2 = period / 4;
    
    Signal_PWM_WritePeriod(period);
    Signal_PWM_WriteCompare1(compare1);
    Signal_PWM_WriteCompare2(compare2);
}

void StartAction(int index, unsigned char isDelay, unsigned char isInvert, unsigned char isOut)
{
    unsigned char value = Motor_Ctr_Reset;    
    
    if(isDelay)
    {
        value |= Motor_Ctr_Delay;
    }
    else
    {
        value |= Motor_Ctr_EnableSignal;
        if(isOut)
        {
            value |= Motor_Ctr_EnableOut;
        }
        if(isInvert)
        {
            value |= Motor_Ctr_Inverse;
        }
    }
    
    /* Enable Action
    1. Reset Counter, indirectly set value for SR-FF
    2. Clear reset, to enable signal from PWM, or Timer
    
    S   R   Action      nQ
    0   0   hold        Q
    1   0   set         1
    0   1   reset       0
    */    
    switch(index)
    {
        case 0:
        Motor_Ctr_0_Write(value);         
        Motor_Ctr_0_Write(value & Motor_Ctr_Clear_Reset);        
        break;
        
        case 1:
        default:
        Motor_Ctr_1_Write(value);           
        Motor_Ctr_1_Write(value & Motor_Ctr_Clear_Reset);        
        break;
    }
}

void StopAction(int index)
{
    /* Disable Motor
       Clear all Control bit
    => No signal from PWM, or Timer
    => Counter is disable, due to reset bit
    */
    switch(index)
    {
        case 0:         Motor_Ctr_0_Write(0);       break;        
        case 1:                 
        default:        Motor_Ctr_1_Write(0);       break;
    }
}

