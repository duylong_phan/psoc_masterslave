#include <project.h>

void GetAdcValue();

int main()
{
    //always enable
    CyGlobalIntEnable; 
    
    //Start PWM
    OutPin_PWM_Start();
    
    //Start ADC, and convertion (important)
    ADC_Start();    
    ADC_StartConvert();
    
    //operation loop
    for(;;)
    {
        GetAdcValue();
        CyDelay(5);
    }
}

void GetAdcValue()
{
    unsigned int compare = 0;
    unsigned int input = ADC_GetResult16(0);
    
    //get command in %
    input = input * 200 / 2048;
    
    //with 50Hz PWM
    //min => 0.6ms == 1850
    //max => 2.3ms == 6900
    //=> width = 1850 + 2525 * a
    //where a is from [0;2]
        
    //get width
    compare = 1850 + (2525 * input)/100;
    
    //only update if value is different
    if(compare != OutPin_PWM_ReadCompare())
    {
        OutPin_PWM_WriteCompare(compare);
        LED_Write(!LED_Read());
    }  
}