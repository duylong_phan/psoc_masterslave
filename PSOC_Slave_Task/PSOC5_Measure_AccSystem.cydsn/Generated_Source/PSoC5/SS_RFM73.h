/*******************************************************************************
* File Name: SS_RFM73.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SS_RFM73_H) /* Pins SS_RFM73_H */
#define CY_PINS_SS_RFM73_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "SS_RFM73_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v2_10 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 SS_RFM73__PORT == 15 && ((SS_RFM73__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    SS_RFM73_Write(uint8 value) ;
void    SS_RFM73_SetDriveMode(uint8 mode) ;
uint8   SS_RFM73_ReadDataReg(void) ;
uint8   SS_RFM73_Read(void) ;
uint8   SS_RFM73_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define SS_RFM73_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define SS_RFM73_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define SS_RFM73_DM_RES_UP          PIN_DM_RES_UP
#define SS_RFM73_DM_RES_DWN         PIN_DM_RES_DWN
#define SS_RFM73_DM_OD_LO           PIN_DM_OD_LO
#define SS_RFM73_DM_OD_HI           PIN_DM_OD_HI
#define SS_RFM73_DM_STRONG          PIN_DM_STRONG
#define SS_RFM73_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define SS_RFM73_MASK               SS_RFM73__MASK
#define SS_RFM73_SHIFT              SS_RFM73__SHIFT
#define SS_RFM73_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define SS_RFM73_PS                     (* (reg8 *) SS_RFM73__PS)
/* Data Register */
#define SS_RFM73_DR                     (* (reg8 *) SS_RFM73__DR)
/* Port Number */
#define SS_RFM73_PRT_NUM                (* (reg8 *) SS_RFM73__PRT) 
/* Connect to Analog Globals */                                                  
#define SS_RFM73_AG                     (* (reg8 *) SS_RFM73__AG)                       
/* Analog MUX bux enable */
#define SS_RFM73_AMUX                   (* (reg8 *) SS_RFM73__AMUX) 
/* Bidirectional Enable */                                                        
#define SS_RFM73_BIE                    (* (reg8 *) SS_RFM73__BIE)
/* Bit-mask for Aliased Register Access */
#define SS_RFM73_BIT_MASK               (* (reg8 *) SS_RFM73__BIT_MASK)
/* Bypass Enable */
#define SS_RFM73_BYP                    (* (reg8 *) SS_RFM73__BYP)
/* Port wide control signals */                                                   
#define SS_RFM73_CTL                    (* (reg8 *) SS_RFM73__CTL)
/* Drive Modes */
#define SS_RFM73_DM0                    (* (reg8 *) SS_RFM73__DM0) 
#define SS_RFM73_DM1                    (* (reg8 *) SS_RFM73__DM1)
#define SS_RFM73_DM2                    (* (reg8 *) SS_RFM73__DM2) 
/* Input Buffer Disable Override */
#define SS_RFM73_INP_DIS                (* (reg8 *) SS_RFM73__INP_DIS)
/* LCD Common or Segment Drive */
#define SS_RFM73_LCD_COM_SEG            (* (reg8 *) SS_RFM73__LCD_COM_SEG)
/* Enable Segment LCD */
#define SS_RFM73_LCD_EN                 (* (reg8 *) SS_RFM73__LCD_EN)
/* Slew Rate Control */
#define SS_RFM73_SLW                    (* (reg8 *) SS_RFM73__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define SS_RFM73_PRTDSI__CAPS_SEL       (* (reg8 *) SS_RFM73__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define SS_RFM73_PRTDSI__DBL_SYNC_IN    (* (reg8 *) SS_RFM73__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define SS_RFM73_PRTDSI__OE_SEL0        (* (reg8 *) SS_RFM73__PRTDSI__OE_SEL0) 
#define SS_RFM73_PRTDSI__OE_SEL1        (* (reg8 *) SS_RFM73__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define SS_RFM73_PRTDSI__OUT_SEL0       (* (reg8 *) SS_RFM73__PRTDSI__OUT_SEL0) 
#define SS_RFM73_PRTDSI__OUT_SEL1       (* (reg8 *) SS_RFM73__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define SS_RFM73_PRTDSI__SYNC_OUT       (* (reg8 *) SS_RFM73__PRTDSI__SYNC_OUT) 


#if defined(SS_RFM73__INTSTAT)  /* Interrupt Registers */

    #define SS_RFM73_INTSTAT                (* (reg8 *) SS_RFM73__INTSTAT)
    #define SS_RFM73_SNAP                   (* (reg8 *) SS_RFM73__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_SS_RFM73_H */


/* [] END OF FILE */
