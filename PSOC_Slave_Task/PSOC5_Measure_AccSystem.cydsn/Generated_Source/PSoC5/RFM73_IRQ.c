/*******************************************************************************
* File Name: RFM73_IRQ.c  
* Version 2.10
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "RFM73_IRQ.h"

/* APIs are not generated for P15[7:6] on PSoC 5 */
#if !(CY_PSOC5A &&\
	 RFM73_IRQ__PORT == 15 && ((RFM73_IRQ__MASK & 0xC0) != 0))


/*******************************************************************************
* Function Name: RFM73_IRQ_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None
*  
*******************************************************************************/
void RFM73_IRQ_Write(uint8 value) 
{
    uint8 staticBits = (RFM73_IRQ_DR & (uint8)(~RFM73_IRQ_MASK));
    RFM73_IRQ_DR = staticBits | ((uint8)(value << RFM73_IRQ_SHIFT) & RFM73_IRQ_MASK);
}


/*******************************************************************************
* Function Name: RFM73_IRQ_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to one of the following drive modes.
*
*  RFM73_IRQ_DM_STRONG     Strong Drive 
*  RFM73_IRQ_DM_OD_HI      Open Drain, Drives High 
*  RFM73_IRQ_DM_OD_LO      Open Drain, Drives Low 
*  RFM73_IRQ_DM_RES_UP     Resistive Pull Up 
*  RFM73_IRQ_DM_RES_DWN    Resistive Pull Down 
*  RFM73_IRQ_DM_RES_UPDWN  Resistive Pull Up/Down 
*  RFM73_IRQ_DM_DIG_HIZ    High Impedance Digital 
*  RFM73_IRQ_DM_ALG_HIZ    High Impedance Analog 
*
* Return: 
*  None
*
*******************************************************************************/
void RFM73_IRQ_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(RFM73_IRQ_0, mode);
}


/*******************************************************************************
* Function Name: RFM73_IRQ_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro RFM73_IRQ_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 RFM73_IRQ_Read(void) 
{
    return (RFM73_IRQ_PS & RFM73_IRQ_MASK) >> RFM73_IRQ_SHIFT;
}


/*******************************************************************************
* Function Name: RFM73_IRQ_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 RFM73_IRQ_ReadDataReg(void) 
{
    return (RFM73_IRQ_DR & RFM73_IRQ_MASK) >> RFM73_IRQ_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(RFM73_IRQ_INTSTAT) 

    /*******************************************************************************
    * Function Name: RFM73_IRQ_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 RFM73_IRQ_ClearInterrupt(void) 
    {
        return (RFM73_IRQ_INTSTAT & RFM73_IRQ_MASK) >> RFM73_IRQ_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif /* CY_PSOC5A... */

    
/* [] END OF FILE */
