/*******************************************************************************
* File Name: RFM73_IRQ.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_RFM73_IRQ_H) /* Pins RFM73_IRQ_H */
#define CY_PINS_RFM73_IRQ_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "RFM73_IRQ_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v2_10 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 RFM73_IRQ__PORT == 15 && ((RFM73_IRQ__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    RFM73_IRQ_Write(uint8 value) ;
void    RFM73_IRQ_SetDriveMode(uint8 mode) ;
uint8   RFM73_IRQ_ReadDataReg(void) ;
uint8   RFM73_IRQ_Read(void) ;
uint8   RFM73_IRQ_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define RFM73_IRQ_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define RFM73_IRQ_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define RFM73_IRQ_DM_RES_UP          PIN_DM_RES_UP
#define RFM73_IRQ_DM_RES_DWN         PIN_DM_RES_DWN
#define RFM73_IRQ_DM_OD_LO           PIN_DM_OD_LO
#define RFM73_IRQ_DM_OD_HI           PIN_DM_OD_HI
#define RFM73_IRQ_DM_STRONG          PIN_DM_STRONG
#define RFM73_IRQ_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define RFM73_IRQ_MASK               RFM73_IRQ__MASK
#define RFM73_IRQ_SHIFT              RFM73_IRQ__SHIFT
#define RFM73_IRQ_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define RFM73_IRQ_PS                     (* (reg8 *) RFM73_IRQ__PS)
/* Data Register */
#define RFM73_IRQ_DR                     (* (reg8 *) RFM73_IRQ__DR)
/* Port Number */
#define RFM73_IRQ_PRT_NUM                (* (reg8 *) RFM73_IRQ__PRT) 
/* Connect to Analog Globals */                                                  
#define RFM73_IRQ_AG                     (* (reg8 *) RFM73_IRQ__AG)                       
/* Analog MUX bux enable */
#define RFM73_IRQ_AMUX                   (* (reg8 *) RFM73_IRQ__AMUX) 
/* Bidirectional Enable */                                                        
#define RFM73_IRQ_BIE                    (* (reg8 *) RFM73_IRQ__BIE)
/* Bit-mask for Aliased Register Access */
#define RFM73_IRQ_BIT_MASK               (* (reg8 *) RFM73_IRQ__BIT_MASK)
/* Bypass Enable */
#define RFM73_IRQ_BYP                    (* (reg8 *) RFM73_IRQ__BYP)
/* Port wide control signals */                                                   
#define RFM73_IRQ_CTL                    (* (reg8 *) RFM73_IRQ__CTL)
/* Drive Modes */
#define RFM73_IRQ_DM0                    (* (reg8 *) RFM73_IRQ__DM0) 
#define RFM73_IRQ_DM1                    (* (reg8 *) RFM73_IRQ__DM1)
#define RFM73_IRQ_DM2                    (* (reg8 *) RFM73_IRQ__DM2) 
/* Input Buffer Disable Override */
#define RFM73_IRQ_INP_DIS                (* (reg8 *) RFM73_IRQ__INP_DIS)
/* LCD Common or Segment Drive */
#define RFM73_IRQ_LCD_COM_SEG            (* (reg8 *) RFM73_IRQ__LCD_COM_SEG)
/* Enable Segment LCD */
#define RFM73_IRQ_LCD_EN                 (* (reg8 *) RFM73_IRQ__LCD_EN)
/* Slew Rate Control */
#define RFM73_IRQ_SLW                    (* (reg8 *) RFM73_IRQ__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define RFM73_IRQ_PRTDSI__CAPS_SEL       (* (reg8 *) RFM73_IRQ__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define RFM73_IRQ_PRTDSI__DBL_SYNC_IN    (* (reg8 *) RFM73_IRQ__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define RFM73_IRQ_PRTDSI__OE_SEL0        (* (reg8 *) RFM73_IRQ__PRTDSI__OE_SEL0) 
#define RFM73_IRQ_PRTDSI__OE_SEL1        (* (reg8 *) RFM73_IRQ__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define RFM73_IRQ_PRTDSI__OUT_SEL0       (* (reg8 *) RFM73_IRQ__PRTDSI__OUT_SEL0) 
#define RFM73_IRQ_PRTDSI__OUT_SEL1       (* (reg8 *) RFM73_IRQ__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define RFM73_IRQ_PRTDSI__SYNC_OUT       (* (reg8 *) RFM73_IRQ__PRTDSI__SYNC_OUT) 


#if defined(RFM73_IRQ__INTSTAT)  /* Interrupt Registers */

    #define RFM73_IRQ_INTSTAT                (* (reg8 *) RFM73_IRQ__INTSTAT)
    #define RFM73_IRQ_SNAP                   (* (reg8 *) RFM73_IRQ__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_RFM73_IRQ_H */


/* [] END OF FILE */
