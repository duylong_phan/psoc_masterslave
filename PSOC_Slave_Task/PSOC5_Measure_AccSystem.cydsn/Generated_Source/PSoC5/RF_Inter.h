/*******************************************************************************
* File Name: RF_Inter.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_RF_Inter_H)
#define CY_ISR_RF_Inter_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void RF_Inter_Start(void);
void RF_Inter_StartEx(cyisraddress address);
void RF_Inter_Stop(void);

CY_ISR_PROTO(RF_Inter_Interrupt);

void RF_Inter_SetVector(cyisraddress address);
cyisraddress RF_Inter_GetVector(void);

void RF_Inter_SetPriority(uint8 priority);
uint8 RF_Inter_GetPriority(void);

void RF_Inter_Enable(void);
uint8 RF_Inter_GetState(void);
void RF_Inter_Disable(void);

void RF_Inter_SetPending(void);
void RF_Inter_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the RF_Inter ISR. */
#define RF_Inter_INTC_VECTOR            ((reg32 *) RF_Inter__INTC_VECT)

/* Address of the RF_Inter ISR priority. */
#define RF_Inter_INTC_PRIOR             ((reg8 *) RF_Inter__INTC_PRIOR_REG)

/* Priority of the RF_Inter interrupt. */
#define RF_Inter_INTC_PRIOR_NUMBER      RF_Inter__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable RF_Inter interrupt. */
#define RF_Inter_INTC_SET_EN            ((reg32 *) RF_Inter__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the RF_Inter interrupt. */
#define RF_Inter_INTC_CLR_EN            ((reg32 *) RF_Inter__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the RF_Inter interrupt state to pending. */
#define RF_Inter_INTC_SET_PD            ((reg32 *) RF_Inter__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the RF_Inter interrupt. */
#define RF_Inter_INTC_CLR_PD            ((reg32 *) RF_Inter__INTC_CLR_PD_REG)


#endif /* CY_ISR_RF_Inter_H */


/* [] END OF FILE */
