/*******************************************************************************
* File Name: RF_CE.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_RF_CE_H) /* Pins RF_CE_H */
#define CY_PINS_RF_CE_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "RF_CE_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v2_10 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 RF_CE__PORT == 15 && ((RF_CE__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    RF_CE_Write(uint8 value) ;
void    RF_CE_SetDriveMode(uint8 mode) ;
uint8   RF_CE_ReadDataReg(void) ;
uint8   RF_CE_Read(void) ;
uint8   RF_CE_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define RF_CE_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define RF_CE_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define RF_CE_DM_RES_UP          PIN_DM_RES_UP
#define RF_CE_DM_RES_DWN         PIN_DM_RES_DWN
#define RF_CE_DM_OD_LO           PIN_DM_OD_LO
#define RF_CE_DM_OD_HI           PIN_DM_OD_HI
#define RF_CE_DM_STRONG          PIN_DM_STRONG
#define RF_CE_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define RF_CE_MASK               RF_CE__MASK
#define RF_CE_SHIFT              RF_CE__SHIFT
#define RF_CE_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define RF_CE_PS                     (* (reg8 *) RF_CE__PS)
/* Data Register */
#define RF_CE_DR                     (* (reg8 *) RF_CE__DR)
/* Port Number */
#define RF_CE_PRT_NUM                (* (reg8 *) RF_CE__PRT) 
/* Connect to Analog Globals */                                                  
#define RF_CE_AG                     (* (reg8 *) RF_CE__AG)                       
/* Analog MUX bux enable */
#define RF_CE_AMUX                   (* (reg8 *) RF_CE__AMUX) 
/* Bidirectional Enable */                                                        
#define RF_CE_BIE                    (* (reg8 *) RF_CE__BIE)
/* Bit-mask for Aliased Register Access */
#define RF_CE_BIT_MASK               (* (reg8 *) RF_CE__BIT_MASK)
/* Bypass Enable */
#define RF_CE_BYP                    (* (reg8 *) RF_CE__BYP)
/* Port wide control signals */                                                   
#define RF_CE_CTL                    (* (reg8 *) RF_CE__CTL)
/* Drive Modes */
#define RF_CE_DM0                    (* (reg8 *) RF_CE__DM0) 
#define RF_CE_DM1                    (* (reg8 *) RF_CE__DM1)
#define RF_CE_DM2                    (* (reg8 *) RF_CE__DM2) 
/* Input Buffer Disable Override */
#define RF_CE_INP_DIS                (* (reg8 *) RF_CE__INP_DIS)
/* LCD Common or Segment Drive */
#define RF_CE_LCD_COM_SEG            (* (reg8 *) RF_CE__LCD_COM_SEG)
/* Enable Segment LCD */
#define RF_CE_LCD_EN                 (* (reg8 *) RF_CE__LCD_EN)
/* Slew Rate Control */
#define RF_CE_SLW                    (* (reg8 *) RF_CE__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define RF_CE_PRTDSI__CAPS_SEL       (* (reg8 *) RF_CE__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define RF_CE_PRTDSI__DBL_SYNC_IN    (* (reg8 *) RF_CE__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define RF_CE_PRTDSI__OE_SEL0        (* (reg8 *) RF_CE__PRTDSI__OE_SEL0) 
#define RF_CE_PRTDSI__OE_SEL1        (* (reg8 *) RF_CE__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define RF_CE_PRTDSI__OUT_SEL0       (* (reg8 *) RF_CE__PRTDSI__OUT_SEL0) 
#define RF_CE_PRTDSI__OUT_SEL1       (* (reg8 *) RF_CE__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define RF_CE_PRTDSI__SYNC_OUT       (* (reg8 *) RF_CE__PRTDSI__SYNC_OUT) 


#if defined(RF_CE__INTSTAT)  /* Interrupt Registers */

    #define RF_CE_INTSTAT                (* (reg8 *) RF_CE__INTSTAT)
    #define RF_CE_SNAP                   (* (reg8 *) RF_CE__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_RF_CE_H */


/* [] END OF FILE */
