/*******************************************************************************
* File Name: SPI_MOSI3.c  
* Version 2.10
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "SPI_MOSI3.h"

/* APIs are not generated for P15[7:6] on PSoC 5 */
#if !(CY_PSOC5A &&\
	 SPI_MOSI3__PORT == 15 && ((SPI_MOSI3__MASK & 0xC0) != 0))


/*******************************************************************************
* Function Name: SPI_MOSI3_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None
*  
*******************************************************************************/
void SPI_MOSI3_Write(uint8 value) 
{
    uint8 staticBits = (SPI_MOSI3_DR & (uint8)(~SPI_MOSI3_MASK));
    SPI_MOSI3_DR = staticBits | ((uint8)(value << SPI_MOSI3_SHIFT) & SPI_MOSI3_MASK);
}


/*******************************************************************************
* Function Name: SPI_MOSI3_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to one of the following drive modes.
*
*  SPI_MOSI3_DM_STRONG     Strong Drive 
*  SPI_MOSI3_DM_OD_HI      Open Drain, Drives High 
*  SPI_MOSI3_DM_OD_LO      Open Drain, Drives Low 
*  SPI_MOSI3_DM_RES_UP     Resistive Pull Up 
*  SPI_MOSI3_DM_RES_DWN    Resistive Pull Down 
*  SPI_MOSI3_DM_RES_UPDWN  Resistive Pull Up/Down 
*  SPI_MOSI3_DM_DIG_HIZ    High Impedance Digital 
*  SPI_MOSI3_DM_ALG_HIZ    High Impedance Analog 
*
* Return: 
*  None
*
*******************************************************************************/
void SPI_MOSI3_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(SPI_MOSI3_0, mode);
}


/*******************************************************************************
* Function Name: SPI_MOSI3_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro SPI_MOSI3_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 SPI_MOSI3_Read(void) 
{
    return (SPI_MOSI3_PS & SPI_MOSI3_MASK) >> SPI_MOSI3_SHIFT;
}


/*******************************************************************************
* Function Name: SPI_MOSI3_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 SPI_MOSI3_ReadDataReg(void) 
{
    return (SPI_MOSI3_DR & SPI_MOSI3_MASK) >> SPI_MOSI3_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(SPI_MOSI3_INTSTAT) 

    /*******************************************************************************
    * Function Name: SPI_MOSI3_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 SPI_MOSI3_ClearInterrupt(void) 
    {
        return (SPI_MOSI3_INTSTAT & SPI_MOSI3_MASK) >> SPI_MOSI3_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif /* CY_PSOC5A... */

    
/* [] END OF FILE */
