/*******************************************************************************
* File Name: P_Tempature.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_P_Tempature_H) /* Pins P_Tempature_H */
#define CY_PINS_P_Tempature_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "P_Tempature_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v2_10 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 P_Tempature__PORT == 15 && ((P_Tempature__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    P_Tempature_Write(uint8 value) ;
void    P_Tempature_SetDriveMode(uint8 mode) ;
uint8   P_Tempature_ReadDataReg(void) ;
uint8   P_Tempature_Read(void) ;
uint8   P_Tempature_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define P_Tempature_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define P_Tempature_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define P_Tempature_DM_RES_UP          PIN_DM_RES_UP
#define P_Tempature_DM_RES_DWN         PIN_DM_RES_DWN
#define P_Tempature_DM_OD_LO           PIN_DM_OD_LO
#define P_Tempature_DM_OD_HI           PIN_DM_OD_HI
#define P_Tempature_DM_STRONG          PIN_DM_STRONG
#define P_Tempature_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define P_Tempature_MASK               P_Tempature__MASK
#define P_Tempature_SHIFT              P_Tempature__SHIFT
#define P_Tempature_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define P_Tempature_PS                     (* (reg8 *) P_Tempature__PS)
/* Data Register */
#define P_Tempature_DR                     (* (reg8 *) P_Tempature__DR)
/* Port Number */
#define P_Tempature_PRT_NUM                (* (reg8 *) P_Tempature__PRT) 
/* Connect to Analog Globals */                                                  
#define P_Tempature_AG                     (* (reg8 *) P_Tempature__AG)                       
/* Analog MUX bux enable */
#define P_Tempature_AMUX                   (* (reg8 *) P_Tempature__AMUX) 
/* Bidirectional Enable */                                                        
#define P_Tempature_BIE                    (* (reg8 *) P_Tempature__BIE)
/* Bit-mask for Aliased Register Access */
#define P_Tempature_BIT_MASK               (* (reg8 *) P_Tempature__BIT_MASK)
/* Bypass Enable */
#define P_Tempature_BYP                    (* (reg8 *) P_Tempature__BYP)
/* Port wide control signals */                                                   
#define P_Tempature_CTL                    (* (reg8 *) P_Tempature__CTL)
/* Drive Modes */
#define P_Tempature_DM0                    (* (reg8 *) P_Tempature__DM0) 
#define P_Tempature_DM1                    (* (reg8 *) P_Tempature__DM1)
#define P_Tempature_DM2                    (* (reg8 *) P_Tempature__DM2) 
/* Input Buffer Disable Override */
#define P_Tempature_INP_DIS                (* (reg8 *) P_Tempature__INP_DIS)
/* LCD Common or Segment Drive */
#define P_Tempature_LCD_COM_SEG            (* (reg8 *) P_Tempature__LCD_COM_SEG)
/* Enable Segment LCD */
#define P_Tempature_LCD_EN                 (* (reg8 *) P_Tempature__LCD_EN)
/* Slew Rate Control */
#define P_Tempature_SLW                    (* (reg8 *) P_Tempature__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define P_Tempature_PRTDSI__CAPS_SEL       (* (reg8 *) P_Tempature__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define P_Tempature_PRTDSI__DBL_SYNC_IN    (* (reg8 *) P_Tempature__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define P_Tempature_PRTDSI__OE_SEL0        (* (reg8 *) P_Tempature__PRTDSI__OE_SEL0) 
#define P_Tempature_PRTDSI__OE_SEL1        (* (reg8 *) P_Tempature__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define P_Tempature_PRTDSI__OUT_SEL0       (* (reg8 *) P_Tempature__PRTDSI__OUT_SEL0) 
#define P_Tempature_PRTDSI__OUT_SEL1       (* (reg8 *) P_Tempature__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define P_Tempature_PRTDSI__SYNC_OUT       (* (reg8 *) P_Tempature__PRTDSI__SYNC_OUT) 


#if defined(P_Tempature__INTSTAT)  /* Interrupt Registers */

    #define P_Tempature_INTSTAT                (* (reg8 *) P_Tempature__INTSTAT)
    #define P_Tempature_SNAP                   (* (reg8 *) P_Tempature__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_P_Tempature_H */


/* [] END OF FILE */
