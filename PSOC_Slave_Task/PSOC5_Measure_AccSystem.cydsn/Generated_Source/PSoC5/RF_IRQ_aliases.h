/*******************************************************************************
* File Name: RF_IRQ.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_RF_IRQ_ALIASES_H) /* Pins RF_IRQ_ALIASES_H */
#define CY_PINS_RF_IRQ_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"



/***************************************
*              Constants        
***************************************/
#define RF_IRQ_0		(RF_IRQ__0__PC)

#endif /* End Pins RF_IRQ_ALIASES_H */

/* [] END OF FILE */
