/*******************************************************************************
* File Name: MISO_Acc4.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_MISO_Acc4_H) /* Pins MISO_Acc4_H */
#define CY_PINS_MISO_Acc4_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "MISO_Acc4_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v2_10 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 MISO_Acc4__PORT == 15 && ((MISO_Acc4__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    MISO_Acc4_Write(uint8 value) ;
void    MISO_Acc4_SetDriveMode(uint8 mode) ;
uint8   MISO_Acc4_ReadDataReg(void) ;
uint8   MISO_Acc4_Read(void) ;
uint8   MISO_Acc4_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define MISO_Acc4_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define MISO_Acc4_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define MISO_Acc4_DM_RES_UP          PIN_DM_RES_UP
#define MISO_Acc4_DM_RES_DWN         PIN_DM_RES_DWN
#define MISO_Acc4_DM_OD_LO           PIN_DM_OD_LO
#define MISO_Acc4_DM_OD_HI           PIN_DM_OD_HI
#define MISO_Acc4_DM_STRONG          PIN_DM_STRONG
#define MISO_Acc4_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define MISO_Acc4_MASK               MISO_Acc4__MASK
#define MISO_Acc4_SHIFT              MISO_Acc4__SHIFT
#define MISO_Acc4_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define MISO_Acc4_PS                     (* (reg8 *) MISO_Acc4__PS)
/* Data Register */
#define MISO_Acc4_DR                     (* (reg8 *) MISO_Acc4__DR)
/* Port Number */
#define MISO_Acc4_PRT_NUM                (* (reg8 *) MISO_Acc4__PRT) 
/* Connect to Analog Globals */                                                  
#define MISO_Acc4_AG                     (* (reg8 *) MISO_Acc4__AG)                       
/* Analog MUX bux enable */
#define MISO_Acc4_AMUX                   (* (reg8 *) MISO_Acc4__AMUX) 
/* Bidirectional Enable */                                                        
#define MISO_Acc4_BIE                    (* (reg8 *) MISO_Acc4__BIE)
/* Bit-mask for Aliased Register Access */
#define MISO_Acc4_BIT_MASK               (* (reg8 *) MISO_Acc4__BIT_MASK)
/* Bypass Enable */
#define MISO_Acc4_BYP                    (* (reg8 *) MISO_Acc4__BYP)
/* Port wide control signals */                                                   
#define MISO_Acc4_CTL                    (* (reg8 *) MISO_Acc4__CTL)
/* Drive Modes */
#define MISO_Acc4_DM0                    (* (reg8 *) MISO_Acc4__DM0) 
#define MISO_Acc4_DM1                    (* (reg8 *) MISO_Acc4__DM1)
#define MISO_Acc4_DM2                    (* (reg8 *) MISO_Acc4__DM2) 
/* Input Buffer Disable Override */
#define MISO_Acc4_INP_DIS                (* (reg8 *) MISO_Acc4__INP_DIS)
/* LCD Common or Segment Drive */
#define MISO_Acc4_LCD_COM_SEG            (* (reg8 *) MISO_Acc4__LCD_COM_SEG)
/* Enable Segment LCD */
#define MISO_Acc4_LCD_EN                 (* (reg8 *) MISO_Acc4__LCD_EN)
/* Slew Rate Control */
#define MISO_Acc4_SLW                    (* (reg8 *) MISO_Acc4__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define MISO_Acc4_PRTDSI__CAPS_SEL       (* (reg8 *) MISO_Acc4__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define MISO_Acc4_PRTDSI__DBL_SYNC_IN    (* (reg8 *) MISO_Acc4__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define MISO_Acc4_PRTDSI__OE_SEL0        (* (reg8 *) MISO_Acc4__PRTDSI__OE_SEL0) 
#define MISO_Acc4_PRTDSI__OE_SEL1        (* (reg8 *) MISO_Acc4__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define MISO_Acc4_PRTDSI__OUT_SEL0       (* (reg8 *) MISO_Acc4__PRTDSI__OUT_SEL0) 
#define MISO_Acc4_PRTDSI__OUT_SEL1       (* (reg8 *) MISO_Acc4__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define MISO_Acc4_PRTDSI__SYNC_OUT       (* (reg8 *) MISO_Acc4__PRTDSI__SYNC_OUT) 


#if defined(MISO_Acc4__INTSTAT)  /* Interrupt Registers */

    #define MISO_Acc4_INTSTAT                (* (reg8 *) MISO_Acc4__INTSTAT)
    #define MISO_Acc4_SNAP                   (* (reg8 *) MISO_Acc4__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_MISO_Acc4_H */


/* [] END OF FILE */
