/*******************************************************************************
* File Name: MISO_Acc3.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_MISO_Acc3_H) /* Pins MISO_Acc3_H */
#define CY_PINS_MISO_Acc3_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "MISO_Acc3_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v2_10 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 MISO_Acc3__PORT == 15 && ((MISO_Acc3__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    MISO_Acc3_Write(uint8 value) ;
void    MISO_Acc3_SetDriveMode(uint8 mode) ;
uint8   MISO_Acc3_ReadDataReg(void) ;
uint8   MISO_Acc3_Read(void) ;
uint8   MISO_Acc3_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define MISO_Acc3_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define MISO_Acc3_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define MISO_Acc3_DM_RES_UP          PIN_DM_RES_UP
#define MISO_Acc3_DM_RES_DWN         PIN_DM_RES_DWN
#define MISO_Acc3_DM_OD_LO           PIN_DM_OD_LO
#define MISO_Acc3_DM_OD_HI           PIN_DM_OD_HI
#define MISO_Acc3_DM_STRONG          PIN_DM_STRONG
#define MISO_Acc3_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define MISO_Acc3_MASK               MISO_Acc3__MASK
#define MISO_Acc3_SHIFT              MISO_Acc3__SHIFT
#define MISO_Acc3_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define MISO_Acc3_PS                     (* (reg8 *) MISO_Acc3__PS)
/* Data Register */
#define MISO_Acc3_DR                     (* (reg8 *) MISO_Acc3__DR)
/* Port Number */
#define MISO_Acc3_PRT_NUM                (* (reg8 *) MISO_Acc3__PRT) 
/* Connect to Analog Globals */                                                  
#define MISO_Acc3_AG                     (* (reg8 *) MISO_Acc3__AG)                       
/* Analog MUX bux enable */
#define MISO_Acc3_AMUX                   (* (reg8 *) MISO_Acc3__AMUX) 
/* Bidirectional Enable */                                                        
#define MISO_Acc3_BIE                    (* (reg8 *) MISO_Acc3__BIE)
/* Bit-mask for Aliased Register Access */
#define MISO_Acc3_BIT_MASK               (* (reg8 *) MISO_Acc3__BIT_MASK)
/* Bypass Enable */
#define MISO_Acc3_BYP                    (* (reg8 *) MISO_Acc3__BYP)
/* Port wide control signals */                                                   
#define MISO_Acc3_CTL                    (* (reg8 *) MISO_Acc3__CTL)
/* Drive Modes */
#define MISO_Acc3_DM0                    (* (reg8 *) MISO_Acc3__DM0) 
#define MISO_Acc3_DM1                    (* (reg8 *) MISO_Acc3__DM1)
#define MISO_Acc3_DM2                    (* (reg8 *) MISO_Acc3__DM2) 
/* Input Buffer Disable Override */
#define MISO_Acc3_INP_DIS                (* (reg8 *) MISO_Acc3__INP_DIS)
/* LCD Common or Segment Drive */
#define MISO_Acc3_LCD_COM_SEG            (* (reg8 *) MISO_Acc3__LCD_COM_SEG)
/* Enable Segment LCD */
#define MISO_Acc3_LCD_EN                 (* (reg8 *) MISO_Acc3__LCD_EN)
/* Slew Rate Control */
#define MISO_Acc3_SLW                    (* (reg8 *) MISO_Acc3__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define MISO_Acc3_PRTDSI__CAPS_SEL       (* (reg8 *) MISO_Acc3__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define MISO_Acc3_PRTDSI__DBL_SYNC_IN    (* (reg8 *) MISO_Acc3__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define MISO_Acc3_PRTDSI__OE_SEL0        (* (reg8 *) MISO_Acc3__PRTDSI__OE_SEL0) 
#define MISO_Acc3_PRTDSI__OE_SEL1        (* (reg8 *) MISO_Acc3__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define MISO_Acc3_PRTDSI__OUT_SEL0       (* (reg8 *) MISO_Acc3__PRTDSI__OUT_SEL0) 
#define MISO_Acc3_PRTDSI__OUT_SEL1       (* (reg8 *) MISO_Acc3__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define MISO_Acc3_PRTDSI__SYNC_OUT       (* (reg8 *) MISO_Acc3__PRTDSI__SYNC_OUT) 


#if defined(MISO_Acc3__INTSTAT)  /* Interrupt Registers */

    #define MISO_Acc3_INTSTAT                (* (reg8 *) MISO_Acc3__INTSTAT)
    #define MISO_Acc3_SNAP                   (* (reg8 *) MISO_Acc3__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_MISO_Acc3_H */


/* [] END OF FILE */
